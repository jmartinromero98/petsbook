const express = require('../common/node_modules/express');

const app = express();
const router = express.Router();

const cors = require('../common/node_modules/cors');
const jwt = require('../common/node_modules/jsonwebtoken');
const { Sequelize } = require('../common/node_modules/sequelize');
const fs = require('fs');
const op = Sequelize.Op;

const Asociaciones = require('../common/models/Asociaciones');
const Usuarios = require('../common/models/Usuarios');
const Autonomias = require('../common/models/Autonomias');
const Provincias = require('../common/models/Provincias');
const Municipios = require('../common/models/Municipios');
const Razas = require('../common/models/Razas');
const Mascotas = require('../common/models/Mascotas');
const Animales = require('../common/models/Animales');
const Solicitudes = require('../common/models/Solicitudes');
const Posts = require('../common/models/Posts');

const bodyParser = require('body-parser');

var bcrypt = require('../common/node_modules/bcrypt');

const saltRounds = 15;

var petNameRegExp = /^[a-zA-Z]+$/

// set the port given or 8000 by default
app.set('port', process.env.PORT || 8500)

// we use regexp to avoid dependencies to validation libraries (like email validator) 
var emailRegExp = /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;
var nameRegExp = /^(([A-Za-z]+[\-\']?)*([A-Za-z]+)?\s)+([A-Za-z]+[\-\']?)*([A-Za-z]+)?$/;

var genre = {
    "Macho": 0,
    "Hembra": 1,
    "Otro": 2
}

// Middlewares:
app.use(cors());

app.listen(app.get('port'), () => {

    console.log(`Server on port ${app.get('port')}`);

});

app.post("/getProfileData", bodyParser.json(), async (req, res) => {

    const result = await Usuarios.findAll({
        include: [{
            include: [{
                include: [{
                    attributes: ['id_autonomia'],
                    model: Autonomias,
                    required: true
                }],
                attributes: ['id_provincia'],
                model: Provincias,
                required: true
            }],
            attributes: ['codine'],
            model: Municipios,
            required: true,
        }],
        attributes: ['nombre', 'email'],
        where: {
            id_usuario: req.body.id_usuario
        }
    })

    res.json({ status: true, data: result })

});

app.post("/getUsers", async (req, res) => {

    console.log("EY");

    const users = await Usuarios.findAll({

        attributes: ['nombre', 'email', 'id_usuario']

    });

    res.json({ status: true, data: users });

})

// get user and friends pets
app.post("/getPets", [bodyParser.json()], async (req, res) => {

    // get user email to find friends
    const user = req.body.id_usuario;

    /*
    // search for all friends
    Solicitudes.findAll({

        attributes: ['',''],
         {foreignKey: 'user_id'}
    })
    */


    // get all pets 
    const pets = await Mascotas.findAll({
        include: [
            {
                model: Razas
            },
            {
                model: Usuarios
            }
        ],
        attributes: ['nombre', 'Foto_perfil', 'edad', 'propietario', 'genero', 'id_raza', 'id_mascota'],
        where: {

            'propietario': user,

        }
    })

    var i = 0;


    // get profile photo
    pets.map((elem) => {

        var path = pets[i].Foto_perfil;
        var data = fs.readFileSync(path, { encoding: 'utf-8' })
        pets[i].Foto_perfil = data;

        i++;
    });

    res.json({ status: true, data: pets });

});

app.post("/modifyProfile", [bodyParser.json({ limit: '50mb' })], async (req, res) => {

    // server side field validation
    if (!nameRegExp.test(req.body.name)) {

        res.json({ status: "name" });

    } else if (!emailRegExp.test(req.body.email)) {

        res.json({ status: "badEmail" });

    } else {

        var idmuni = await Municipios.findAll({

            attributes: ['codine'],
            where: {

                codine: req.body.municipio

            }

        });

        if (idmuni.length == 0) {

            res.json({ status: "badMuni" });

        }

        // database register validation
        var isRegistred = await Usuarios.findAll({

            attributes: ['email'],
            where: {

                email: req.body.email,
                id_usuario: {
                    [op.ne]: req.body.id_usuario
                }

            }

        });

        if (isRegistred.length != 0) {

            res.json({ status: "emailFound" });

        } else {

            const user = await Usuarios.update({

                nombre: req.body.name,
                email: req.body.email,
                municipio: parseInt(req.body.municipio),

            }, {

                where: {

                    id_usuario: req.body.id_usuario

                }

            });

            const info = await Usuarios.findAll({

                attributes: ['nombre', 'email', 'id_usuario', 'Foto_perfil'],
                include: [
                    {
                        include: [{
                            include: [{
                                attributes: ['nombre'],
                                model: Autonomias,
                                required: true
                            }],
                            attributes: ['nombre'],
                            model: Provincias,
                            required: true
                        }],
                        attributes: ['nombre'],
                        model: Municipios,
                        required: true
                    }

                ],
                where: {
                    id_usuario: req.body.id_usuario
                }

            });

            res.json({ status: true, data: info });

            if (req.body.img != "#") {

                const path = await Usuarios.findAll({
                    attributes: ['Foto_perfil'],
                    where: {
                        id_usuario: req.body.id_usuario,
                    }
                });

                console.log(path);

                fs.writeFileSync(path[0].dataValues.Foto_perfil, req.body.img, (err) => {

                    console.log(err);

                });

            }

            res.json({ status: true });

        }

    }

});

app.post("/modifyPassword", bodyParser.json(), async (req, res) => {

    if (req.body.password != req.body.repeated) {

        res.json({ status: "rpassword" });

    } else if (req.body.password.length < 8) {

        res.json({ status: "password" });

    } else {

        const user = await Usuarios.findAll({
            attributes: ['password'],
            where: {
                id_usuario: req.body.id_usuario
            }
        })

        bcrypt.hash(req.body.password, saltRounds, async function (err, hash) {

            await Usuarios.update({

                password: hash

            }, {

                where: {

                    id_usuario: req.body.id_usuario

                }
            })


        });

        res.json({ status: true });


    }

});

app.post("/getPetsPosts", [bodyParser.json()], async (req, res) => {

    const posts = await Posts.findAll({

        include: [{
            model: Mascotas,
            required: true,
            where: {
                propietario: req.body.id_usuario,
            }
        }],
        order: [['createdAt', 'DESC']]
    });

    var dirname;
    var filePost;

    var realPosts = [];

    // filter posts depending on number of page demanded by user
    for (var i = 0; i < posts.length; i++) {

        posts[i].Mascota.Foto_perfil = fs.readFileSync(posts[i].Mascota.Foto_perfil, { encoding: 'utf-8' });

        dirname = posts[i].imagen.split("petImages/" + posts[i].Mascota.id_mascota + "/")[1];
        filePost = dirname.split('.txt')[0];

        //getting image from path
        posts[i].imagen = fs.readFileSync(posts[i].imagen, { encoding: 'utf-8' })
        realPosts.push(posts[i]);

    }



    res.json({ status: true, data: realPosts });

});

app.post("/sites/ca", async (req, res) => {

    const ca = await Autonomias.findAll({

        attributes: ['nombre', 'id_autonomia'],

    });

    var i = 0;
    ca.map((elem) => {

        ca[i] = elem.dataValues;
        i++;

    });

    res.send({ status: true, data: ca });

})

app.post("/sites/provincias", bodyParser.json(), async (req, res) => {

    console.log(req.body);

    const prov = await Provincias.findAll({

        attributes: ['nombre', 'id_provincia'],
        where: {

            id_autonomia: req.body.id_autonomia

        }

    });

    var i = 0;
    prov.map((elem) => {

        prov[i] = elem.dataValues;
        i++;

    });

    res.send({ status: true, data: prov });

})

app.post("/sites/municipios", bodyParser.json(), async (req, res) => {

    console.log(req.body);

    const muni = await Municipios.findAll({

        attributes: ['nombre', 'codine'],
        where: {

            id_provincia: req.body.id_provincia

        }

    });

    console.log(muni);

    var i = 0;
    muni.map((elem) => {

        muni[i] = elem.dataValues;
        i++;

    });

    console.log(muni);

    res.send({ status: true, data: muni });

});

app.post("/getBasicInfo", [bodyParser.json()], async (req, res) => {

    const pet = await Mascotas.findAll({

        include: [{

            include: [{

                attributes: ['id_animal'],
                model: Animales,
                required: true,

            }],

            attributes: ['id_raza'],
            model: Razas,
            required: true,

        }],

        attributes: ['nombre', 'edad', 'genero'],
        where: {

            id_mascota: req.body.id_mascota

        }

    });

    pet[0].genero = genre[pet[0].genero];

    res.json({ status: true, data: pet });

});

app.post("/type", async (req, res) => {

    const tipos = await Animales.findAll({

        attributes: ['nombre', 'id_animal']

    });

    var i = 0;
    tipos.map((elem) => {
        tipos[i] = elem.dataValues;
        i++;
    });

    res.json({ status: true, data: tipos });

})

app.post("/breeds", bodyParser.json(), async (req, res) => {

    const resultado = await Razas.findAll({

        attributes: ['nombre', 'id_raza'],
        where: {

            id_tipo: req.body.id_animal

        }

    });

    var i = 0;
    resultado.map((elem) => {
        resultado[i] = elem.dataValues;
        i++;
    });

    res.json({ status: true, data: resultado });

});

app.post("/modifyPetProfile", [bodyParser.json({ limit: '50mb' })], async (req, res) => {

    const now = new Date();
    const minPetYear = now.getUTCFullYear() - 60;

    if (!petNameRegExp.test(req.body.nombre)) {

        res.json({ status: "badName" });

    } else if (req.body.year == undefined || req.body.year == "" || parseInt(req.body.year) > now.getUTCFullYear() || parseInt(req.body.year) < minPetYear) {

        res.json({ status: "badYear" });

    } else if (parseInt(req.body.genre) != 1 && parseInt(req.body.genre) != 2 && parseInt(req.body.genre) != 3) {

        res.json({ status: "badGenre" });

    } else {

        const raza = await Razas.findAll({

            where: {
                id_raza: req.body.raza
            }

        })

        if (raza.length != 1) {

            res.json({ status: "badRaza" });

        } else {

            const pet = await Mascotas.update({

                nombre: req.body.nombre,
                edad: req.body.year,
                genero: req.body.genre,
                id_raza: req.body.raza,

            }, {

                where: {
                    id_mascota: req.body.id_mascota
                }

            })

            if (req.body.img != "#") {

                const path = await Mascotas.findAll({
                    attributes: ['Foto_perfil'],
                    where: {
                        id_mascota: req.body.id_mascota,
                    }
                });

                console.log(path);

                fs.writeFileSync(path[0].dataValues.Foto_perfil, req.body.img, (err) => {

                    console.log(err);

                });

            }

            res.json({ status: true });

        }

    }

});

app.post("/getFriends", [bodyParser.json()], async (req, res) => {

    var amigos = await Solicitudes.findAll({
        include: [{
            include: [{
                include: [{
                    include: [{
                        model: Autonomias,
                        attributes: ['nombre']
                    }],
                    model: Provincias,
                    attributes: ['nombre']
                }],
                model: Municipios,
                attributes: ['nombre']
            }],
            model: Usuarios,
            as: 'receptorTable',
            attributes: ['nombre', 'id_usuario', 'Foto_perfil']
        }, {
            include: [{
                include: [{
                    include: [{
                        model: Autonomias,
                        attributes: ['nombre']
                    }],
                    model: Provincias,
                    attributes: ['nombre']
                }],
                model: Municipios,
                attributes: ['nombre']
            }],
            model: Usuarios,
            as: 'emisorTable',
            attributes: ['nombre', 'id_usuario', 'Foto_perfil']
        }],
        where: {
            [op.or]: [{
                emisor: req.body.id_usuario
            }, {
                receptor: req.body.id_usuario
            }]
        }
    })

    //manage to avoid sending repeated and useless user data.
    var i = 0;
    while (i < amigos.length) {

        var tmp1 = amigos[i].id;
        var tmp2 = amigos[i].estado;
        var tmp3 = amigos[i].emisor;
        var tmp4 = amigos[i].receptor;
        var tmp5 = amigos[i].receptorTable;
        var tmp6 = amigos[i].emisorTable;

        amigos[i] = {
            id: tmp1,
            estado: tmp2,
            emisor: tmp3,
            receptor: tmp4,
            receptorTable: tmp5,
            emisorTable: tmp6
        }

        if (amigos[i].receptorTable.id_usuario == req.body.id_usuario) {

            amigos[i].receptorTable = {};

            amigos[i].emisorTable.Foto_perfil
            var path = amigos[i].emisorTable.Foto_perfil;
            var data = fs.readFileSync(path, { encoding: 'utf-8' })
            amigos[i].emisorTable.Foto_perfil = data;

        } else {

            amigos[i].emisorTable = {};

            amigos[i].receptorTable.Foto_perfil
            var path = amigos[i].receptorTable.Foto_perfil;
            var data = fs.readFileSync(path, { encoding: 'utf-8' })
            amigos[i].receptorTable.Foto_perfil = data;
        }

        i++;

    }


    res.json(amigos);

})

app.post("/deletePost", [bodyParser.json()], async (req, res) => {

    //COMPROBAR QUE EL ID DEL POST A ELIMINAR ES REALMENTE 
    const resultado = await Posts.findAll({
        include: [{
            model: Mascotas,
            where: {
                propietario: req.body.id_usuario,
            },
            required: true
        }],
        where: {
            id_post: req.body.id_post
        }
    })

    console.log(resultado);

    if (resultado.length == 1) {

        const path = await Posts.findAll({
            attributes: ['imagen'],
            where: {
                id_post: req.body.id_post
            }
        })

        fs.unlinkSync(path[0].dataValues.imagen);


        await Posts.destroy({
            where: {
                id_post: req.body.id_post
            }
        })


        res.json({ status: true });

    } else {

        res.json({ status: false });

    }

})

app.post("/deletePet", [bodyParser.json()], async (req, res) => {

    // CHECK IF USER IS THE PET OWNER
    const result = await Mascotas.findAll({
        where: {
            [op.and]: [{
                propietario: req.body.id_usuario
            }, {
                id_mascota: req.body.id_mascota
            }]
        }
    })

    if (result.length == 1) {

        var dirname = __dirname;
        dirname = dirname.replace("routes", "database");
        var dirname = dirname.concat("/petImages/" + req.body.id_mascota);

        fs.rmdirSync(dirname, { recursive: true });

        await Mascotas.destroy({
            where: {
                id_mascota: req.body.id_mascota
            }
        })

        res.json({ status: true });

    } else {

        res.json({ status: false, msg: "badOwner" });

    }

});

app.post("/deleteFriend", [bodyParser.json()], async (req, res) => {

    await Solicitudes.destroy({
        where: {
            [op.and]: [{
                estado: 'Aceptada'
            }, {
                [op.or]: [{
                    [op.and]: [{
                        emisor: req.body.id_usuario
                    }, {
                        receptor: req.body.amigo
                    }]
                }, {
                    [op.and]: [{
                        emisor: req.body.amigo
                    }, {
                        receptor: req.body.id_usuario
                    }]
                }]
            }]

        }
    })

    res.json({ status: true });

});

app.post("/deleteSession", [bodyParser.json()], async (req, res) => {

    const path = await Usuarios.findAll({
        attributes: ['Foto_perfil'],
        where: {
            id_usuario: req.body.id_usuario
        }
    })

    
    await Usuarios.destroy({
        where: {
            id_usuario: req.body.id_usuario
        }
    })

    fs.rmdirSync(path[0].dataValues.Foto_perfil.split("/profile.txt")[0], { recursive: true });

    res.json({ status: true })

});

