import PBNavbar from './PBNavbar';
import Modify from './Modify';
import Delete from './Delete';
import Main from './Main';
import { Route, Switch, useHistory } from 'react-router-dom';
import './styles/App.css';
import './styles/friends.css';
import './styles/index.css';
import './styles/profile.css';
import './styles/ProfileCol.css';
import './styles/table.css';
import './styles/pets.css';
import './styles/session.css';

function App() {


  return (
    <>
      <PBNavbar />

      <Switch>

        <Route path="/modify">
          <Modify />
        </Route>

        <Route path="/delete">
          <Delete />
        </Route>

        <Route path="/">
          <Main />
        </Route>

      </Switch>

    </>
  );
}

export default App;
