import Form from 'react-bootstrap/Form';
import { useEffect, useState } from 'react';



function BreedInput(props) {

    const [breedChecked, setBreedChecked] = useState(false);

    function getBreeds() {

        fetch('http://localhost:8500/breeds', {
    
            method: 'POST',
            headers: {
    
                "Content-Type": "application/json",
                'Accept': 'application/json'
    
            },
            body: JSON.stringify({
    
                "id_animal": document.getElementById('selectTipo').value,
    
            })
    
        }).then((res) => res.json()).then((res) => {
    
            console.log(res)
    
            var select = document.getElementById('selectBreed');
    
            // crearing previous breeds
            while (select && select.options.length > 1) {
                select.remove(select.options.length - 1);
            }
    
            // we would manage not ERR_CONNECTION_REFUSED or similar errors in register and login
            res.data.map((elem) => {
    
                select.options[select.options.length] = new Option(elem.nombre, elem.id_raza);
    
            })

            if (props.defaultBreed != 0 && select.options.length > props.defaultBreed && !breedChecked) {

                setBreedChecked(true);

                select.options[props.defaultBreed].selected = 'selected';

            }
    
    
        })
    
    }

    useEffect(() => {

        fetch('http://localhost:8500/type', {

            method: 'POST',

        }).then((res) => res.json()).then((res) => {

            console.log(res)

            var select = document.getElementById('selectTipo');
            // we would manage not ERR_CONNECTION_REFUSED or similar errors in register and login
            res.data.map((elem) => {
                
                select.options[select.options.length] = new Option(elem.nombre, elem.id_animal);

                
            })

            console.log(select.options);
            console.log(props.defaultType);
            console.log(select.options.length);


            if (props.defaultType != 0 && select.options.length > props.defaultType) {

                select.options[props.defaultType].selected = 'selected';
                getBreeds();

            }


        })

    },[props.defaultType]);


    return (
        <>
            <Form.Group id="exampleForm.SelectCustomSizeSm">
                <div className="input-label">Tipo de animal</div>
                <Form.Control calssName="my-1 mr-sm-2" id="selectTipo" as="select" onChange={(e) => {props.setBreed(0);getBreeds()}} custom>
                    <option value="0"> Elija un tipo de animal</option>
                </Form.Control>
            </Form.Group>

            <Form.Group id="exampleForm.SelectCustomSizeSm">
                <div className="input-label">Raza/Especie de animal</div>
                <Form.Control className={props.isBreedValid.valid ? "my-1 mr-sm-2" :  "my-1 mr-sm-2 is-invalid"} onChange={e => {props.setBreed(e.target.value); props.setBreedValid({valid: true, msg: ""})}} id="selectBreed" as="select" custom>
                    <option value="0"> Elija un raza o especie</option>
                </Form.Control>
                <div class="invalid-feedback">
                    {props.isBreedValid.msg}
                </div>
            </Form.Group>
        </>

    )

}

export default BreedInput;