import { useState, useRef } from "react";
import Cropper from "react-cropper";
import "cropperjs/dist/cropper.css";
import "./styles/pets.css"
import Form from "react-bootstrap/Form";
import Button from 'react-bootstrap/Button';

function CropperFunc(props) {

    const [image, setImage] = useState(null);
    const cropper = useRef();
    const onChange = (e) => {
        e.preventDefault();
        props.setValid({ status: true, msg: "" });
        let files;
        if (e.dataTransfer) {
            files = e.dataTransfer.files;
        } else if (e.target) {
            files = e.target.files;
        }
        const reader = new FileReader();
        reader.onload = () => {
            
            setImage(reader.result);
        };
        reader.readAsDataURL(files[0]);
    };

    const getCropData = () => {

        if (typeof cropper.current !== "undefined") {


            if (cropper.current.getCroppedCanvas() == null) {

                alert("FORMATO INCORRECTO");
                props.setValid({ status: false, msg: "El formato del archivo es incorrecto" });

            } else {

                props.setImage(cropper.current.getCroppedCanvas().toDataURL());

            }


        }
    };

    return (
        <>
            <div style={{ width: "100%" }}>
                <Form.Label style={{ fontWeight: "bold" }}>{props.titulo}</Form.Label>
                {props.descripcion && 
                <>
                <br/>
                {props.descripcion}
                <br/>
                </>}
                <br></br>
                <input type="file" onChange={onChange} />
                <br />
                <br />
                {image &&
                    <>
                    
                        <Cropper
                            style={{ height: 200, width: "100%" }}
                            initialAspectRatio={1}
                            aspectRatio={1}
                            preview=".img-preview"
                            src={image}
                            viewMode={1}
                            guides={true}
                            minCropBoxHeight={10}
                            minCropBoxWidth={10}
                            background={false}
                            responsive={true}
                            autoCropArea={1}
                            checkOrientation={false} // https://github.com/fengyuanchen/cropperjs/issues/671
                            onInitialized={(instance) => {
                                cropper.current = instance;
                            }} />

                        <div className="row">
                            <div className="box col" style={{ width: "50%", float: "right" }}>
                                <Form.Label style={{ fontWeight: "bold" }}>Previsualización</Form.Label>
                                <div
                                    className="img-preview"
                                    style={{ width: "218px", float: "left", height: "218px" }}
                                />
                            </div>
                            <div
                                className="box col"
                                style={{ width: "50%", float: "right", height: "203px" }}
                            >
                                <Form.Label style={{ fontWeight: "bold" }}>Foto seleccionada</Form.Label>
                                <img style={{ width: "100%" }} src={props.cropData} alt="cropped" />
                            </div>
                        </div>

                        <div className="invalid-feedback" style={ {display: "block", padding: "5px", margin: "5px" } }>
                            {props.valid.msg}
                        </div>

                        <div className="row">
                            <Button variant="primary" style={{ alignSelf: "center", marginLeft: "20px" }} onClick={() => {getCropData(); if (props.onChange) { props.onChange() }}}>
                                Selecciona imagen
                            </Button>
                        </div>
                        <br style={{ clear: "both" }} />

                    </>
                }
            </div>
        </>

    );
};

export default CropperFunc;