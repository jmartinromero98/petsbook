import { useEffect, useState } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ListGroup from 'react-bootstrap/ListGroup';

import { useHistory } from 'react-router-dom';

import './styles/table.css';

function Main() {

    const history = useHistory();

    const [users, setUsers] = useState([]);
    const [option, setOption] = useState("modify");

    useEffect(() => {

        // if cookie send it and check its vality
        fetch('http://localhost:8500/getUsers', {

            method: 'POST',

        }).then((res) => res.json()).then((res) => {

            if (res.status == true) {

                setUsers(res.data);

            }

        });



    }, []);

    return (
        <>
            <Row>
                <Col sm={4}>
                    <ListGroup className="pb-session-modifymenu" style={{marginTop: "50px", padding: "20px"}}>
                        <ListGroup.Item style={option == "modify" ? { background: "gray" } : { background: "white" }} onClick={() => { setOption("modify") }}>
                            Modificar datos
                            </ListGroup.Item>
                        <ListGroup.Item  style={option == "delete" ? { background: "gray" } : { background: "white" }} onClick={() => { setOption("delete") }}>
                            Eliminar datos
                        </ListGroup.Item >
                    </ListGroup>
                </Col>
                    <Col sm={8}>
                        <Col className="pb-adminmain-col" md={10}>
                            <table style={{ marginTop: "50px" }}>
                                <thead>
                                    <tr class="table100-head">
                                        <th class="column1">ID usuario</th>
                                        <th class="column2">Nombre</th>
                                        <th class="column3">Email</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    {users.map((user) => {

                                        console.log(option);

                                        return (
                                            <tr onClick={() => {history.push(option+"?id="+user.id_usuario)}}>
                                                <td class="column1">{user.id_usuario}</td>
                                                <td class="column2">{user.nombre}</td>
                                                <td class="column3">{user.email}</td>
                                            </tr>
                                        )


                                    })}
                                </tbody>
                            </table>
                        </Col>
                    </Col>
            </Row>
        </>
    )

}

export default Main;