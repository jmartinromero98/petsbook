import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import ListGroup from 'react-bootstrap/ListGroup';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import FormInput from './FormInput';
import BreedInput from './BreedInput';

import { useState, useEffect, useContext } from 'react';

import Cropper from './Cropper';

import requestHelper from './RequestHelper';

function Modify() {

    const [page, setPage] = useState("cuenta");
    const [mascotas, setMascotas] = useState([]);
    const [show, setShow] = useState(false);

    // form states to get input values
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");

    const [auto, setAuto] = useState([]);
    const [prov, setProv] = useState([]);
    const [muni, setMuni] = useState([]);

    const [autoSel, setAutoSel] = useState([]);
    const [provSel, setProvSel] = useState([]);
    const [muniSel, setMuniSel] = useState([]);

    // card states used by inputs to display themselves
    const [isEmailValid, setEmailValid] = useState({ valid: true, msg: "" });
    const [isNameValid, setNameValid] = useState({ valid: true, msg: "" });
    const [isMunicipioValid, setMunicipioValid] = useState({ valid: true, msg: "" });

    // password
    const [currentPass, setCurrentPass] = useState();
    const [newPass, setNewPass] = useState();
    const [repeatedPass, setRepeatedPass] = useState();


    const [isNewPassValid, setNewPassValid] = useState({ valid: true, msg: "" });
    const [isRepeatedPassValid, setRepeatedPassValid] = useState({ valid: true, msg: "" });

    //hooks of modify pet fields
    const [petName, setPetName] = useState("");
    const [year, setYear] = useState("");
    const [genre, setGenre] = useState(-1);
    const [cropData, setCropData] = useState("#");
    const [animal, setAnimal] = useState(0);
    const [breed, setBreed] = useState(0);
    const [idMascota, setIdMascota] = useState(-1);

    const [isLoading, setLoading] = useState(false);

    const [cropDataProfile, setCropDataProfile] = useState("#");
    const [isCropProfileValid, setCropProfileValid] = useState({ valid: true, msg: "" });

    var url_string = window.location.href
    var url = new URL(url_string);
    var id = url.searchParams.get("id")

    const [isImageValid, setImageValid] = useState({ valid: true, msg: "" });
    const [isPetNameValid, setPetNameValid] = useState({ valid: true, msg: "" });
    const [isGenreValid, setGenreValid] = useState({ valid: true, msg: "" });
    const [isBreedValid, setBreedValid] = useState({ valid: true, msg: "" });
    const [isYearValid, setYearValid] = useState({ valid: true, msg: "" });
    const [isCropValid, setCropValid] = useState({ valid: true, msg: "" });

    const [checked, isChecked] = useState(false);

    // hooks for modal
    const handleClose = () => {

        setShow(false);
        setPetNameValid({ valid: true, msg: "" });
        setGenreValid({ valid: true, msg: "" });
        setBreedValid({ valid: true, msg: "" });
        setYearValid({ valid: true, msg: "" });
        setCropValid({ valid: true, msg: "" });
        setImageValid({ valid: true, msg: "" });
        isChecked(false);

    };
    const handleShow = () => setShow(true);
    const handleCrop = (i) => setCropData(i);
    const handleCropValid = (i) => setCropValid(i);


    useEffect(() => {

        isChecked(false);

        if (page == "cuenta") {

            getUserInfo();

            requestHelper.post('/sites/ca', {}, false).then((res) => {

                setAuto(res.data);

            });

            setNewPass("");
            setRepeatedPass("");

        }

        if (page == "mascotas") {

            getPets();

        }

    }, [page]);

    function getUserInfo() {

        // getting friends
        requestHelper.post('/getProfileData', {

            "id_usuario": id

        }, true).then((res) => {

            if (res.status == true) {

                setName(res.data[0].nombre);
                setEmail(res.data[0].email);

                setAutoSel(res.data[0].Municipio.Provincia.Autonomia.id_autonomia);
                setProvSel(res.data[0].Municipio.Provincia.id_provincia);
                setMuniSel(res.data[0].Municipio.codine);

            }

        });

    }

    function onSubmit(e) {

        e.preventDefault();

        if (page == "cuenta") {

            requestHelper.post('/modifyProfile', {

                img: cropDataProfile,
                name: name,
                email: email,
                municipio: muniSel,
                id_usuario: id,

            }, true).then((res) => {

                if (res.status == "name") {

                    setNameValid({ valid: false, msg: "El formato del nombre no es correcto" });

                }

                if (res.status == "badEmail") {

                    setEmailValid({ valid: false, msg: "El formato del correo no es correcto" });

                }

                if (res.status == "emailFound") {

                    setEmailValid({ valid: false, msg: "El correo ya tiene una cuenta asignada." });

                }

                if (res.status == "badMuni") {

                    setMunicipioValid({ valid: false, msg: "El municipio seleccionado no es correcto" });

                }

                if (res.status == true) {

                    isChecked(true);
                    getUserInfo();

                }

            });

        }

        if (page == "contraseña") {

            setLoading(true);

            requestHelper.post('/modifyPassword', {

                id_usuario: id,
                password: newPass,
                repeated: repeatedPass,

            }, true).then((res) => {

                setLoading(false);

                if (res.status == "password") {

                    setNewPassValid({ valid: false, msg: "La contraseña debe contar con al menos 8 carácteres." });

                }

                if (res.status == "rpassword") {

                    setRepeatedPassValid({ valid: false, msg: "Las contraseñas no son coincidentes" });

                }

                if (res.status == true) {

                    isChecked(true);

                }

            });

        }

        if (page == "mascotas") {

            requestHelper.post('/modifyPetProfile', {

                nombre: petName,
                year: year,
                raza: breed,
                genre: genre,
                id_mascota: idMascota,
                img: cropData

            }, true).then((res) => {

                if (res.status == "badName") {

                    setPetNameValid({ valid: false, msg: "El formato del nombre de la mascota es incorrecto." });

                }

                if (res.status == "badYear") {

                    setYearValid({ valid: false, msg: "El año de nacimiento de la mascota es incorrecto." });

                }

                if (res.status == "badRaza") {

                    setBreedValid({ valid: false, msg: "La raza o especie del animal es incorrecta" });

                }

                if (res.status == "badGenre") {

                    setGenreValid({ valid: false, msg: "El género introducido es incorrecto." });

                }

                if (res.status == true) {

                    isChecked(true);
                    getPets();

                }

            });

        }
    }

    function getPets() {
        // getting pets

        requestHelper.post('/getPets', {

            "id_usuario": id

        }, true).then((res) => {

            if (res.status == true) {

                setMascotas(res.data);

            }

        });
    }

    function getPetProfile(e) {

        // getting pet profile
        requestHelper.post('/getBasicInfo', { id_mascota: e }, true).then((res) => {

            if (res.status == true) {

                setPetName(res.data[0].nombre);
                setYear(res.data[0].edad);
                setGenre(res.data[0].genero + 1);
                setBreed(res.data[0].Raza.id_raza);
                setAnimal(res.data[0].Raza.Animale.id_animal);
            }

        });

    }

    function getProvinces() {

        requestHelper.post('/sites/provincias', {

            "id_autonomia": autoSel,

        }, false).then((res) => {

            setProv(res.data);

        })

    }

    useEffect(() => {

        getProvinces()

    }, [autoSel]);

    function getMunicipios() {

        requestHelper.post('/sites/municipios', {

            "id_provincia": provSel

        }, false).then((res) => {

            setMuni(res.data);

        })

    }

    useEffect(() => {

        getMunicipios()

    }, [provSel]);



    return (
        <>
            <Row>
                <Col sm={4} style={{ paddingLeft: "30px", paddingRight: "30px" }}>
                    <ListGroup className="pb-session-modifymenu">
                        <ListGroup.Item style={page == "cuenta" ? { background: "gray" } : { background: "white" }} onClick={() => { setPage("cuenta") }}>
                            Cuenta
                        </ListGroup.Item >
                        <ListGroup.Item style={page == "mascotas" ? { background: "gray" } : { background: "white" }} onClick={() => { setPage("mascotas") }}>
                            Mascotas
                        </ListGroup.Item>
                        <ListGroup.Item style={page == "contraseña" ? { background: "gray" } : { background: "white" }} onClick={() => { setPage("contraseña") }}>
                            Contraseña
                        </ListGroup.Item>
                    </ListGroup>
                </Col>
                <Col sm={8} >
                    <div style={{ paddingTop: "50px" }}></div>
                    {page == "cuenta" &&
                        <>
                            <Col md={10} className="mx-auto col">
                                <form className="myform form" onSubmit={onSubmit} name="a" noValidate="noValidate">
                                    <div className="col-md-12 text-center">
                                        <h1 className="pb-sessioncard-title">
                                            Tu perfil
                                        </h1>
                                    </div>
                                    <Cropper
                                        titulo={"Foto de perfil"}
                                        descripcion={"Añade una nueva foto de perfil si deseas cambiar la actual, de lo contrario, no selecciones ningún archivo."}
                                        cropData={cropDataProfile}
                                        setImage={(i) => setCropDataProfile(i)}
                                        valid={isCropProfileValid}
                                        setValid={(i) => setCropProfileValid(i)}
                                        onChange={() => { isChecked(false) }}
                                    />
                                    <FormInput
                                        value={name}
                                        onChange={(e) => { isChecked(false); setName(e.target.value); setNameValid({ valid: true, msg: "" }) }}
                                        valid={isNameValid.valid}
                                        msg={isNameValid.msg}
                                        type="text"
                                        name="su nombre"
                                        id="name"
                                        label="Nombre" />

                                    <FormInput
                                        value={email}
                                        onChange={(e) => { isChecked(false); setEmail(e.target.value); setEmailValid({ valid: true, msg: "" }) }}
                                        valid={isEmailValid.valid}
                                        msg={isEmailValid.msg}
                                        type="email"
                                        name="su email"
                                        id="emailInput"
                                        label="Email"
                                    />

                                    <Form.Group id="exampleForm.SelectCustomSizeSm">
                                        <div className="input-label">Comunidad autonoma</div>
                                        <Form.Control className="my-1 mr-sm-2" id="selectCA" as="select" onChange={(e) => { isChecked(false); setAutoSel(e.target.value) }} custom>
                                            <option value="0"> Elija una comunidad autonoma...</option>
                                            {auto.map((auto, idx) => {
                                                return (
                                                    <option value={auto.id_autonomia} selected={auto.id_autonomia == autoSel} key={idx}>{auto.nombre}</option>
                                                )
                                            })}
                                        </Form.Control>
                                    </Form.Group>

                                    <Form.Group id="exampleForm.SelectCustomSizeSm">
                                        <div className="input-label">Provincia</div>
                                        <Form.Control as="select" id="selectProv" onChange={(e) => { isChecked(false); setProvSel(e.target.value) }} custom>
                                            <option value="0"> Elija una provincia...</option>
                                            {prov.map((prov, idx) => {
                                                return (
                                                    <option value={prov.id_provincia} selected={prov.id_provincia == provSel} key={idx}>{prov.nombre}</option>
                                                )
                                            })
                                            }
                                        </Form.Control>
                                    </Form.Group>

                                    <Form.Group id="exampleForm.SelectCustomSizeSm">
                                        <div className="input-label">Municipio</div>
                                        <Form.Control as="select" id="selectMuni" onChange={(e) => { isChecked(false); setMuniSel(e.target.value); setMunicipioValid({ valid: true, msg: "" }) }} className={isMunicipioValid.valid ? "" : "is-invalid"} custom>
                                            <option value="0">Elija un municipio...</option>
                                            {muni.map((muni, idx) => {
                                                return (
                                                    <option value={muni.codine} selected={muni.codine == muniSel} key={idx}>{muni.nombre}</option>
                                                )
                                            })}
                                        </Form.Control>
                                        <div className="invalid-feedback">
                                            {isMunicipioValid.msg}
                                        </div>
                                    </Form.Group>


                                    <div className="ml-auto">
                                        <div className="text-right">
                                            <button className="btn btn-success" type="submit" checked={checked} disabled={checked} >
                                                {checked ? "Guardado" : "Guardar"}
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </Col>
                        </>
                    }
                    <ul className="list-group list-group-flush">
                        {page == "mascotas" &&

                            mascotas.map((elem, idx) => {
                                return (
                                    <li className="list-group-item" key="idx">
                                        <div className="row pb-pets-petlistcont"
                                            onClick={(e) => { setIdMascota(elem.id_mascota); getPetProfile(elem.id_mascota); setShow(true); }}>
                                            <div className="col-2 pb-pets-petlistimgcol">
                                                <img className="pb-pets-petlistimg" src={elem.Foto_perfil} alt="" >
                                                </img>
                                            </div>
                                            <div className="col-10 pb-pets-petlistcol">
                                                <Row style={{ display: "inline" }}>
                                                    <div className="row pb-pets-petlistname">
                                                        {elem.nombre}
                                                    </div>
                                                    <div className="row">
                                                        {elem.Raza.nombre}
                                                    </div>
                                                    <div className="row">
                                                        {elem.genero}
                                                    </div>
                                                </Row>
                                            </div>
                                            <div className="col pb-pets-petlistcol">
                                            </div>
                                        </div>
                                    </li>
                                )
                            })

                        }
                    </ul>
                    {page == "contraseña" &&
                        <>
                            <Col md={10} className="mx-auto col">
                                <form className="myform form" onSubmit={onSubmit} name="b" noValidate="noValidate">

                                    <FormInput
                                        value={newPass}
                                        onChange={(e) => { setNewPass(e.target.value); setNewPassValid({ valid: true, msg: "" }) }}
                                        valid={isNewPassValid.valid}
                                        msg={isNewPassValid.msg}
                                        type="password"
                                        name="la nueva contraseña"
                                        id="nueva"
                                        label="Nueva contraseña" />

                                    <FormInput
                                        value={repeatedPass}
                                        onChange={(e) => { setRepeatedPass(e.target.value); setRepeatedPassValid({ valid: true, msg: "" }) }}
                                        valid={isRepeatedPassValid.valid}
                                        msg={isRepeatedPassValid.msg}
                                        type="password"
                                        name="la nueva contraseña"
                                        id="repeated"
                                        label="Nueva contraseña" />

                                    <div className="ml-auto">
                                        <div className="text-right">
                                            <button className="btn btn-success" disabled={isLoading || checked} checked={checked}>
                                                {isLoading && <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>}
                                                {!isLoading && !checked && "Guardar"}
                                                {isLoading && !checked && "Guardando..."}
                                                {checked && "Guardado"}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </Col>
                        </>

                    }
                    <Modal
                        show={show}
                        onHide={handleClose}
                        keyboard={false}
                    >
                        <form onSubmit={onSubmit} name="petRegister" noValidate="noValidate">
                            <Modal.Header closeButton>
                                <Modal.Title>
                                    Modificar mascota
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <Cropper
                                    titulo={"Foto de perfil"}
                                    descripcion={"Añade una nueva foto de perfil si deseas cambiar la actual, de lo contrario, no selecciones ningún archivo."}
                                    cropData={cropData}
                                    setImage={handleCrop}
                                    valid={isCropValid}
                                    setValid={handleCropValid}
                                    onChange={() => { isChecked(false) }}

                                />
                                {isImageValid.msg != "" && <span className="pb-fakeinvalid">Debes elegir una imagen en un formato válido</span>}
                                <FormInput
                                    valid={isPetNameValid.valid}
                                    onChange={(e) => { setPetName(e.target.value); setPetNameValid({ valid: true, msg: "" }) }}
                                    value={petName}
                                    label="Nombre de la mascota"
                                    type="text"
                                    msg={isPetNameValid.msg}
                                    id="#forumTitle"
                                    name="el nombre de la mascota"
                                    maxlength="100" />

                                <FormInput
                                    valid={isYearValid.valid}
                                    msg={isYearValid.msg}
                                    onChange={(e) => { setYear(e.target.value); setYearValid({ valid: true, msg: "" }); isChecked(false) }}
                                    value={year}
                                    label="Año de nacimiento"
                                    type="number"
                                    id="#forumTitle"
                                    name="el año de nacimiento de la mascota"
                                    maxlength="100" />

                                <BreedInput isBreedValid={isBreedValid} setBreed={setBreed} defaultType={animal} defaultBreed={breed} setBreedValid={setBreedValid} onChange={() => { isChecked(false) }}
                                />

                                <Form.Label style={{ fontWeight: "bold" }}>Género</Form.Label>

                                <Form.Control onChange={e => { setGenre(e.target.value); setGenreValid({ valid: true, msg: "" }); }} className={isGenreValid.valid ? "my-1 mr-sm-2" : "my-1 mr-sm-2 is-invalid"} id="selectGenre" as="select" value={genre} custom>
                                    <option value="0" key="0"> Elija un género del animal</option>
                                    <option value="1" key="1"> Macho </option>
                                    <option value="2" key="2"> Hembra </option>
                                    <option value="3" key="3"> Otro </option>
                                </Form.Control>
                                <div className="invalid-feedback">
                                    {isGenreValid.msg}
                                </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={handleClose}>
                                    Cancelar
                                </Button>
                                <button id="actionButton" type="submit" className=" btn btn-success" disabled={isLoading || checked} checked={checked}>
                                    {checked ? "Guardado" : "Guardar"}
                                </button>
                            </Modal.Footer>
                        </form>
                    </Modal>
                </Col>
            </Row>
        </>
    )

}

export default Modify;