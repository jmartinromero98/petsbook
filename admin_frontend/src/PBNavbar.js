import Navbar from 'react-bootstrap/Navbar';
import Row from 'react-bootstrap/Row';
import { useHistory } from 'react-router-dom';

function PBNavbar() {

    const history = useHistory();

    return (
        <>
            <Navbar className="navbar-dark bg-dark shadow-lg sticky-top" expand="lg" bg="dark" id="pb-navbar" style={{ zIndex: "20" }}>
                <Row className="pb-navbar-row">
                    <div className="d-flex justify-content-between" id="pb-navbar-flex">
                        <a className="navbar-brand" onClick={(e) => { history.push("/") }}>
                            <div className="p-1 pb-leftnavbar-container align-items-center" id="pb-logo-1">
                                <img id="pb-navbar-logo" src="img/logo.svg"></img>

                                <span className="Camila">PetsBook</span>

                            </div>
                        </a>
                    </div>
                </Row>
            </Navbar>
        </>
    )
}

export default PBNavbar;