import Form from 'react-bootstrap/Form';
import { useState, useEffect } from 'react';

function SitesInput(props) {

    const [peticion, setPeticion] = useState(false);
    const [options, setOptions] = useState([]);

    const [autoChecked, setAutoChecked] = useState(false);
    const [provChecked, setProvChecked] = useState(false);
    const [muniChecked, setMuniChecked] = useState(false);

    useEffect(() => {

        if (!peticion) {

            setPeticion(true);

            fetch('http://localhost:8000/session/sites/ca', {

                method: 'POST',
            }).then((res) => res.json()).then((res) => {

                console.log(props.auto.current)
                // we would manage not ERR_CONNECTION_REFUSED or similar errors in register and login

                setOptions(res.data);
                console.log(res.data);

            })

        } else {

            options.map((elem) => {
                props.auto.current.options[props.auto.current.options.length] = new Option(elem.nombre, elem.id_autonomia);
            })

            console.log("JAUME's PRINT", options);

            if (props.defaultAuto && !autoChecked && props.auto.current.options.length > 1) {
                props.auto.current.options[props.defaultAuto].selected = 'selected';
                getProvinces();
                //setAutoChecked(false);
            }

        }

    }, [props.auto, peticion, options]);

    function getProvinces() {

        fetch('http://localhost:8000/session/sites/provincias', {

            method: 'POST',
            headers: {

                "Content-Type": "application/json",
                'Accept': 'application/json'

            },
            body: JSON.stringify({

                "id_autonomia": props.auto.current.value,

            })
        }).then((res) => res.json()).then((res) => {

            console.log(res)

            // crearing previous provinces
            while (props.muni.current && props.muni.current.options.length > 1) {
                props.muni.current.remove(props.muni.current.options.length - 1);
            }

            // clearing previous provinces
            while (props.prov.current && props.prov.current.options.length > 1) {
                props.prov.current.remove(props.prov.current.options.length - 1);
            }
            if (props.prov != null && props.prov.current != null) {

                // we would manage not ERR_CONNECTION_REFUSED or similar errors in register and login
                res.data.map((elem) => {
                    props.prov.current.options[props.prov.current.options.length] = new Option(elem.nombre, elem.id_provincia);
                })

                if (props.defaultProv && !provChecked) {
                    for (let elem of props.prov.current.getElementsByTagName('option')) {
                        if (elem.value == props.defaultProv) {
                            elem.selected = "selected";
                        }
                    }
                    getMunicipios();
                    //setProvChecked(false);
                }

            }


        })
    }

    function getMunicipios() {

        fetch('http://localhost:8000/session/sites/municipios', {

            method: 'POST',
            headers: {

                "Content-Type": "application/json",
                'Accept': 'application/json'

            },
            body: JSON.stringify({

                "id_provincia": props.prov.current.value,

            })
        }).then((res) => res.json()).then((res) => {

            console.log(res)

            // crearing previous provinces
            while (props.muni.current && props.muni.current.options.length > 1) {
                props.muni.current.remove(props.muni.current.options.length - 1);
            }

            if (props.muni.current != null) {

                // we would manage not ERR_CONNECTION_REFUSED or similar errors in register and login
                res.data.map((elem) => {

                    props.muni.current.options[props.muni.current.options.length] = new Option(elem.nombre, elem.codine);

                })

                if (props.defaultMuni && !muniChecked) {
                    for (let elem of props.muni.current.getElementsByTagName('option')) {
                        if (elem.value == props.defaultMuni) {
                            elem.selected = "selected";
                        }
                    }
                    //setProvChecked(false);
                }

            }



        })

    }

    return (

        <>
            <Form.Group id="exampleForm.SelectCustomSizeSm">
                <div className="input-label">Comunidad autonoma</div>
                <Form.Control calssName="my-1 mr-sm-2" id="selectCA" ref={props.auto} as="select" onChange={getProvinces} custom>
                    <option value="0"> Elija una comunidad autonoma...</option>
                </Form.Control>
            </Form.Group>

            <Form.Group id="exampleForm.SelectCustomSizeSm">
                <div className="input-label">Provincia</div>
                <Form.Control as="select" id="selectProv" ref={props.prov} onChange={getMunicipios} custom>
                    <option value="0"> Elija una provincia...</option>
                </Form.Control>
            </Form.Group>

            <Form.Group id="exampleForm.SelectCustomSizeSm">
                <div className="input-label">Municipio</div>
                <Form.Control as="select" id="selectMuni" ref={props.muni} onChange={() => props.setMunicipioValid({ valid: true, msg: "" })} className={props.isMunicipioValid.valid ? "" : "is-invalid"} custom>
                    <option value="0">Elija un municipio...</option>
                </Form.Control>
                <div class="invalid-feedback">
                    {props.isMunicipioValid.msg}
                </div>
            </Form.Group>
        </>

    )
}

export default SitesInput;