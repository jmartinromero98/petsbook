const { Sequelize, DataTypes } = require('../../common/node_modules/sequelize');

const sequelize = new Sequelize('petsbook', 'root', '1234', {
    host: 'localhost',
    dialect: 'mysql',/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});

const Usuarios = require('../../common/models/Usuarios');
const Autonomias = require('../../common/models/Autonomias');
const Provincias = require('../../common/models/Provincias');
const Municipios = require('../../common/models/Municipios');
const Animales = require('../../common/models/Animales');
const Comentarios = require('../../common/models/Comentarios');
const Mascotas = require('../../common/models/Mascotas');
const Mensajes = require('../../common/models/Mensajes');
const Posts = require('../../common/models/Posts');
const Razas = require('../../common/models/Razas');
const Solicitudes = require('../../common/models/Solicitudes');
const Ubicaciones = require('../../common/models/Ubicaciones');
const ComentariosUbis = require('../../common/models/ComentariosUbis');
const Asociaciones = require('../../common/models/Asociaciones');

var animales = [
    "Perro",
    "Gato",
    "Ave",
    "Conejo",
    "Roedor",
    "Hurón",
    "Pez",
    "Caballo",
    "Anuro",
    "Reptil",
    "Arácnido",
    "Otro"
];

var perros = [
    "‍Mestizo / sin raza",
    "‍Otra / no disponible",
    "Affenpinscher",
    "Afgano",
    "Aidi",
    "Akita Americano",
    "Akita Japonés",
    "Alano Español",
    "Basenji",
    "Basset Hound",
    "Beagle",
    "Beagle Harrier",
    "Bedlington Terrier",
    "Berger de Picardie",
    "Bichón Boloñés",
    "Bichon Frise",
    "Bichón Habanero",
    "BloodHound",
    "Bobtail",
    "Borzoi",
    "Boston Terrier",
    "Boxer",
    "Boyero Australiano",
    "Bretón Spaniel",
    "Briard",
    "Bull Terrier",
    "Bulldog",
    "Bullmastín",
    "Cairn Terrier",
    "Caniche",
    "Caniche Miniatura",
    "Caniche Toy",
    "Carlino",
    "Cavalier King Charles",
    "Chesapeake Bay Retriever",
    "Chihuahua",
    "Chin Japones",
    "Chow Chow",
    "Cocker Spaniel Americano",
    "Cocker Spaniel Inglés",
    "Collie",
    "Collie Barbudo",
    "Collie de la Frontera",
    "Corgi Galés Cárdigan",
    "Crestado Chino",
    "Dachshund",
    "Dalmata",
    "Dandie Dinmont Terrier",
    "Deerhound Escocés",
    "Deutsch Drahthaar",
    "Doberman",
    "Dogo Argentino",
    "Elkhound Noruego",
    "Faraón Hound",
    "Field Spaniel",
    "Fila Brasilero",
    "Fox Terrier",
    "Golden Retriever",
    "Gran Danés",
    "Grifón de Bruselas",
    "Hamilton Stovare",
    "Husky Siberiano",
    "Jack Russell Terrier",
    "Keeshond",
    "Labrador Retriever",
    "Lhasa Apso",
    "Lobero o Lebrel irlandés",
    "Malamute de Alaska",
    "Maltes",
    "Mastín Español",
    "Mastin Napolitano",
    "Mastin Pirineos",
    "Mastín Tibetano",
    "Otterhound",
    "Pastor Alemán",
    "Pastor Autraliano",
    "Pastor Belga",
    "Pastor Belga Laekenois",
    "Pastor Belga Malinois",
    "Pastor Belga Tervueren",
    "Pastor de Anatolia",
    "Pastor de Shetland",
    "Pequinés",
    "Perro de aguas",
    "Perro de Canaan",
    "Pinscher Miniatura",
    "Pitbull Terrier Americano",
    "Podenco Ibicenco",
    "Pointer",
    "Pomerano",
    "Pug",
    "Ridgeback de Rodesia",
    "Rottweiler",
    "Saluki",
    "Samoyedo",
    "San Bernardo",
    "Schnauzer Gigante",
    "Schnauzer Miniatura",
    "Setter Ingles",
    "Setter Irlandes",
    "Shar Pei",
    "Shiba Inu",
    "Shih Tzu",
    "Spaniel Tibetano",
    "Spitz Finlandés",
    "Springer Spaniel Galés",
    "Springer Spaniel Inglés",
    "Terranova",
    "Terrier de Airedale",
    "Terrier de Norfolk",
    "Terrier de Skye",
    "Vizsla",
    "Weimaraner",
    "Whippet",
    "Wolfhound Irlandés",
    "Yorkshire Terrier"
];
var gatos = [
    "‍Mestizo / sin raza",
    "‍Otra / no disponible",
    "Abisinio",
    "Africano",
    "American Curl",
    "Angora Turco",
    "Azul Ruso",
    "Balinés",
    "Bengali",
    "Bobtail japonés",
    "Bombay",
    "Bosque de Noruega",
    "Británico de pelo corto",
    "Británico de Pelo Corto Azul",
    "Británico de pelo largo",
    "Burmés",
    "Burmilla",
    "California Spangled",
    "Cartujo",
    "Cornish Rex",
    "Devon Rex",
    "Europeo de Pelo Corto",
    "Exótico de pelo corto",
    "German Rex",
    "Habana brown",
    "Highland Fold",
    "Himalayo",
    "Javanés",
    "Korat",
    "Maine Coon",
    "Manx",
    "Mau Egipcio",
    "Munchkin",
    "Neva Masquerade",
    "Ocicat",
    "Oriental de Pelo Corto",
    "Oriental de Pelo Largo",
    "Persa",
    "Peterbald",
    "Ragamuffin",
    "Ragdoll",
    "Sagrado de Birmania",
    "Scottish Fold",
    "Selkirk Rex",
    "Serengeti",
    "Seychellois",
    "Siamés",
    "Siamés Moderno",
    "Siamés Tradicional",
    "Siberiano",
    "Snowshoe",
    "Somalí",
    "Sphynx",
    "Tonkinés",
    "Van Turco"
];

var aves = [
    "‍Otra / desconocida",
    "Agaporni",
    "Cacatúa",
    "Canario",
    "Cotorra",
    "Diamante de Gould",
    "Diamante mandarín",
    "Guacamayo",
    "Jilguero",
    "Loro Amazonas",
    "Loro Arcoiris",
    "Loro Eclecto",
    "Loro Yaco",
    "Ninfa",
    "Periquito",
    "Pionites (caique de cabeza negra)",
    "Ruiseñor"
];

var conejos = [
    "‍Mestizo / sin raza",
    "‍Otra / no disponible",
    "American",
    "American chinchilla",
    "American Sable",
    "Amfuzzylop",
    "Belgian Hare",
    "Beveren",
    "Blanc de Hotot",
    "Britannia Petite",
    "Californian",
    "Champagne d'argent",
    "Chinchilla",
    "Cinnamon",
    "Creme d'argent",
    "Dutch small",
    "Dwarfhotot",
    "English angora",
    "Englishspot",
    "Englop small",
    "Flemish giant",
    "Florida white",
    "French angora",
    "Frenchlop small",
    "Giant angora",
    "Giant chinchilla",
    "Harlequin small",
    "Havana",
    "Himalayan",
    "Hollandlop small",
    "Hotot",
    "Jersey wooly",
    "Lilac",
    "Minilop small",
    "Minirex",
    "Netherland dwarf",
    "Newzeland small",
    "Palomino",
    "Polish small",
    "Rex",
    "Rhynelander",
    "Saint angora",
    "Satin",
    "Silver",
    "Silver Fox",
    "Silver marten",
    "Tan"
];

var roedores = [
    "‍Otra / no disponible",
    "Ardilla",
    "Chinchilla",
    "Cobaya",
    "Hamster",
    "Jerbo",
    "Lirón",
    "Marmota",
    "Rata",
    "Ratón"
];

var hurones = [
    "‍Mestizo / sin raza",
    "‍Otra / no disponible",
    "Angora",
    "Bulldog",
    "Estándar",
    "Whippet"
];

var peces = [
    "Agua caliente",
    "Agua fría",
    "Agua salada"
];

var caballos = [
    "‍Mestizo / sin raza",
    "‍Otra / no disponible",
    "Akhal-Teké",
    "Albino",
    "Apaloossa",
    "Árabe-portuguesa",
    "Árabe-Shagya",
    "Asturcón",
    "Aveliñés",
    "Bardigiano",
    "Bereber",
    "Buckskin",
    "Budyonny",
    "Caballo de las Murgues",
    "Chileno",
    "Chilote",
    "Criollo argentino",
    "Darashouri",
    "Don",
    "Dülmener|Dülmener Wildpferd",
    "Española o andaluza",
    "Francés de silla",
    "Freiberg",
    "Frisón",
    "Gelder",
    "Gotland",
    "Haflinger",
    "Hannoveriano",
    "Holstein",
    "Irish Cob",
    "Irish Hunter",
    "Islandés",
    "Jaca navarra",
    "Karabakh",
    "Konik",
    "Kustanair",
    "Lipizzano",
    "Lokai",
    "Lusitano",
    "Morgan Horse",
    "Mustang",
    "Nonius",
    "Oldenburgues",
    "Palomino",
    "Paso Fino",
    "Percherón",
    "Peruano de Paso.",
    "Pinto",
    "Przewalski",
    "Purasangre",
    "Quarter Horse",
    "Salernitano",
    "San Fratelano",
    "Shetland Pony",
    "Silla americano",
    "Tennessee Walking Horse",
    "Tersk",
    "Tinker",
    "Torik",
    "Trakehner",
    "Ucraniano",
    "Waler"
];

var hanuros = [
    "‍Otra / desconocida",
    "Rana arborícola verde de Australia",
    "Rana bermeja",
    "Rana campestre",
    "Rana común",
    "Rana griega",
    "Rana leopardo",
    "Rana toro",
    "Rana venenosa",
    "Rana verde",
    "Rana verde europea",
    "Ranita de San Antón",
    "Ranita meridional",
    "Sapillo moteado",
    "Sapillo pintojo",
    "Sapo común",
    "Sapo corredor",
    "Sapo de espuelas",
    "Sapo de espuelas oriental",
    "Sapo de espuelas pardo",
    "Sapo de vientre amarillo",
    "Sapo de vientre de fuego oriental",
    "Sapo gigante",
    "Sapo partero",
    "Sapo partero balear",
    "Sapo verde"
];

var reptiles = [
    "‍Otra / no disponible",
    "Camaleón",
    "Gecko",
    "Iguana",
    "Lagarto",
    "Serpiente",
    "Tortuga",
    "Tritón"
];

var aracnidos = [
    "‍Otra / desconocida",
    "Alacrán",
    "Araña",
    "Escorpión",
    "Tarántula"
];

var otros =
    [
        "‍Otro / no disponible",
        "Abeja",
        "Avispa",
        "Burro",
        "Cabra",
        "Cerdo",
        "Gallina / Gallo",
        "Ganso",
        "Insecto",
        "Luciérnaga",
        "Mantis religiosa",
        "Mariposa",
        "Mosca",
        "Mula",
        "Oca",
        "Oveja",
        "Paloma",
        "Pato",
        "Saltamontes",
        "Vaca / toro"
    ];

async function init() {

    await Autonomias.sync({ alter: true });
    console.log("The table for the Autonomia model was just (re)created!");

    await Provincias.sync({ alter: true });
    console.log("The table for the Provincia model was just (re)created!");

    await Municipios.sync({ alter: true });
    console.log("The table for the Municipio model was just (re)created!");

    await Usuarios.sync({ alter: true });
    console.log("The table for the Usuario model was just (re)created!");

    await Animales.sync({ alter: true });
    console.log("The table for the Animales model was just (re)created!");

    await Razas.sync({ alter: true });
    console.log("The table for the Razas model was just (re)created!");

    await Mensajes.sync({ alter: true });
    console.log("The table for the Mensajes model was just (re)created!");

    await Solicitudes.sync({ alter: true });
    console.log("The table for the Solicitudes model was just (re)created!");

    await Ubicaciones.sync({ alter: true });
    console.log("The table for the Ubicaciones model was just (re)created!");

    await ComentariosUbis.sync({ alter: true });
    console.log("The table for the ComentariosUbis model was just (re)created!");

    await Mascotas.sync({ alter: true });
    console.log("The table for the Mascotas model was just (re)created!");

    await Posts.sync({ alter: true });
    console.log("The table for the Posts model was just (re)created!");

    await Comentarios.sync({ alter: true });
    console.log("The table for the Comentarios model was just (re)created!");

    

    animales.map((elem) => {

        Animales.findOrCreate({

            where: {
                nombre: elem
            },
            defaults: {
                nombre: elem
            }

        });

    });


    perros.map((elem) => {

        Razas.findOrCreate({
            where: {
                nombre: elem,
                id_tipo: 1
            },
            defaults: {


                nombre: elem,
                id_tipo: 1
            }
        });

    });

    
    gatos.map((elem) => {

        Razas.findOrCreate({
            where: {
                nombre: elem,
                id_tipo: 2
            },
            defaults: {

                nombre: elem,
                id_tipo: 2
            }
        })

    })

    
    aves.map((elem) => {

        Razas.findOrCreate({
            where: {
                nombre: elem,
                id_tipo: 3
            },
            defaults: {


                nombre: elem,
                id_tipo: 3
            }
        })

    })

    
    conejos.map((elem) => {

        Razas.findOrCreate({
            where: {
                nombre: elem,
                id_tipo: 4
            },
            defaults: {


                nombre: elem,
                id_tipo: 4
            }
        })

    })


    
    roedores.map((elem) => {

        Razas.findOrCreate({
            where: {
                nombre: elem,
                id_tipo: 5
            },
            defaults: {


                nombre: elem,
                id_tipo: 5
            }
        })

    })

    
    hurones.map((elem) => {

        Razas.findOrCreate({
            where: {
                nombre: elem,
                id_tipo: 6
            },
            defaults: {


                nombre: elem,
                id_tipo: 6
            }
        })

    })

    
    peces.map((elem) => {

        Razas.findOrCreate({
            where: {
                nombre: elem,
                id_tipo: 7
            },
            defaults: {


                nombre: elem,
                id_tipo: 7
            }
        })

    })

    
    caballos.map((elem) => {

        Razas.findOrCreate({
            where: {
                nombre: elem,
                id_tipo: 8
            },
            defaults: {


                nombre: elem,
                id_tipo: 8
            }
        })

    })

    
    hanuros.map((elem) => {

        Razas.findOrCreate({
            where: {
                nombre: elem,
                id_tipo: 9
            },
            defaults: {


                nombre: elem,
                id_tipo: 9
            }
        })

    })

    
    reptiles.map((elem) => {

        Razas.findOrCreate({
            where: {
                nombre: elem,
                id_tipo: 10
            },
            defaults: {


                nombre: elem,
                id_tipo: 10
            }
        })

    })


    
    aracnidos.map((elem) => {

        Razas.findOrCreate({
            where: {
                nombre: elem,
                id_tipo: 11
            },
            defaults: {


                nombre: elem,
                id_tipo: 11
            }
        })

    })

    
    otros.map((elem) => {

        Razas.findOrCreate({
            where: {
                nombre: elem,
                id_tipo: 12
            },
            defaults: {


                nombre: elem,
                id_tipo: 12
            }
        })

    })
    
    

    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);

    }

}

module.exports = init;