// Require as a module loader function
const express = require('express');
const cors = require('../common/node_modules/cors');
const bodyParser = require('body-parser');

const app = express();

const db_init = require('./database/database');

const http = require("http");
const server = http.createServer(app);

const jwt = require('../common/node_modules/jsonwebtoken');


// Settings:

// set the port given or 8000 by default
app.set('port', process.env.PORT || 8000)


// Middlewares:
app.use(cors());

// check json format 
var jsonParser = bodyParser.json()


// Routes:
app.use("/session", require("./routes/session.routes"));
app.use("/pets", require("./routes/pets.routes"));
app.use("/friends", require("./routes/friends.routes"));
app.use("/ubis", require("./routes/ubis.routes"));

// Static files:

// Starting the server
server.listen(app.get('port'), () => {
    db_init();
    console.log(`Server on port ${app.get('port')}`);

});



function authentication(req) {
    
    //console.log(req);

    const authHeader = req
    var token = authHeader && authHeader.split(' ')[1]
    token = token.split('"')[0];
    //console.log(token);

    const id_usuario = jwt.decode(token).id_usuario;

    //console.log(id_usuario);

    // unauthotized
    if ( token == null || token == undefined ) return -1; 

    return jwt.verify(token, 'shhhhh', (err, decoded) => {

        // session expired
        if (err) return -1;

        // correct. Saving token in header
        return id_usuario;

    });

}

/*
const SocketIO = require('socket.io');
const io = SocketIO(server, {
    cors: {
        origin: "http://localhost:3000",
        methods: ["GET", "POST"],
        allowedHeaders: ["authorization"],
        credentials: true
    }
});

//websockets
let interval;
const users = [];

io.use((socket, next) => {
    if (authentication(socket.request)) {
        next();
    } else {
        next(new Error("Fail when authentication"));
    }
});

io.on("connection", (socket) => {
  console.log("New client connected");
  //if (interval) {
  //  clearInterval(interval);
  //}
  //interval = setInterval(() => getApiAndEmit(socket), 1000000);
  getApiAndEmit(socket);
  socket.on("disconnect", () => {
    console.log("Client disconnected");
    users.filter((elem) => {
        if (elem.id_usuario != socket.id_usuario) {
            return elem.keep;
        }
    })
    //clearInterval(interval);
  });
});

const getApiAndEmit = socket => {
    console.log(socket);
  const response = new Date();
  // Emitting a new message. Will be consumed by the client
  socket.emit("FromAPI", response);
}
*/

/*
    WEBSOCKETS SERVER
*/

const WebSocket = require('../common/node_modules/ws');
const Autonomias = require('../common/models/Autonomias');
const Provincias = require('../common/models/Provincias');
const Municipios = require('../common/models/Municipios');
const Usuarios = require('../common/models/Usuarios');
const Solicitudes = require('../common/models/Solicitudes');
const { Op } = require("../common/node_modules/sequelize");

const wss = new WebSocket.Server({ server: server });

//data structures for real time data sharing
var users = {};

wss.on("connection",(ws) => {

    console.log("CONNECTED!") ;

    ws.on("message", async (msg) => {
        console.log(msg);

        //getting token field
        var j_msg = JSON.parse(msg);
        token = JSON.stringify(j_msg.token);
        var id = authentication(token)

         if (id != -1) {

            // updating lat and long
            users[id] = {lat: j_msg.lat, lng: j_msg.lng, ws: ws};
            ws['id_usuario'] = id;

            //getting user friends
            const amigos = await Solicitudes.findAll({
                where: {
                    [Op.or]: [{
                        emisor: id
                    }, {
                        receptor: id
                    }]
                }

            })

            for (var i = 0; i < amigos.length; i++) {
                if (amigos[i].emisor == id) {
                    amigos[i] = amigos[i].receptor;
                } else {
                    amigos[i] = amigos[i].emisor
                }
            }

            // sending user lat long to friend's sockets
            amigos.map((elem) => {
                if (users[elem] != undefined) {
                    console.log("=======================");
                    console.log(users[elem].ws);
                    users[elem].ws.send(JSON.stringify({type: "location", id: id, lat: users[id].lat, lng: users[id].lng}))
                } 
            })

         } else {

            users[id] = {};
            ws.close();

        }

    })

    ws.on("close", async () => {

        console.log("DISCONNECTED!");
        console.log(ws.id_usuario);

         //sending last message telling user is disconnected.
         const amigos = await Solicitudes.findAll({
            where: {
                [Op.or]: [{
                    emisor: ws.id_usuario
                }, {
                    receptor: ws.id_usuario
                }]
            }

        })

        for (var i = 0; i < amigos.length; i++) {
            if (amigos[i].emisor == ws.id_usuario) {
                amigos[i] = amigos[i].receptor;
            } else {
                amigos[i] = amigos[i].emisor
            }
        }

        // sending user lat long to friend's sockets
        amigos.map((elem) => {
            if (users[elem] != undefined) {
                console.log("========================================================");
                users[elem].ws.send(JSON.stringify({type: "ending", id: ws.id_usuario}))
            } 
        })
        
        delete users[ws.id_usuario]

        console.log(users);

    })

});