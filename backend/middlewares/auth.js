var jwt = require('../../common/node_modules/jsonwebtoken');
const bodyParser = require('body-parser');

const verifyToken = (req, res, next) => {

    console.log("Verifying token...");

    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]

    console.log(jwt.decode(token));
    req.id_usuario = jwt.decode(token).id_usuario;

    // unauthotized
    if ( token == null || token == undefined ) return res.json({status: "badToken"}); 

    jwt.verify(token, 'shhhhh', (err, decoded) => {

        // session expired
        if (err) return res.json({status: "badVerify"});

        // correct. Saving token in header
        req.headers['authorization'] = token;
        next();

    });

}

module.exports = verifyToken;