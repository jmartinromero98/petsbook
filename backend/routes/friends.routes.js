const express = require('express');
const bodyParser = require('body-parser');
const { response } = require('express');
const router = express.Router()
const { Sequelize, Model } = require('../../common/node_modules/sequelize');

const Municipios = require('../../common/models/Municipios');
const Usuarios = require('../../common/models/Usuarios');
const Autonomias = require('../../common/models/Autonomias');
const Provincias = require('../../common/models/Provincias');
const Solicitudes = require('../../common/models/Solicitudes');
const Razas = require('../../common/models/Razas');
const Animales = require('../../common/models/Animales');
const Mascotas = require('../../common/models/Mascotas');
const fs = require('fs');

const op = Sequelize.Op;

const verifyToken = require('../middlewares/auth');
const jwt = require('../../common/node_modules/jsonwebtoken');
const Mensajes = require('../../common/models/Mensajes');


router.post("/getUsers", [bodyParser.json(), verifyToken], async (req, res) => {

    var result;

    // sql búsqueda personas no amigas
    /*
    SELECT * FROM Usuarios LEFT JOIN (SELECT Usuarios.id_usuario FROM Usuarios JOIN Solicitudes ON (Usuarios.id_usuario = Solicitudes.emisor AND Solicitudes.receptor = 1 ) OR (Usuarios.id_usuario = Solicitudes.receptor AND Solicitudes.emisor = 1)) AS A ON Usuarios.id_usuario = A.id_usuario WHERE A.id_usuario IS NULL AND Usuarios.id_usuario != 1
    */

    // sql búsqueda personas amigas
    /*
    SELECT * FROM Usuarios LEFT JOIN Solicitudes ON (Usuarios.id_usuario = Solicitudes.emisor AND Solicitudes.receptor = 1 AND Solicitudes.estado = "Aceptada") OR (Usuarios.id_usuario = Solicitudes.receptor AND Solicitudes.emisor = 1 AND Solicitudes.estado = "Aceptada") WHERE Usuarios.id_usuario != 1
    */

    // Row query rompe la independencia con los cambios en la bd. No nos interesa.

    // when a field is 0 we dont get in mind as a filter

    var offset = (req.body.page - 1) * 5;
    var limit = 5;

    console.log(req.body);

    var j_name;

    if (req.body.name != "") {

        j_name = {
            nombre: {
                [op.substring]: req.body.nombre
            }
        }

    } else {

        j_name = {}

    }

    if (parseInt(req.body.Municipio) == 0) {

        if (parseInt(req.body.Provincia) == 0) {

            if (parseInt(req.body.Autonomia) == 0) {

                result = await Usuarios.findAll({
                    include: [{
                        include: [{
                            include: [{
                                model: Autonomias
                            }],
                            model: Provincias,
                        }],
                        model: Municipios,
                        required: true
                    },
                    {
                        model: Solicitudes,
                        as: 'SolAsEmisor',
                        where: {
                            receptor: req.id_usuario,
                        },
                        required: false,
                    },

                    {
                        model: Solicitudes,
                        as: 'SolAsReceptor',
                        where: {
                            emisor: req.id_usuario,
                        },
                        required: false,
                    }
                    ],
                    offset: offset,
                    limit: limit,
                    attributes: ['nombre', 'id_usuario', 'Foto_perfil'],
                    where: [{
                        id_usuario: {
                            [op.ne]: req.id_usuario
                        }
                    }, j_name],
                    order: [
                        ['id_usuario', 'ASC'],
                    ]

                })

            } else {

                //prblm
                result = await Usuarios.findAll({
                    include: [{
                        include: [{
                            include: [{
                                model: Autonomias
                            }],
                            model: Provincias,
                            where: {
                                id_autonomia: req.body.Autonomia
                            }
                        }],
                        model: Municipios,
                        required: true
                    },
                    {
                        model: Solicitudes,
                        as: 'SolAsEmisor',
                        required: false,
                    },

                    {
                        model: Solicitudes,
                        as: 'SolAsReceptor',
                        where: {
                            emisor: req.id_usuario,
                        },
                        required: false,
                    }
                    ]
                    ,
                    attributes: ['nombre', 'id_usuario', 'Foto_perfil'],
                    where: [{
                        id_usuario: {
                            [op.ne]: req.id_usuario
                        }
                    }, j_name],
                    order: [
                        ['id_usuario', 'ASC'],
                    ]
                })

            }

        } else {

            result = await Usuarios.findAll({
                include: [{
                    include: [{
                        include: [{
                            model: Autonomias
                        }],
                        model: Provincias,
                    }],
                    model: Municipios,
                    where: {
                        id_provincia: req.body.Provincia
                    },
                    required: true
                },
                {
                    model: Solicitudes,
                    as: 'SolAsEmisor',
                    required: false,
                },

                {
                    model: Solicitudes,
                    as: 'SolAsReceptor',
                    where: {
                        emisor: req.id_usuario,
                    },
                    required: false,
                }
                ],
                attributes: ['nombre', 'id_usuario', 'Foto_perfil'],
                where: [{
                    id_usuario: {
                        [op.ne]: req.id_usuario
                    }
                }, j_name],
                order: [
                    ['id_usuario', 'ASC'],
                ]
            })

        }

    } else {

        result = await Usuarios.findAll({
            include: [{
                include: [{
                    include: [{
                        model: Autonomias,
                        required: true
                    }],
                    model: Provincias,
                    required: true
                }],
                model: Municipios,
                where: {
                    codine: req.body.Municipio
                },
                required: true
            },
            {
                model: Solicitudes,
                as: 'SolAsEmisor',
                required: false,
            },

            {
                model: Solicitudes,
                as: 'SolAsReceptor',
                where: {
                    emisor: req.id_usuario,
                },
                required: false,
            }
            ],
            attributes: ['nombre', 'id_usuario', 'Foto_perfil'],
            where: [{
                id_usuario: {
                    [op.ne]: req.id_usuario
                }
            }, j_name],
            order: [
                ['id_usuario', 'ASC'],
            ]
        })

    }


    result.map((elem) => {

        var path = elem.Foto_perfil;
        var data = fs.readFileSync(path, { encoding: 'utf-8' })
        elem.Foto_perfil = data;

    })


    res.json({ status: true, data: result });

});

router.post("/getProfile", [bodyParser.json(), verifyToken], async (req, res) => {

    console.log(req.body);

    const solicitud = await Solicitudes.findAll({
        attributes: ['estado', 'emisor', 'receptor'],
        where: {
            [op.or]: [{
                [op.and]: [{
                    emisor: req.id_usuario,
                }, {
                    receptor: req.body.id_profile
                }]


            }, {
                [op.and]: [{
                    receptor: req.id_usuario,
                }, {
                    emisor: req.body.id_profile
                }]
            }]
        }
    })

    var profile;

    if ((solicitud.length == 1 && solicitud[0].estado == "Aceptada") || req.body.id_profile == req.id_usuario) {

        profile = await Usuarios.findAll({
            include: [{
                include: [{
                    include: [{
                        model: Autonomias,
                        attributes: ['nombre'],
                        required: true
                    }],
                    model: Provincias,
                    attributes: ['nombre'],
                    required: true
                }],
                model: Municipios,
                attributes: ['nombre'],
                required: true
            },
            {
                include: [{
                    include: [{
                        model: Animales,
                        required: true
                    }],
                    model: Razas,
                    required: true
                }],
                model: Mascotas,
                as: 'mascotas',
                required: false
            }],
            attributes: ['nombre', 'email', 'Foto_perfil'],
            where: {
                id_usuario: req.body.id_profile
            }
        });

    } else {

        profile = await Usuarios.findAll({
            include: [{
                include: [{
                    include: [{
                        model: Autonomias,
                        attributes: ['nombre'],
                        required: true
                    }],
                    model: Provincias,
                    attributes: ['nombre'],
                    required: true
                }],
                model: Municipios,
                attributes: ['nombre'],
                required: true
            },
            {
                include: [{
                    include: [{
                        model: Animales,
                        required: true
                    }],
                    model: Razas,
                    required: true
                }],
                model: Mascotas,
                as: 'mascotas',
                required: false
            }],
            attributes: ['nombre', 'Foto_perfil'],
            where: {
                id_usuario: req.body.id_profile
            }
        });

    }

    var path = profile[0].Foto_perfil;
    var data = fs.readFileSync(path, { encoding: 'utf-8' })
    profile[0].Foto_perfil = data;

    var i = 0;
    // get pets photo
    profile[0].mascotas.map((elem) => {

        var path = profile[0].mascotas[i].Foto_perfil;
        var data = fs.readFileSync(path, { encoding: 'utf-8' })
        profile[0].mascotas[i].Foto_perfil = data;

        i++;
    });

    res.json({ status: true, data: profile });
});

router.post("/petition", [bodyParser.json(), verifyToken], async (req, res) => {

    if (parseInt(req.body.receptor) == NaN) {

        res.json({ status: false });

    } else {

        Solicitudes.create({
            emisor: req.id_usuario,
            receptor: req.body.receptor,
            estado: 'Enviada'
        })

        res.json({ status: true });

    }

});

router.post("/cancel", [bodyParser.json(), verifyToken], async (req, res) => {

    if (parseInt(req.body.receptor) == NaN) {

        res.json({ status: false });

    } else {

        Solicitudes.destroy({
            where: {
                emisor: req.id_usuario,
                receptor: req.body.receptor
            }
        })

        res.json({ status: true });

    }

})

router.post("/accept", [bodyParser.json(), verifyToken], async (req, res) => {

    if (parseInt(req.body.receptor) == NaN) {

        res.json({ status: false });

    } else {

        Solicitudes.update({
            estado: "Aceptada"
        }, {
            where: {
                id: req.body.id
            }
        })

        res.json({ status: true });

    }

});

router.post("/remove", [bodyParser.json(), verifyToken], async (req, res) => {

    if (parseInt(req.body.receptor) == NaN) {

        res.json({ status: false });

    } else {

        Solicitudes.destroy({
            where: {
                id: req.body.id,
            }
        })

        res.json({ status: true });

    }

});

router.post("/getFriends", verifyToken, async (req, res) => {

    var amigos = await Solicitudes.findAll({
        include: [{
            include: [{
                include: [{
                    include: [{
                        model: Autonomias,
                        attributes: ['nombre']
                    }],
                    model: Provincias,
                    attributes: ['nombre']
                }],
                model: Municipios,
                attributes: ['nombre']
            }],
            model: Usuarios,
            as: 'receptorTable',
            attributes: ['nombre', 'id_usuario', 'Foto_perfil']
        }, {
            include: [{
                include: [{
                    include: [{
                        model: Autonomias,
                        attributes: ['nombre']
                    }],
                    model: Provincias,
                    attributes: ['nombre']
                }],
                model: Municipios,
                attributes: ['nombre']
            }],
            model: Usuarios,
            as: 'emisorTable',
            attributes: ['nombre', 'id_usuario', 'Foto_perfil']
        }],
        where: {
            [op.or]: [{
                emisor: req.id_usuario
            }, {
                receptor: req.id_usuario
            }]
        }
    })

    //manage to avoid sending repeated and useless user data.
    var i = 0;
    while (i < amigos.length) {

        var tmp1 = amigos[i].id;
        var tmp2 = amigos[i].estado;
        var tmp3 = amigos[i].emisor;
        var tmp4 = amigos[i].receptor;
        var tmp5 = amigos[i].receptorTable;
        var tmp6 = amigos[i].emisorTable;

        amigos[i] = {
            id: tmp1,
            estado: tmp2,
            emisor: tmp3,
            receptor: tmp4,
            receptorTable: tmp5,
            emisorTable: tmp6
        }

        if (amigos[i].receptorTable.id_usuario == req.id_usuario) {

            amigos[i].receptorTable = {};

            amigos[i].emisorTable.Foto_perfil
            var path = amigos[i].emisorTable.Foto_perfil;
            var data = fs.readFileSync(path, { encoding: 'utf-8' })
            amigos[i].emisorTable.Foto_perfil = data;

        } else {

            amigos[i].emisorTable = {};

            amigos[i].receptorTable.Foto_perfil
            var path = amigos[i].receptorTable.Foto_perfil;
            var data = fs.readFileSync(path, { encoding: 'utf-8' })
            amigos[i].receptorTable.Foto_perfil = data;
        }

        i++;

    }


    res.json(amigos);

})

router.post("/getMessages", [verifyToken, bodyParser.json()], async (req, res) => {

    const msg = await Mensajes.findAll({
        include: [{
            attributes: ['nombre'],
            model: Usuarios,
            as: 'emTable',
            required: true
        },
        {
            attributes: ['nombre'],
            model: Usuarios,
            as: 'reTable',
            required: true
        }],
        where: {
            [op.or]: [{
                [op.and]: [{
                    emisor: req.id_usuario,
                }, {
                    receptor: req.body.id
                }]


            }, {
                [op.and]: [{
                    receptor: req.id_usuario,
                }, {
                    emisor: req.body.id
                }]
            }]
        }
    })

    const profile = await Usuarios.findAll({

        attributes: ['Foto_perfil'],
        where: {

            id_usuario: req.body.id

        }

    })

    var foto = fs.readFileSync(profile[0].dataValues.Foto_perfil, { encoding: 'utf-8' })

    res.json({ status: true, data: {msg: msg, foto: foto }});

})

router.post("/sendMessage", [verifyToken, bodyParser.json()], async (req, res) => {

    console.log(req.body);

    if (req.body.msg == "") {

        res.json({ status: "badMsg" });

    } else {

        const result = await Mensajes.create({

            emisor: req.id_usuario,
            receptor: req.body.receptor,
            Texto: req.body.msg,

        })



        const msg = await Mensajes.findAll({
            include: [{
                attributes: ['nombre'],
                model: Usuarios,
                as: 'emTable',
                required: true
            },
            {
                attributes: ['nombre'],
                model: Usuarios,
                as: 'reTable',
                required: true
            }],
            where: {
                [op.or]: [{
                    [op.and]: [{
                        emisor: req.id_usuario,
                    }, {
                        receptor: req.body.receptor
                    }]


                }, {
                    [op.and]: [{
                        receptor: req.id_usuario,
                    }, {
                        emisor: req.body.receptor
                    }]
                }]
            }
        })

        res.json({ status: true, data: msg });

    }

});

router.post("/deleteMessages", [verifyToken, bodyParser.json()], async (req, res) => {

    await Mensajes.destroy({
        where: {
            [op.or]: [{
                [op.and]: [{
                    emisor: req.id_usuario
                }, {
                    receptor: req.body.id_usuario
                }]
            }, {
                [op.and]: [{
                    emisor: req.body.id_usuario
                }, {
                    receptor: req.id_usuario
                }]
            }]
        }
    })

    res.json({ status: true });

});

router.post("/deleteFriend", [verifyToken, bodyParser.json()], async (req, res) => {

    await Solicitudes.destroy({
        where: {
            [op.and]: [{
                estado: 'Aceptada'
            }, {
                [op.or]: [{
                    [op.and]: [{
                        emisor: req.id_usuario
                    }, {
                        receptor: req.body.id_usuario
                    }]
                }, {
                    [op.and]: [{
                        emisor: req.body.id_usuario
                    }, {
                        receptor: req.id_usuario
                    }]
                }]
            }]

        }
    })

    res.json({ status: true });

});

module.exports = router