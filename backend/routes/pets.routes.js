const express = require('express');
const bodyParser = require('body-parser');
const { response } = require('express');
const router = express.Router()
const { body } = require('../../common/node_modules/express-validator');
const { Sequelize } = require('../../common/node_modules/sequelize');

const Animales = require('../../common/models/Animales');
const Mascotas = require('../../common/models/Mascotas');
const Posts = require('../../common/models/Posts');
const Razas = require('../../common/models/Razas');
const fs = require('fs');
const Municipios = require('../../common/models/Municipios');
const Usuarios = require('../../common/models/Usuarios');
const Comentarios = require('../../common/models/Comentarios');
const Solicitudes = require('../../common/models/Solicitudes');

const verifyToken = require('../middlewares/auth');
const jwt = require('../../common/node_modules/jsonwebtoken');
const Provincias = require('../../common/models/Provincias');
const Autonomias = require('../../common/models/Autonomias');
const { EDESTADDRREQ } = require('constants');
const op = Sequelize.Op;

const HashGenre = [
    "Macho",
    "Hembra",
    "Otro"
]

var petNameRegExp = /^[a-zA-Z]+$/

var genre = {
    "Macho": 0,
    "Hembra": 1,
    "Otro": 2
}

router.post("/type", async (req, res) => {

    const tipos = await Animales.findAll({

        attributes: ['nombre', 'id_animal']

    });

    var i = 0;
    tipos.map((elem) => {
        tipos[i] = elem.dataValues;
        i++;
    });

    res.json({ status: true, data: tipos });

})

router.post("/breeds", bodyParser.json(), async (req, res) => {

    const resultado = await Razas.findAll({

        attributes: ['nombre', 'id_raza'],
        where: {

            id_tipo: req.body.id_animal

        }

    });

    var i = 0;
    resultado.map((elem) => {
        resultado[i] = elem.dataValues;
        i++;
    });

    res.json({ status: true, data: resultado });

});

router.post("/createPet", [bodyParser.json({ limit: '50mb' }), verifyToken], async (req, res) => {

    // turtle is the domestic animal with the longest life expectancy
    // we dont want to refuse any animal register because of its age
    const now = new Date();
    const minPetYear = now.getUTCFullYear() - 60;

    if (req.body.img == "#") {

        res.json({ status: "badImage" });

    } else if (!petNameRegExp.test(req.body.name)) {

        res.json({ status: "badName" });

    } else if (req.body.year == undefined || req.body.year == "" || parseInt(req.body.year) > now.getUTCFullYear() || parseInt(req.body.year) < minPetYear) {

        res.json({ status: "badYear" });

    } else if (parseInt(req.body.genre) != 1 && parseInt(req.body.genre) != 2 && parseInt(req.body.genre) != 3) {

        res.json({ status: "badGenre" });

    } else {

        const idraza = await Razas.findAll({

            attributes: ['id_raza'],
            where: {

                id_raza: req.body.breed,

            }

        })

        if (idraza.length == 0) {

            res.json({ status: "badBreed" });

        } else {



            //get username
            var username = jwt.decode(req.headers['authorization']).id_usuario;

            console.log(username);

            // checked all fields. Now store pet info and photo
            const mascota = await Mascotas.create({
                nombre: req.body.name,
                edad: req.body.year,
                propietario: username,
                Foto_perfil: null,
                genero: req.body.genre,
                id_raza: parseInt(req.body.breed)
            })

            console.log(mascota);

            var dirname = __dirname;
            dirname = dirname.replace("routes", "database");
            var dirname = dirname.concat("/petImages/" + mascota.id_mascota);

            fs.mkdir(dirname, { recursive: true }, (err) => {
                if (err) throw err;
            });

            //creating profile proho file
            fs.writeFile(dirname + '/profile.txt', req.body.img, (err) => {

                if (err) throw err;

                console.log('profile photo saved!');
            });

            mascota.Foto_perfil = dirname + '/profile.txt';
            await mascota.save();


            res.json({ status: true });

        }


    }


});

router.get("/", (req, res) => {

    /*
    Municipios.findAll({
        attributes: ['codine', 'nombre'],
        include: [
            {
                model: Usuarios,
                where: {
                    municipio: codine
                },
                required: true
            }
        ]
    }).then((rows) => {

        res.json({ data: rows });

    })
    */


})

// get user and friends pets
router.post("/getPets", [bodyParser.json(), verifyToken], async (req, res) => {

    // get user email to find friends
    const user = jwt.decode(req.headers['authorization']).id_usuario;

    /*
    // search for all friends
    Solicitudes.findAll({

        attributes: ['',''],
         {foreignKey: 'user_id'}
    })
    */

    // get all pets 
    const pets = await Mascotas.findAll({
        include: [
            {
                model: Razas
            },
            {
                model: Usuarios
            }
        ],
        attributes: ['nombre', 'Foto_perfil', 'edad', 'propietario', 'genero', 'id_raza', 'id_mascota'],
        where: {

            'propietario': user,

        }
    })

    var i = 0;


    // get profile photo
    pets.map((elem) => {

        var path = pets[i].Foto_perfil;
        var data = fs.readFileSync(path, { encoding: 'utf-8' })
        pets[i].Foto_perfil = data;

        i++;
    });

    res.json({ status: true, data: pets });

});

router.post("/searchPets", [verifyToken, bodyParser.json()], async (req, res) => {

    //json for name
    var j_name = {};
    if (req.body.nombre != "" && req.body.nombre != undefined) {

        j_name = {
            nombre: {
                [op.substring]: req.body.nombre
            }
        }

    }

    var offset = (req.body.page - 1) * 5;
    var limit = 5;

    //json for genre
    var j_genero = {};
    if (req.body.genero == "1" || req.body.genero == "2" || req.body.genero == "3") {

        j_genero = {

            genero: HashGenre[parseInt(req.body.genero) - 1]

        }

    }

    //json for pet type
    var j_tipo = {};
    if (parseInt(req.body.raza) == 0) {

        if (parseInt(req.body.animal) == 0) {

            j_tipo = {

                include: [{

                    model: Animales,
                    required: true

                }],

                model: Razas,
                required: true
            }

        } else {

            j_tipo = {

                include: [{

                    model: Animales,
                    where: {
                        id_animal: req.body.animal
                    },
                    required: true

                }],

                model: Razas,
                required: true
            }

        }

    } else {

        j_tipo = {

            include: [{

                model: Animales,
                required: true

            }],

            model: Razas,
            where: {
                id_raza: req.body.raza
            },
            required: true
        }

    }



    // json for pet site
    var j_sitio = {};
    if (parseInt(req.body.municipio) == 0) {

        if (parseInt(req.body.provincia) == 0) {

            if (parseInt(req.body.autonomia) == 0) {

                j_sitio = {

                    include: [{

                        include: [{

                            include: [{

                                model: Autonomias,
                                required: true

                            }],

                            model: Provincias,
                            required: true
                        }],

                        model: Municipios,
                        required: true
                    }],

                    model: Usuarios,
                    required: true

                }

            } else {

                j_sitio = {

                    include: [{

                        include: [{

                            include: [{

                                model: Autonomias,
                                where: {
                                    id_autonomia: req.body.autonomia
                                },
                                required: true

                            }],

                            model: Provincias,
                            required: true
                        }],

                        model: Municipios,
                        required: true
                    }],

                    model: Usuarios,
                    required: true

                }

            }

        } else {

            j_sitio = {

                include: [{

                    include: [{

                        include: [{

                            model: Autonomias,
                            required: true

                        }],

                        model: Provincias,
                        where: {
                            id_provincia: req.body.provincia
                        },
                        required: true
                    }],

                    model: Municipios,
                    required: true
                }],

                model: Usuarios,
                required: true

            }

        }

    } else {

        j_sitio = {

            include: [{

                include: [{

                    include: [{

                        model: Autonomias,
                        required: true

                    }],

                    model: Provincias,
                    required: true
                }],

                model: Municipios,
                where: {
                    codine: req.body.municipio
                },
                required: true
            }],

            model: Usuarios,
            required: true

        }

    }

    const pets = await Mascotas.findAll({
        include: [
            j_tipo,
            j_sitio,
        ],
        offset: offset,
        limit: limit,
        attributes: ['nombre', 'genero', 'Foto_perfil', 'edad', 'id_mascota'],
        where: [j_name, j_genero, {

            propietario: {
                
                [op.ne]: req.id_usuario

            }

        }]
    });

    var i = 0;
    // get profile photo
    pets.map((elem) => {

        var path = pets[i].Foto_perfil;
        var data = fs.readFileSync(path, { encoding: 'utf-8' })
        pets[i].Foto_perfil = data;

        i++;

    });

    res.json(pets);


})

router.post("/getProfile", [verifyToken, bodyParser.json()], async (req, res) => {

    const pet = await Mascotas.findAll({
        include: [{
            include: [{
                include: [{
                    include: [{
                        attributes: ['nombre'],
                        model: Autonomias,
                        required: true
                    }],
                    attributes: ['nombre'],
                    model: Provincias,
                    required: true
                }],
                attributes: ['nombre'],
                model: Municipios,
                required: true,
            }],
            attributes: ['nombre', 'id_usuario', 'Foto_perfil'],
            model: Usuarios,
            required: true
        }, {
            include: [{
                attributes: ['nombre'],
                model: Animales,
                required: true,
            }],
            attributes: ['nombre'],
            model: Razas,
            required: true
        }],
        attributes: ['nombre', 'Foto_perfil', 'genero', 'edad', 'id_mascota'],
        where: {
            id_mascota: req.body.id_mascota
        }
    })

    var path = pet[0].Foto_perfil;
    var data = fs.readFileSync(path, { encoding: 'utf-8' })
    pet[0].Foto_perfil = data;

    var path = pet[0].Usuario.Foto_perfil;
    var data = fs.readFileSync(path, { encoding: 'utf-8' })
    pet[0].Usuario.Foto_perfil = data;

    res.json(pet);

});

router.post("/uploadPost", [verifyToken, bodyParser.json({ limit: '50mb' })], async (req, res) => {

    if (req.body.title == undefined || req.body.title == "") {

        res.json({ status: "badTitle" });

    } else {

        if (req.body.desc == undefined || req.body.desc == "") {

            res.json({ status: "badDesc" });

        } else {

            if (req.body.photo == "#") {

                res.json({ status: "noImage" });

            } else {

                const isProp = await Mascotas.findAll({
                    where: {
                        id_mascota: req.body.id,
                        propietario: req.id_usuario
                    }
                })

                console.log(isProp);

                if (isProp.length != 1) {

                    res.json({ status: "noOwner" });

                } else {

                    const posts = await Posts.findAndCountAll({
                        where: {
                            id_mascota: req.body.id
                        }
                    });

                    var nPost = posts.count + 1;

                    console.log(nPost);

                    var dirname = __dirname;
                    dirname = dirname.replace("routes", "database");
                    var dirname = dirname.concat("/petImages/" + req.body.id);

                    //creating profile proho file
                    fs.writeFile(dirname + '/' + nPost + '.txt', req.body.photo, (err) => {

                        if (err) throw err;

                        console.log('profile photo saved!');
                    });

                    var path = dirname + "/" + nPost + ".txt"

                    const post = Posts.create({
                        titulo: req.body.title,
                        Descripcion: req.body.desc,
                        id_mascota: req.body.id,
                        imagen: path
                    })

                    res.json({ status: true });

                }

            }



        }



    }

});

router.post("/getPosts", [verifyToken, bodyParser.json()], async (req, res) => {

    var realPosts = [];

    //pet number of post
    var min = req.body.page * 5;
    var max = min + 5;

    if (req.body.page < 1) {

        res.json({ status: "badPage" });

    } else {

        // we need to check if animal owner and user are friends
        const propietario = await Mascotas.findAll({
            include: [{
                attributes: ['id_usuario'],
                model: Usuarios,
                required: true,
            }],
            attributes: ['id_mascota'],
            where: {
                id_mascota: req.body.id_mascota
            }

        });

        console.log(propietario[0].Usuario);

        const solicitud = await Solicitudes.findAll({
            attributes: ['estado', 'emisor', 'receptor'],
            where: {
                [op.or]: [{
                    [op.and]: [{
                        emisor: req.id_usuario,
                    }, {
                        receptor: propietario[0].Usuario.id_usuario
                    }]


                }, {
                    [op.and]: [{
                        receptor: req.id_usuario,
                    }, {
                        emisor: propietario[0].Usuario.id_usuario
                    }]
                }]
            }
        })


        console.log(solicitud.length);

        if ((solicitud.length == 0 && propietario[0].Usuario.id_usuario != req.id_usuario) ||
            (solicitud.length > 0 && solicitud[0].estado != "Aceptada")) {

            res.json({ status: "noFriends" });

        } else {

            const posts = await Posts.findAll({
                where: {
                    id_mascota: req.body.id_mascota
                },
                order: [['createdAt', 'DESC']]
            });

            var dirname;
            var filePost;

            // filter posts depending on number of page demanded by user
            for (var i = 0; i < posts.length; i++) {

                dirname = posts[i].imagen.split("petImages/" + req.body.id_mascota + "/")[1];
                filePost = dirname.split('.txt')[0];

                //getting image from path
                posts[i].imagen = fs.readFileSync(posts[i].imagen, { encoding: 'utf-8' })
                realPosts.push(posts[i]);

            }

            res.json({ status: true, data: realPosts });

            console.log(posts);

        }

    }

});

router.post("/getComments", [verifyToken, bodyParser.json()], async (req, res) => {

    if (parseInt(req.body.id_post) == NaN) {

        res.json({ status: "badPost" });

    } else {

        // we need to check if animal owner and user are friends
        const propietario = await Posts.findAll({
            include: [{
                include: [{
                    attributes: ['id_usuario'],
                    model: Usuarios,
                    required: true,
                }],
                attributes: ['id_mascota'],
                model: Mascotas,
                required: true
            }],
            where: {
                id_post: req.body.id_post
            }
        });

        const solicitud = await Solicitudes.findAll({
            attributes: ['estado', 'emisor', 'receptor'],
            where: {
                [op.or]: [{
                    [op.and]: [{
                        emisor: req.id_usuario,
                    }, {
                        receptor: propietario[0].Mascota.Usuario.id_usuario
                    }]


                }, {
                    [op.and]: [{
                        receptor: req.id_usuario,
                    }, {
                        emisor: propietario[0].Mascota.Usuario.id_usuario
                    }]
                }]
            }
        })

        console.log(propietario[0].Mascota.Usuario.id_usuario);
        console.log(req.id_usuario);
        console.log(solicitud.length);

        if ((solicitud.length == 0 && propietario[0].Mascota.Usuario.id_usuario != req.id_usuario) ||
            (solicitud.length > 0 && solicitud[0].estado != "Aceptada")) {

            res.json({ status: "noFriends" });

        } else {

            const comments = await Comentarios.findAll({
                include: [{
                    attributes: ['id_usuario', 'nombre', 'Foto_perfil'],
                    model: Usuarios,
                    required: true
                }],
                where: {
                    id_post: req.body.id_post
                },
                order: [['CreatedAt']]
            });

            var i = 0;

            console.log("AQUIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII");
            console.log(comments);

            // get profile photo
            comments.map((elem) => {

                var path = comments[i].Usuario.Foto_perfil;
                var data = fs.readFileSync(path, { encoding: 'utf-8' })
                comments[i].Usuario.Foto_perfil = data;

                i++;
            });

            res.json({ status: true, data: comments });

        }

    }

});

router.post("/sendComment", [verifyToken, bodyParser.json()], async (req, res) => {

    if (req.body.comentario == "") {

        res.json({ status: "badComment" });

    } else {

        // we need to check if animal owner and user are friends
        const propietario = await Posts.findAll({
            include: [{
                include: [{
                    attributes: ['id_usuario'],
                    model: Usuarios,
                    required: true,
                }],
                attributes: ['id_mascota'],
                model: Mascotas,
                required: true
            }],
            where: {
                id_post: req.body.id_post
            }
        });

        // check if they are friends
        const solicitud = await Solicitudes.findAll({
            attributes: ['estado', 'emisor', 'receptor'],
            where: {
                [op.or]: [{
                    [op.and]: [{
                        emisor: req.id_usuario,
                    }, {
                        receptor: propietario[0].Mascota.Usuario.id_usuario
                    }]


                }, {
                    [op.and]: [{
                        receptor: req.id_usuario,
                    }, {
                        emisor: propietario[0].Mascota.Usuario.id_usuario
                    }]
                }]
            }
        })

        console.log(solicitud[0]);

        if ((solicitud.length == 0 && propietario[0].Mascota.Usuario.id_usuario != req.id_usuario) ||
            (solicitud.length > 0 && solicitud[0].estado != "Aceptada")) {

            res.json({ status: "noFriends" });

        } else {

            const comment = await Comentarios.create({
                comentario: req.body.comentario,
                usuario: req.id_usuario,
                id_post: req.body.id_post
            })



            res.json({ status: true, data: comment });

        }

    }

});

router.post("/getPetList", [verifyToken, bodyParser.json()], async (req, res) => {

    const ownerPets = await Mascotas.findAll({
        include: [
            {
                model: Razas
            },
            {
                model: Usuarios
            }
        ],
        attributes: ['nombre', 'Foto_perfil', 'edad', 'propietario', 'genero', 'id_raza', 'id_mascota'],
        where: {
            propietario: req.id_usuario
        }
    });

    var i = 0;

    // get profile photo
    ownerPets.map((elem) => {

        var path = ownerPets[i].Foto_perfil;
        var data = fs.readFileSync(path, { encoding: 'utf-8' })
        ownerPets[i].Foto_perfil = data;

        i++;
    });

    var amigos = await Solicitudes.findAll({
        include: [{
            include: [{
                include: [{
                    include: [{
                        model: Animales,
                        required: true
                    }],
                    model: Razas,
                    required: true
                }],
                model: Mascotas,
                as: 'mascotas'
            }],
            model: Usuarios,
            as: 'receptorTable',
            attributes: ['nombre', 'id_usuario']
        }, {
            include: [{
                include: [{
                    include: [{
                        model: Animales,
                        required: true
                    }],
                    model: Razas,
                    required: true
                }],
                model: Mascotas,
                as: 'mascotas',
            }],
            model: Usuarios,
            as: 'emisorTable',
            attributes: ['nombre', 'id_usuario']
        }],
        where: {
            [op.and]: [{
                estado: 'Aceptada'
            }, {
                [op.or]: [{
                    emisor: req.id_usuario
                }, {
                    receptor: req.id_usuario
                }]
            }],

        }
    })

    //manage to avoid sending repeated and useless user data.
    var i = 0;
    while (i < amigos.length) {

        var tmp1 = amigos[i].id;
        var tmp2 = amigos[i].estado;
        var tmp3 = amigos[i].emisor;
        var tmp4 = amigos[i].receptor;
        var tmp5 = amigos[i].receptorTable;
        var tmp6 = amigos[i].emisorTable;

        amigos[i] = {
            id: tmp1,
            estado: tmp2,
            emisor: tmp3,
            receptor: tmp4,
        }

        if (tmp5.id_usuario == req.id_usuario) {

            amigos[i].amigo = tmp6;

        } else {

            amigos[i].amigo = tmp5;
        }

        i++;

    }


    // get pets profile photo
    amigos.map((solicitud) => {

        solicitud.amigo.mascotas.map((elem) => {

            var path = elem.Foto_perfil;
            var data = fs.readFileSync(path, { encoding: 'utf-8' })
            elem.Foto_perfil = data;

            i++;

        });

    });


    res.json({ status: true, data: amigos });

});

router.post("/getPetsPosts", [verifyToken, bodyParser.json()], async (req, res) => {

    const posts = await Posts.findAll({

        include: [{
            model: Mascotas,
            required: true,
            where: {
                propietario: req.id_usuario,
            }
        }],
        order: [['createdAt', 'DESC']]
    });

    var dirname;
    var filePost;

    var realPosts = [];

    // filter posts depending on number of page demanded by user
    for (var i = 0; i < posts.length; i++) {

        posts[i].Mascota.Foto_perfil = fs.readFileSync(posts[i].Mascota.Foto_perfil, { encoding: 'utf-8' });

        dirname = posts[i].imagen.split("petImages/" + posts[i].Mascota.id_mascota + "/")[1];
        filePost = dirname.split('.txt')[0];

        //getting image from path
        posts[i].imagen = fs.readFileSync(posts[i].imagen, { encoding: 'utf-8' })
        realPosts.push(posts[i]);

    }



    res.json({ status: true, data: realPosts });

});

router.post("/deletePost", [verifyToken, bodyParser.json()], async (req, res) => {

    //COMPROBAR QUE EL ID DEL POST A ELIMINAR ES REALMENTE 
    const resultado = await Posts.findAll({
        include: [{
            model: Mascotas,
            where: {
                propietario: req.id_usuario,
            },
            required: true
        }],
        where: {
            id_post: req.body.id_post
        }
    })

    console.log(resultado);

    if (resultado.length == 1) {

        const path = await Posts.findAll({
            attributes: ['imagen'],
            where: {
                id_post: req.body.id_post
            }
        })

        fs.unlinkSync(path[0].dataValues.imagen);


        await Posts.destroy({
            where: {
                id_post: req.body.id_post
            }
        })


        res.json({ status: true });

    } else {

        res.json({ status: false });

    }

})

router.post("/deletePet", [verifyToken, bodyParser.json()], async (req, res) => {

    // CHECK IF USER IS THE PET OWNER
    const result = await Mascotas.findAll({
        where: {
            [op.and]: [{
                propietario: req.id_usuario
            }, {
                id_mascota: req.body.id_mascota
            }]
        }
    })

    if (result.length == 1) {

        var dirname = __dirname;
        dirname = dirname.replace("routes", "database");
        var dirname = dirname.concat("/petImages/" + req.body.id_mascota);

        fs.rmdirSync(dirname, { recursive: true });

        await Mascotas.destroy({
            where: {
                id_mascota: req.body.id_mascota
            }
        })

        res.json({ status: true });

    } else {

        res.json({ status: false, msg: "badOwner" });

    }

});

router.post("/getBasicInfo", [verifyToken, bodyParser.json()], async (req, res) => {

    const pet = await Mascotas.findAll({

        include: [{

            include: [{

                attributes: ['id_animal'],
                model: Animales,
                required: true,

            }],

            attributes: ['id_raza'],
            model: Razas,
            required: true,

        }],

        attributes: ['nombre', 'edad', 'genero'],
        where: {

            id_mascota: req.body.id_mascota

        }

    });

    pet[0].genero = genre[pet[0].genero];

    res.json({ status: true, data: pet });

});

router.post("/modifyProfile", [verifyToken, bodyParser.json({ limit: '50mb' })], async (req, res) => {

    const now = new Date();
    const minPetYear = now.getUTCFullYear() - 60;

    if (!petNameRegExp.test(req.body.nombre)) {

        res.json({ status: "badName" });

    } else if (req.body.year == undefined || req.body.year == "" || parseInt(req.body.year) > now.getUTCFullYear() || parseInt(req.body.year) < minPetYear) {

        res.json({ status: "badYear" });

    } else if (parseInt(req.body.genre) != 1 && parseInt(req.body.genre) != 2 && parseInt(req.body.genre) != 3) {

        res.json({ status: "badGenre" });

    } else {

        const raza = await Razas.findAll({

            where: {
                id_raza: req.body.raza
            }

        })

        if (raza.length != 1) {

            res.json({ status: "badRaza" });

        } else {

            const pet = await Mascotas.update({

                nombre: req.body.nombre,
                edad: req.body.year,
                genero: req.body.genre,
                id_raza: req.body.raza,

            }, {

                where: {
                    id_mascota: req.body.id_mascota
                }

            })

            if (req.body.img != "#") {

                const path = await Mascotas.findAll({
                    attributes: ['Foto_perfil'],
                    where: {
                        id_mascota: req.body.id_mascota,
                    }
                });

                console.log(path);

                fs.writeFileSync(path[0].dataValues.Foto_perfil, req.body.img, (err) => {

                    console.log(err);

                });

            }

            res.json({ status: true });

        }

    }

});

module.exports = router;
