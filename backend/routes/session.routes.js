
// express, router and field validators for handlers
const express = require('express');
const bodyParser = require('body-parser');
const { response } = require('express');
const router = express.Router()
const { body } = require('../../common/node_modules/express-validator');
const { Sequelize, QueryTypes } = require('../../common/node_modules/sequelize');
const fs = require('fs');

// nodemailer for email sending
const nodemailer = require('../../common/node_modules/nodemailer');

const sequelize = new Sequelize('petsbook', 'root', '1234', {
    host: 'localhost',
    dialect: 'mysql',/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});

const op = Sequelize.Op;

// database
const Usuarios = require('../../common/models/Usuarios');
const Autonomias = require('../../common/models/Autonomias');
const Provincias = require('../../common/models/Provincias');
const Municipios = require('../../common/models/Municipios');
const Mascotas = require('../../common/models/Mascotas');


// environment variables
require('../../common/node_modules/dotenv').config();

// security utils
var bcrypt = require('../../common/node_modules/bcrypt');
const saltRounds = 15;
const verifyToken = require('../middlewares/auth');
const jwt = require('../../common/node_modules/jsonwebtoken');
const Posts = require('../../common/models/Posts');
const Solicitudes = require('../../common/models/Solicitudes');

// we use regexp to avoid dependencies to validation libraries (like email validator) 
var emailRegExp = /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;
var nameRegExp = /^(([A-Za-z]+[\-\']?)*([A-Za-z]+)?\s)+([A-Za-z]+[\-\']?)*([A-Za-z]+)?$/;


router.post("/data", [bodyParser.json(), verifyToken], async (req, res) => {

    // getting db key from token
    var decoded = jwt.decode(req.headers['authorization']);

    // searching for db info to given key
    const info = await Usuarios.findAll({

        attributes: ['nombre', 'email', 'id_usuario', 'Foto_perfil'],
        include: [
            {
                include: [{
                    include: [{
                        attributes: ['nombre'],
                        model: Autonomias,
                        required: true
                    }],
                    attributes: ['nombre'],
                    model: Provincias,
                    required: true
                }],
                attributes: ['nombre'],
                model: Municipios,
                required: true
            }

        ],
        where: {
            id_usuario: decoded.id_usuario
        }

    });

    if (info.length != 1) {

        res.json({ status: false });

    } else {

        var path = info[0].Foto_perfil;
        var data = fs.readFileSync(path, { encoding: 'utf-8' })
        info[0].Foto_perfil = data;

        res.send({

            status: true,
            usuario: info[0].dataValues,

        });

    }




});

router.post("/sites/ca", async (req, res) => {

    const ca = await Autonomias.findAll({

        attributes: ['nombre', 'id_autonomia'],

    });

    var i = 0;
    ca.map((elem) => {

        ca[i] = elem.dataValues;
        i++;

    });

    res.send({ status: true, data: ca });

})

router.post("/sites/provincias", bodyParser.json(), async (req, res) => {

    console.log(req.body);

    const prov = await Provincias.findAll({

        attributes: ['nombre', 'id_provincia'],
        where: {

            id_autonomia: req.body.id_autonomia

        }

    });

    var i = 0;
    prov.map((elem) => {

        prov[i] = elem.dataValues;
        i++;

    });

    res.send({ status: true, data: prov });

})

router.post("/sites/municipios", bodyParser.json(), async (req, res) => {

    console.log(req.body);

    const muni = await Municipios.findAll({

        attributes: ['nombre', 'codine'],
        where: {

            id_provincia: req.body.id_provincia

        }

    });

    console.log(muni);

    var i = 0;
    muni.map((elem) => {

        muni[i] = elem.dataValues;
        i++;

    });

    console.log(muni);

    res.send({ status: true, data: muni });

});



router.post("/register", bodyParser.json({ limit: '50mb' }), async (req, res) => {
    console.log("REGISTER");

    // server side field validation
    if (!nameRegExp.test(req.body.name)) {

        res.json({ status: "name" });

    } else if (!emailRegExp.test(req.body.email)) {

        res.json({ status: "badEmail" });

    } else if (req.body.password != req.body.repeated) {

        res.json({ status: "rpassword" });

    } else if (req.body.password.length < 8) {

        res.json({ status: "password" });

    } else if (req.body.Foto_perfil == "#") {

        res.json({ status: "badImage" });

    } else {

        var idmuni = await Municipios.findAll({

            attributes: ['codine'],
            where: {

                codine: req.body.municipio

            }

        });

        if (idmuni.length == 0) {

            res.json({ status: "badMuni" });

        }

        // database register validation
        var isRegistred = await Usuarios.findAll({

            attributes: ['email'],
            where: {

                email: req.body.email

            }

        });

        if (isRegistred.length != 0) {

            res.json({ status: "emailFound" });

        } else {

            //creating user
            bcrypt.hash(req.body.password, saltRounds, async function (err, hash) {

                const user = await Usuarios.create({

                    nombre: req.body.name,
                    password: hash,
                    email: req.body.email,
                    municipio: parseInt(req.body.municipio),
                    recovery: null,
                    fechaRecovery: null,
                    Foto_perfil: null

                });

                var dirname = __dirname;
                dirname = dirname.replace("routes", "database");
                var dirname = dirname.concat("/userImages/" + user.id_usuario);

                fs.mkdir(dirname, { recursive: true }, (err) => {
                    if (err) throw err;
                });

                //creating profile proho file
                fs.writeFile(dirname + '/profile.txt', req.body.Foto_perfil, (err) => {

                    if (err) throw err;

                    console.log('profile photo saved!');
                });

                user.Foto_perfil = dirname + '/profile.txt';
                await user.save();

                console.log(user);

                // returning json token  
                jwt.sign({

                    email: req.body.email,
                    id_usuario: user.dataValues.id_usuario,
                    exp: Math.floor(Date.now() / 1000) + (60 * 60),

                },
                    'shhhhh',
                    function (err, token) {

                        if (err) res.json({ status: "errorSigningToken" });
                        res.json({ status: true, token: token });

                    })

                console.log("Nuevo usuario creado!");

            });

        }

    }

})

router.post("/login", bodyParser.json(), async (req, res) => {

    console.log("body recibido: " + req.body.email);

    // server side field validation
    if (!emailRegExp.test(req.body.email)) {

        res.json({ status: "badEmail" });

    } else {

        // checking if user exists in database
        // database register validation
        var isRegistred = await Usuarios.findAll({

            attributes: ['email', 'password', 'id_usuario'],
            where: {

                email: req.body.email

            }
        });


        if (isRegistred.length == 0) {

            res.json({ status: "emailNotFound" });

        } else {

            // check for password vality
            bcrypt.compare(req.body.password, isRegistred[0].dataValues.password, function (err, iguales) {

                if (!iguales) {

                    res.json({ status: "badPassword" });

                } else {

                    // returning session
                    // returning json token to keep session 
                    jwt.sign({

                        email: req.body.email,
                        id_usuario: isRegistred[0].dataValues.id_usuario,
                        exp: Math.floor(Date.now() / 1000) + (60 * 60),

                    },
                        'shhhhh',
                        function (err, token) {

                            if (err) {

                                res.json({ status: "errorSigningToken" })

                            } else {

                                res.json({ status: true, token: token });

                            }

                        })

                }

            });

        }

    }

});


router.post("/check", (req, res) => {

    console.log("Verifying token...");

    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]

    console.log(jwt.decode(token));

    if (jwt.decode(token) == null) {

        res.json({ status: "noConnected" });

    } else {

        req.id_usuario = jwt.decode(token).id_usuario;

        if (req.id_usuario == null) res.json({ status: "noConnected" });

        // unauthotized
        if (token == null || token == undefined) return res.json({ status: "noConnected" });

        jwt.verify(token, 'shhhhh', (err, decoded) => {

            // session expired
            if (err) return res.json({ status: "noConnected" });

            // correct. Saving token in header
            req.headers['authorization'] = token;

        });

        res.json({ status: true });

    }



});


router.post("/forgot", bodyParser.json(), async (req, res) => {

    console.log(req.body.email);

    // email validation
    if (!emailRegExp.test(req.body.email)) {

        res.json({ status: "badEmail" });

    } else {

        // check db email
        var isRegistred = await Usuarios.findAll({

            attributes: ['email'],
            where: {

                email: req.body.email

            }

        });

        if (isRegistred.length == 0) {

            res.json({ status: "emailNotFound" });

        } else {

            require('crypto').randomBytes(3, async function (err, buffer) {

                var token = buffer.toString('hex');

                var dt = new Date();
                dt.setMinutes(dt.getMinutes() + 1);

                //save recovery code in db
                const saved = await Usuarios.update({

                    recovery: token,
                    fechaRecovery: dt

                }, {

                    where: {
                        email: req.body.email
                    },

                })
                // sending token
                //create transport
                let transporter = nodemailer.createTransport({

                    service: 'Gmail',
                    auth: {

                        user: 'petsbook.help@gmail.com',
                        pass: "contrapetsbook1"

                    }
                });

                // Configure mail data
                let mailOptions = {

                    from: 'petsbook.help@gmail.com',
                    to: req.body.email,
                    subject: 'Recuperación de la contraseña.',
                    text: 'Código de recuperación: \n\n' + token + "\n\nIntroduzca este código en PetsBook, en la página de restablecimiento de contraseña y cambie su contraseña.",

                };

                transporter.sendMail(mailOptions, function (err, data) {

                    if (err) {

                        console.log(err);

                    } else {

                        res.json({ status: true });
                        console.log("EMAIL ENVIADO!!");

                    }
                });

            });
        }

    }

});


router.post("/restore", bodyParser.json(), async (req, res) => {

    // email field validation
    if (!emailRegExp.test(req.body.email)) {

        res.json({ status: "badEmail" });

    } else {

        // check db email
        var recovery = await Usuarios.findAll({

            attributes: ['email', 'recovery', 'fechaRecovery', 'id_usuario'],
            where: {

                email: req.body.email

            }
        });

        if (recovery.length == 0) {

            res.json({ status: "emailNotFound" });

        } else if (req.body.password != req.body.repeated) {

            res.json({ status: "rpassword" });

        } else if (req.body.password.length < 8) {

            res.json({ status: "password" });

        } else {

            var now = new Date();


            if (recovery[0].dataValues.recovery != req.body.recovery) {

                res.json({ status: "badHash" });

            } else if (recovery[0].dataValues.fechaRecovery.getTime() < now.getTime()) {

                res.json({ status: "expiredHash" });

            } else {

                //generate crypt to password
                bcrypt.hash(req.body.password, saltRounds, async function (err, hash) {

                    //save new password
                    const newPassword = await Usuarios.update({

                        password: hash,

                    }, {
                        where: {

                            email: req.body.email
                        },
                    })

                    // generate jwt to keep session and send it.
                    jwt.sign({

                        email: req.body.email,
                        id_usuario: recovery[0].dataValues.id_usuario,
                        exp: Math.floor(Date.now() / 1000) + (60 * 60),

                    },
                        'shhhhh',
                        function (err, token) {

                            if (err) {

                                res.json({ status: "errorSigningToken" });

                            } else {

                                res.json({ status: true, token: token });

                            }

                        })

                });

            }

        }

    }

})

router.post("/getPosts", [verifyToken, bodyParser.json()], async (req, res) => {

    var offset = ((req.body.page - 1) * 5);
    var limit = 5;

    const posts = await sequelize.query("SELECT Posts.*, Mascotas.nombre, Mascotas.Foto_perfil FROM Mascotas JOIN (SELECT IF(emisor = ?, receptor, emisor) AS id_amigo FROM Solicitudes WHERE receptor = ? OR emisor = ?) AS A ON Mascotas.propietario = A.id_amigo JOIN Posts ON Posts.id_mascota = Mascotas.id_mascota ORDER BY Posts.createdAt DESC LIMIT ?,?", {
        replacements: [req.id_usuario, req.id_usuario, req.id_usuario, offset, limit],
        type: QueryTypes.SELECT,
        mapToModel: true,
        model: Posts
    });

    var dirname;
    var filePost;

    var realPosts = [];

    // filter posts depending on number of page demanded by user
    for (var i = 0; i < posts.length; i++) {

        //getting post image
        dirname = posts[i].imagen.split("petImages/" + posts[i].id_mascota + "/")[1];
        filePost = dirname.split('.txt')[0];


        posts[i].imagen = fs.readFileSync(posts[i].imagen, { encoding: 'utf-8' })
        realPosts.push(posts[i]);


        //getting profile image
        var url = posts[i].dataValues.Foto_perfil;
        console.log(posts[i].dataValues.Foto_perfil);
        var data = fs.readFileSync(url, { encoding: 'utf-8' })
        posts[i].dataValues.Foto_perfil = data;


    }

    res.json({ status: true, data: realPosts });

});

router.post("/deleteSession", [verifyToken, bodyParser.json()], async (req, res) => {

    /*
    const mascotas = await Mascotas.findAll({
        attributes: ['Foto_perfil'],
        where: {
            propietario: req.id_usuario
        }
    })

    for (var i = 0; i < mascotas.length; i++) {

        var dirname = __dirname;
        dirname = dirname.replace("routes", "database");
        var dirname = dirname.concat("/petImages/" + req.body.id_mascota);

        fs.rmdirSync(dirname, { recursive: true });

        await Mascotas.destroy({
            where: {
                id_mascota: req.body.id_mascota
            }
        })

    }*/

    const path = await Usuarios.findAll({
        attributes: ['Foto_perfil'],
        where: {
            id_usuario: req.id_usuario
        }
    })

    console.log(path);

    
    await Usuarios.destroy({
        where: {
            id_usuario: req.id_usuario
        }
    })
    

    fs.rmdirSync(path[0].dataValues.Foto_perfil.split("/profile.txt")[0], { recursive: true });

    res.json({ status: true })

});

router.post("/getProfileData", [verifyToken, bodyParser.json()], async (req, res) => {

    const result = await Usuarios.findAll({
        include: [{
            include: [{
                include: [{
                    attributes: ['id_autonomia'],
                    model: Autonomias,
                    required: true
                }],
                attributes: ['id_provincia'],
                model: Provincias,
                required: true
            }],
            attributes: ['codine'],
            model: Municipios,
            required: true,
        }],
        attributes: ['nombre', 'email'],
        where: {
            id_usuario: req.id_usuario
        }
    })

    res.json({ status: true, data: result })

});

router.post("/modifyProfile", [verifyToken, bodyParser.json({ limit: '50mb' })], async (req, res) => {

    // server side field validation
    if (!nameRegExp.test(req.body.name)) {

        res.json({ status: "name" });

    } else if (!emailRegExp.test(req.body.email)) {

        res.json({ status: "badEmail" });

    } else {

        var idmuni = await Municipios.findAll({

            attributes: ['codine'],
            where: {

                codine: req.body.municipio

            }

        });

        if (idmuni.length == 0) {

            res.json({ status: "badMuni" });

        }

        // database register validation
        var isRegistred = await Usuarios.findAll({

            attributes: ['email'],
            where: {

                email: req.body.email,
                id_usuario: {
                    [op.ne]: req.id_usuario
                }

            }

        });

        if (isRegistred.length != 0) {

            res.json({ status: "emailFound" });

        } else {

            const user = await Usuarios.update({

                nombre: req.body.name,
                email: req.body.email,
                municipio: parseInt(req.body.municipio),

            }, {

                where: {

                    id_usuario: req.id_usuario

                }

            });

            const info = await Usuarios.findAll({

                attributes: ['nombre', 'email', 'id_usuario', 'Foto_perfil'],
                include: [
                    {
                        include: [{
                            include: [{
                                attributes: ['nombre'],
                                model: Autonomias,
                                required: true
                            }],
                            attributes: ['nombre'],
                            model: Provincias,
                            required: true
                        }],
                        attributes: ['nombre'],
                        model: Municipios,
                        required: true
                    }

                ],
                where: {
                    id_usuario: req.id_usuario
                }

            });

            res.json({ status: true, data: info });

            if (req.body.img != "#") {

                const path = await Usuarios.findAll({
                    attributes: ['Foto_perfil'],
                    where: {
                        id_usuario: req.id_usuario,
                    }
                });

                console.log(path);

                fs.writeFileSync(path[0].dataValues.Foto_perfil, req.body.img, (err) => {

                    console.log(err);

                });

            }

            res.json({ status: true });

        }

    }

});

router.post("/modifyPassword", [verifyToken, bodyParser.json()], async (req, res) => {

    if (req.body.password != req.body.repeated) {

        res.json({ status: "rpassword" });

    } else if (req.body.password.length < 8) {

        res.json({ status: "password" });

    } else {

        const user = await Usuarios.findAll({
            attributes: ['password'],
            where: {
                id_usuario: req.id_usuario
            }
        })

        bcrypt.compare(req.body.current, user[0].dataValues.password, function (err, iguales) {

            if (!iguales) {

                res.json({ status: "badPassword" });

            } else {

                bcrypt.hash(req.body.password, saltRounds, async function (err, hash) {

                    await Usuarios.update({

                        password: hash

                    }, {

                        where: {

                            id_usuario: req.id_usuario

                        }
                    })


                });

                res.json({ status: true });

            }
        });

    }

});

module.exports = router;
