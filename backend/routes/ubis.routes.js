const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router()
const { Sequelize } = require('../../common/node_modules/sequelize');
const fs = require('fs');
const verifyToken = require('../middlewares/auth');

const op = Sequelize.Op;

const Provincias = require('../../common/models/Provincias');
const Autonomias = require('../../common/models/Autonomias');
const Municipios = require('../../common/models/Municipios');
const Usuarios = require('../../common/models/Usuarios');
const Ubicaciones = require('../../common/models/Ubicaciones');
const ComentariosUbis = require('../../common/models/ComentariosUbis');

const typeUbi = {
    "1": "Parque",
    "2": "Tienda",
    "3": "Veterinario"
}


router.post("/getUbis", [verifyToken, bodyParser.json()], async (req, res) => {

    // json for location site
    var j_sitio = {};
    var j_name = {};

    if (req.body.name != "") {

        j_name = {
            titulo: {
                [op.substring]: req.body.name
            }
        }

    }

    if (parseInt(req.body.municipio) == 0) {

        if (parseInt(req.body.provincia) == 0) {

            if (parseInt(req.body.autonomia) == 0) {

                console.log("JIER");

                j_sitio = {

                    include: [{

                        include: [{

                            model: Autonomias,
                            required: true

                        }],

                        model: Provincias,
                        required: true
                    }],

                    model: Municipios,
                    required: true

                }

            } else {

                j_sitio = {

                    include: [{

                        include: [{

                            model: Autonomias,
                            where: {
                                id_autonomia: req.body.autonomia
                            },
                            required: true

                        }],

                        model: Provincias,
                        required: true
                    }],

                    model: Municipios,
                    required: true

                }

            }

        } else {

            j_sitio = {


                include: [{

                    include: [{

                        model: Autonomias,
                        required: true

                    }],

                    model: Provincias,
                    where: {
                        id_provincia: req.body.provincia
                    },
                    required: true
                }],

                model: Municipios,
                required: true

            }

        }

    } else {

        j_sitio = {

            include: [{

                include: [{

                    model: Autonomias,
                    required: true

                }],

                model: Provincias,
                required: true
            }],

            model: Municipios,
            where: {
                codine: req.body.municipio
            },
            required: true

        }

    }

    var Ubis;

    if (req.body.tipo == "0") {

        Ubis = await Ubicaciones.findAll({
            include: [
                j_sitio
            ],
            attributes: ['titulo', 'id_ubi', 'lat', 'long', 'municipio', 'descripcion', 'tipo'],
            where: [j_name]
        });

    } else {

        Ubis = await Ubicaciones.findAll({
            include: [
                j_sitio
            ],
            attributes: ['titulo', 'id_ubi', 'lat', 'long', 'municipio', 'descripcion', 'tipo'],
            where: [{
                tipo: typeUbi[req.body.tipo]
            }, j_name]
        });

    }


    console.log(Ubis);

    res.json({ status: true, data: Ubis });

})

router.post("/getComments", [verifyToken, bodyParser.json()], async (req, res) => {

    const comentarios = await ComentariosUbis.findAll({
        include: [{
            attributes: ['nombre', 'id_usuario', 'Foto_perfil'],
            model: Usuarios,
            as: 'escritor'
        }],
        where: {
            lugar: req.body.ubicacion
        }
    });

    var i = 0;

    
    // get profile photo
    comentarios.map((elem) => {

        var path = elem.escritor.Foto_perfil;
        var data = fs.readFileSync(path, { encoding: 'utf-8' })
        elem.escritor.Foto_perfil = data;

        i++;
    });
    

    res.json({ status: true, data: comentarios });

})

router.post("/sendComment", [verifyToken, bodyParser.json()], async (req, res) => {

    if (req.body.ubicacion < 0 || req.body.ubicacion == undefined || req.body.ubicacion == null) {

        res.json({ status: "badUbi" });

    } else {

        if (req.body.comentario == "") {

            res.json("badComment");

        } else {

            await ComentariosUbis.create({
                emisor: req.id_usuario,
                lugar: req.body.ubicacion,
                comentario: req.body.comentario
            });

            const comentarios = await ComentariosUbis.findAll({
                include: [{
                    attributes: ['nombre', 'id_usuario'],
                    model: Usuarios,
                    as: 'escritor',
                }],
                where: {
                    lugar: req.body.ubicacion
                }
            });

            res.json({ status: true, data: comentarios });

        }

    }

});

router.post("/registerUbi", [verifyToken, bodyParser.json()], async (req, res) => {

    if ((req.body.lat == "") || (req.body.lng == "") || (req.body.lat == null) || (req.body.lng == null)) {

        res.json({ status: "badLocation" });

    } else {

        if (req.body.titulo == undefined || req.body.titulo == "" || req.body.titulo == null) {

            res.json({ status: "badTitle" });

        } else {

            if (req.body.descripcion == undefined || req.body.descripcion == "" || req.body.descripcion == null) {

                res.json({ status: "badDescription" });

            } else {

                if (parseInt(req.body.tipo) == NaN || parseInt(req.body.tipo) > 3 || parseInt(req.body.tipo) < 0) {

                    res.json({ status: "badType" });

                } else {

                    if (req.body.municipio == "" || req.body.municipio == null || req.body.municipio == undefined || req.body.municipio == 0) {

                        res.json({ status: "badMuni" });

                    } else {

                        const ubi = await Ubicaciones.create({
                            titulo: req.body.titulo,
                            descripcion: req.body.descripcion,
                            lat: req.body.lat,
                            long: req.body.lng,
                            municipio: req.body.municipio,
                            tipo: typeUbi[req.body.tipo]
                        })

                        res.json({ status: true });

                    }

                }

            }
        }

    }

});

module.exports = router;