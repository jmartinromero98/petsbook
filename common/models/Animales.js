const { Sequelize, DataTypes } = require('sequelize');

const sequelize = new Sequelize('petsbook', 'root', '1234', {
    host: 'localhost',
    dialect: 'mysql',/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});

const Animales = sequelize.define('Animales', {
    id_animal: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    },
    nombre: {
        type: DataTypes.STRING,
        allowNull: false,
    }
});

module.exports = Animales;