const { Sequelize, DataTypes, MACADDR } = require('sequelize');
const Usuarios = require('../models/Usuarios');
const Autonomias = require('../models/Autonomias');
const Provincias = require('../models/Provincias');
const Municipios = require('../models/Municipios');
const Animales = require('../models/Animales');
const Comentarios = require('../models/Comentarios');
const Mascotas = require('../models/Mascotas');
const Mensajes = require('../models/Mensajes');
const Posts = require('../models/Posts');
const Razas = require('../models/Razas');
const Solicitudes = require('../models/Solicitudes');
const Ubicaciones = require('../models/Ubicaciones');
const ComentariosUbis = require('./ComentariosUbis');
const Asociaciones = require('../models/Asociaciones');

const sequelize = new Sequelize('petsbook', 'root', '1234', {
    host: 'localhost',
    dialect: 'mysql',/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});

Usuarios.belongsTo(Municipios, {foreignKey: { name: 'municipio', allowNull: false}});

Provincias.belongsTo(Autonomias, {foreignKey: { name: 'id_autonomia', allowNull: false}});

Municipios.belongsTo(Provincias, {foreignKey: { name: 'id_provincia', allowNull: false}});
Razas.belongsTo(Animales, {foreignKey: { name: 'id_tipo', allowNull: false}});

Mascotas.belongsTo(Usuarios, { foreignKey: { name: 'propietario', allowNull: false}, onDelete: 'CASCADE'});
Mascotas.belongsTo(Razas, {foreignKey: { name: 'id_raza', allowNull: false}});

Comentarios.belongsTo(Usuarios, {foreignKey: { name: 'usuario', allowNull: false}, onDelete: 'CASCADE'});
Comentarios.belongsTo(Posts, {foreignKey: { name: 'id_post', allowNull: false}, onDelete: 'CASCADE'});

Mensajes.belongsTo(Usuarios, {as: 'emTable',foreignKey: { name: 'emisor', allowNull: true}, onDelete: 'CASCADE'});
Mensajes.belongsTo(Usuarios,  {as: 'reTable', foreignKey: {name: 'receptor', allowNull: true}, onDelete: 'CASCADE'});

Posts.belongsTo(Mascotas, {foreignKey: { name: 'id_mascota', allowNull: false}, onDelete: 'CASCADE'});
Posts.hasMany(Comentarios, {foreignKey: { name: 'id_post', allowNull: true}});

Solicitudes.belongsTo(Usuarios, {as: 'emisorTable', foreignKey: { name: 'emisor', allowNull: true}, onDelete: 'CASCADE'});
Solicitudes.belongsTo(Usuarios, {as: 'receptorTable',foreignKey: { name: 'receptor', allowNull: true}, onDelete: 'CASCADE'});

Ubicaciones.belongsTo(Municipios, {foreignKey: { name: 'municipio', allowNull: false}});

ComentariosUbis.belongsTo(Usuarios, {as: 'escritor',foreignKey: { name: 'emisor', allowNull: false}, onDelete: 'CASCADE'});
ComentariosUbis.belongsTo(Ubicaciones, {as: 'lugarTable', foreignKey: {name: 'lugar', allowNull: false}});

Usuarios.hasMany(Solicitudes, {as: 'SolAsEmisor', foreignKey: 'emisor', allowNull: false});
Usuarios.hasMany(Solicitudes, {as: 'SolAsReceptor', foreignKey: 'receptor', allowNull: false});

Usuarios.hasMany(Mensajes, {as: 'c', foreignKey: 'emisor', allowNull: false});
Usuarios.hasMany(Mensajes, {as: 'd', foreignKey: 'receptor', allowNull: false});

Usuarios.hasMany(Mascotas, {as: 'mascotas', foreignKey:'propietario', allowNull: true});