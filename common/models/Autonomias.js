const { Sequelize, DataTypes } = require('sequelize');

const sequelize = new Sequelize('petsbook', 'root', '1234', {
    host: 'localhost',
    dialect: 'mysql',/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});

const Autonomias = sequelize.define('Autonomias', {
    nombre: {
        type: DataTypes.STRING(28)
    },
    id_autonomia: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    }
});

module.exports = Autonomias;