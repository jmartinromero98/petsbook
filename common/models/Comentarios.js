const { Sequelize, DataTypes } = require('sequelize');
const Posts = require('./Posts');
const Usuarios = require('./Usuarios');

const sequelize = new Sequelize('petsbook', 'root', '1234', {
    host: 'localhost',
    dialect: 'mysql',/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});

const Comentarios = sequelize.define('Comentarios', {

    comentario: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
    id_comentario: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
    }
});

module.exports = Comentarios;