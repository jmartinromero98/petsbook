const { Sequelize, DataTypes } = require('sequelize');
const Ubicaciones = require('./Ubicaciones');
const Usuarios = require('./Usuarios');

const sequelize = new Sequelize('petsbook', 'root', '1234', {
    host: 'localhost',
    dialect: 'mysql',/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});

const ComentariosUbis = sequelize.define('ComentariosUbis', {

    comentario: {
        type: DataTypes.STRING,
    },

});

module.exports = ComentariosUbis;