const { Sequelize, DataTypes } = require('sequelize');
const Razas = require('./Razas');
const Usuarios = require('./Usuarios');

const sequelize = new Sequelize('petsbook', 'root', '1234', {
    host: 'localhost',
    dialect: 'mysql',/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});

const Mascotas = sequelize.define('Mascotas', {
    nombre: {
        type: DataTypes.STRING,
        Genero: DataTypes.ENUM('Macho', 'Hembra'),
    },
    Foto_perfil: {
        type:DataTypes.STRING,
        allowNull: true
    },
    edad: {
        type: DataTypes.INTEGER,
    },
    id_mascota: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull:false,
    },
    genero: {
        type: DataTypes.ENUM('Macho','Hembra', 'Otro'),
        allowNull: false,
    }
});

module.exports = Mascotas;