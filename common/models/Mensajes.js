const { Sequelize, DataTypes } = require('sequelize');
const Usuarios = require('./Usuarios');

const sequelize = new Sequelize('petsbook', 'root', '1234', {
    host: 'localhost',
    dialect: 'mysql',/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});

const Mensajes = sequelize.define('Mensajes', {

    Texto: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    id_mensaje: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    }
});

module.exports = Mensajes;