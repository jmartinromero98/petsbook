const { Sequelize, DataTypes } = require('sequelize');
const Provincias  = require('./Provincias');

const sequelize = new Sequelize('petsbook', 'root', '1234', {
    host: 'localhost',
    dialect: 'mysql',/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});

const Municipios = sequelize.define('Municipios', {
    nombre: {
        type: DataTypes.STRING(47),
    },
    codine: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
    }
})

module.exports = Municipios;