const { Sequelize, DataTypes } = require('sequelize');
const Mascotas = require('./Mascotas');

const sequelize = new Sequelize('petsbook', 'root', '1234', {
    host: 'localhost',
    dialect: 'mysql',/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});

const Posts = sequelize.define('Posts', {
    titulo: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    Descripcion: {
        type: DataTypes.TEXT,
    },
    id_post: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    },
    imagen: {
        type:DataTypes.STRING,
        allowNull: false
    }
});

module.exports = Posts;