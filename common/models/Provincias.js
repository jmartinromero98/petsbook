const { Sequelize, DataTypes } = require('sequelize');
const Autonomias  = require('./Autonomias');

const sequelize = new Sequelize('petsbook', 'root', '1234', {
    host: 'localhost',
    dialect: 'mysql',/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});

const Provincias = sequelize.define('Provincias',  {

    nombre: {
        type: DataTypes.STRING(22),
    },
    id_provincia: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    }

});

module.exports = Provincias;
