const { Sequelize, DataTypes, SequelizeScopeError } = require('sequelize');
const Animales = require('./Animales');

const sequelize = new Sequelize('petsbook', 'root', '1234', {
    host: 'localhost',
    dialect: 'mysql',/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});

const Razas = sequelize.define('Razas', {
    id_raza: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },
    nombre: {
        type: DataTypes.STRING,
        allowNull: false,
    }

});

module.exports = Razas;