const { Sequelize, DataTypes } = require('sequelize');
const Usuarios = require('./Usuarios');

const sequelize = new Sequelize('petsbook', 'root', '1234', {
    host: 'localhost',
    dialect: 'mysql',/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});

const Solicitudes = sequelize.define('Solicitudes', {

    estado: {
        // podría hacer "rechazada" pero no entrará dentro del alcance del TFG.
        //  gestionaríamos que el emisor de una solicitud rechazada no la pudiera repetir. El receptor sí.
        type: DataTypes.ENUM('Enviada', 'Aceptada'), 
        allowNull: false,
    }

});

module.exports = Solicitudes;