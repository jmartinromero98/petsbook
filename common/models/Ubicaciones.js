const { Sequelize, DataTypes } = require('sequelize');
const Municipios = require('./Municipios');
const Usuarios = require('./Usuarios');

const sequelize = new Sequelize('petsbook', 'root', '1234', {
    host: 'localhost',
    dialect: 'mysql',/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});

const Ubicaciones = sequelize.define('Ubicaciones', {
    titulo: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    descripcion: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
    lat: {
        type: DataTypes.FLOAT,
    },
    long: {
        type: DataTypes.FLOAT,
    },
    id_ubi: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    tipo: {
    	type: DataTypes.ENUM('Parque','Tienda', 'Veterinario'),
        allowNull: false,
    }

});

module.exports = Ubicaciones;
