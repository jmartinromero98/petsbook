const { Sequelize, DataTypes } = require('sequelize');
const Municipios = require('./Municipios');

const sequelize = new Sequelize('petsbook', 'root', '1234', {
    host: 'localhost',
    dialect: 'mysql',/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});


const Usuarios = sequelize.define('Usuarios', {
    // Model attributes are defined here
    nombre: {
        type: DataTypes.STRING,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    recovery: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    fechaRecovery: {
        type: DataTypes.DATE,
        allowNull: true
    },
    id_usuario: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    Foto_perfil: {
        type:DataTypes.STRING,
        allowNull: true
    }  
});

module.exports = Usuarios;