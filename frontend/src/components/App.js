import PBNavbar from './PBNavbar';
import { Route, Switch } from 'react-router-dom';
import SessionCard from './SessionCard';
import MainContent from './MainContent';
import ListPage from './ListPage';
import PetList from './PetList';
import Pets from './Pets';
import Cropper from './Cropper';
import Friends from './Friends';
import { useState, useEffect } from 'react';
import Context from '../util/AppContext';
import Public from './Public';
import UserList from './UserList';
import { useHistory, useParams } from 'react-router-dom';
import Profile from './Profile';
import PetProfile from './PetProfile';
import MessageList from './MessageList';
import Modify from './Modify';
import Delete from './Delete';
import Sites from './Sites';
import requestHelper from './RequestHelper';

import { propTypes } from 'react-bootstrap/esm/Image';

function App() {

  // state hooks for session information.
  const [usuario, setUsuario] = useState(undefined);
  const [mascotas, setMascotas] = useState([]);
  const [amigos, setAmigos] = useState([]);
  const [mascotasAmigos, setMascotasAmigos] = useState([]);

  // state hook to check session. (create our own hook with useEffect to send to server and check token validation).
  const [connected, setConnected] = useState(false);

  const history = useHistory();

  let { param } = useParams();

  useEffect(() => {


    //check if we have cookies.
    if (document.cookie) {

      requestHelper.post('/session/check', {}, true).then((res) => {

        // if login succesfull, save jwt in cookie and redirect 
        if (res.status == true) {

          requestHelper.post('/session/data', {}, true).then((res) => {

            if (res.status == true) {

              setUsuario(res.usuario);

              requestHelper.post('/pets/getPets', {}, true).then((res) => {

                if (res.status == true) {

                  setMascotas(res.data);

                  requestHelper.post('/friends/getFriends', {}, true).then((res) => {

                    setAmigos(res);

                  });

                } else {

                  // problem fetching pets data
                  setConnected(false);
                  history.push("/");

                }

              });

            } else {

              //problem fetching user data
              setConnected(false);
              history.push("/");

            }

          })

          setConnected(true);

        } else {

          // bad token or expired token.
          setConnected(false);
          history.push("/");

        }

      });

    } else {

      //don't have token
      setConnected(false);
      history.push("/");

    }

  }, []);
  

  return (

    <>
      <div>
          <Context.Provider value={{usuario, amigos, setAmigos, setUsuario, mascotas, setMascotas, mascotasAmigos, setMascotasAmigos, connected, setConnected}}>
            <PBNavbar />
            <Switch>

              <Route path="/messages">
                <MessageList usuario={usuario} />
              </Route>

              <Route path="/sites">
                <Sites id_usuario={usuario && usuario.id_usuario} />
              </Route>

              <Route path="/login">
                <SessionCard type="Login" buttonName="Acceda" />
              </Route>

              <Route path="/register">
                <SessionCard type="Register" buttonName="Registrarse" />
              </Route>

              <Route path="/friends">
                {usuario && <Friends id_usuario={usuario.id_usuario} />}
              </Route>

              <Route path="/userList">
                <UserList />
              </Route>

              <Route path="/forgot">
                <SessionCard type="Forgot" buttonName="Enviar" />
              </Route>

              <Route path="/main">
                <MainContent />
              </Route>

              <Route path="/pets">
                <Pets usuario={usuario} />
              </Route>

              <Route path="/petList">
                <PetList></PetList>
              </Route>

              <Route path="/restore">
                <SessionCard type="Restore" buttonName="Cambiar"></SessionCard>
              </Route>

              <Route path="/user">
              {usuario && <Profile />}
              </Route>

              <Route path="/pet">
                <PetProfile usuario={usuario != null && usuario} />
              </Route>

              <Route path="/modify">
                <Modify usuario={usuario != null && usuario} />
              </Route>

              <Route path="/delete">
                <Delete id_usuario={usuario && usuario.id_usuario}/>
              </Route>

              <Route path="/">
                <Public />
              </Route>

            </Switch>
          </Context.Provider>
      </div>
    </>

  );

}

export default App;
