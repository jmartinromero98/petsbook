import Form from 'react-bootstrap/Form';
import { useEffect, useState } from 'react';
import requestHelper from './RequestHelper';

function BreedInput(props) {

    const [breedChecked, setBreedChecked] = useState(false);

    function getBreeds() {

        requestHelper.post('/pets/breeds', {
    
            "id_animal": document.getElementById('selectTipo').value,

        }, false).then((res) => {
    
    
            var select = document.getElementById('selectBreed');
    
            // crearing previous breeds
            while (select && select.options.length > 1) {
                select.remove(select.options.length - 1);
            }
    
            // we would manage not ERR_CONNECTION_REFUSED or similar errors in register and login
            res.data.map((elem) => {
    
                select.options[select.options.length] = new Option(elem.nombre, elem.id_raza);
    
            })

            if (props.defaultBreed != 0 && select.options.length > props.defaultBreed && !breedChecked) {

                setBreedChecked(true);

                select.options[props.defaultBreed].selected = 'selected';

            }
    
    
        })
    
    }

    useEffect(() => {

        requestHelper.post('/pets/type', {}, false).then((res) => {


            var select = document.getElementById('selectTipo');
            // we would manage not ERR_CONNECTION_REFUSED or similar errors in register and login
            res.data.map((elem) => {
                
                select.options[select.options.length] = new Option(elem.nombre, elem.id_animal);

                
            })



            if (props.defaultType != 0 && select.options.length > props.defaultType) {

                select.options[props.defaultType].selected = 'selected';
                getBreeds();

            }


        })

    },[props.defaultType]);


    return (
        <>
            <Form.Group id="exampleForm.SelectCustomSizeSm">
                <div className="input-label">Tipo de animal</div>
                <Form.Control className="my-1 mr-sm-2" id="selectTipo" as="select" onChange={(e) => {props.setBreed(0);getBreeds(); if ( props.onChange ) { props.onChange() } }} custom>
                    <option value="0"> Elija un tipo de animal</option>
                </Form.Control>
            </Form.Group>

            <Form.Group id="exampleForm.SelectCustomSizeSm">
                <div className="input-label">Raza/Especie de animal</div>
                <Form.Control className={props.isBreedValid.valid ? "my-1 mr-sm-2" :  "my-1 mr-sm-2 is-invalid"} onChange={e => {props.setBreed(e.target.value); props.setBreedValid({valid: true, msg: ""}); if ( props.onChange ) { props.onChange() }}} id="selectBreed" as="select" custom>
                    <option value="0"> Elija un raza o especie</option>
                </Form.Control>
                <div className="invalid-feedback">
                    {props.isBreedValid.msg}
                </div>
            </Form.Group>
        </>

    )

}

export default BreedInput;