import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import '../styles/ChatCol.scss';
import { useState, useContext } from 'react';
import MessageModal from './MessageModal';
import requestHelper from './RequestHelper';
import Row from 'react-bootstrap/Row';
import Context from '../util/AppContext';

function ChatCol() {


    const [friendSelected, setFriendSelected] = useState();
    const [input, setInput] = useState("");
    const [show, setShow] = useState(false);
    const [messages, setMessages] = useState([]);
    const [search, setSearch] = useState("");
    const [friendPhoto, setFriendPhoto] = useState();

    const context = useContext(Context);

    function handleMessages(e) {


        requestHelper.post('/friends/getMessages', { id: e }, true).then((res) => {

            if (res.status == true) {

                setMessages(res.data.msg);
                setFriendPhoto(res.data.foto);

            }

        });

    };

    function noAmigos() {

        if (context.amigos == 0) {

            return true;

        }

        for (var i = 0; i < context.amigos.length; i++) {

            if (context.amigos[i].estado == "Aceptada") {

                return false;

            }

        }

        return true;

    }


    return (

        <>
            <Card className="pb-chatcol-card">
                <Card.Header className="pb-chatcolcard-header sticky-top">Contactos</Card.Header>
                <ListGroup className="pb-chatcol-chatlist pb-invisible-scrollbar" variant="flush" style={{ padding: "0px !important" }}>
                {noAmigos() && <h1 className="pb-userlist-notfound" style={{ padding: "20px", fontSize: "20px" }}>Haz amigos para comenzar a mensajear con ellos</h1>}
                    {context.amigos.length > 0 && context.usuario.id_usuario && context.amigos.map((elem, idx) => {

                        if (elem.estado == "Aceptada") {
                            if (elem.receptor == context.usuario.id_usuario) {
                                elem = elem.emisorTable
                            } else {
                                elem = elem.receptorTable
                            }

                            return (
                                <li className="list-group-item" key={idx} style={{ display: elem.nombre.toLowerCase().indexOf(search.toLowerCase()) !== -1 ? "" : "none" }}>
                                    <div className="row pb-friends-friendlistcont container"
                                        onClick={() => {
                                            setShow(true);
                                            setFriendSelected(elem.id_usuario);
                                            handleMessages(elem.id_usuario);
                                        }}>

                                        <div className="col-4 pb-pets-petlistimgcol">
                                            <img className="pb-pets-petlistimg" src={elem && elem.Foto_perfil} alt="" >
                                            </img>
                                        </div>
                                        <div className="col-8 pb-pets-petlistcol">
                                            <Row style={{ display: "inline" }}>
                                                <div className="row pb-pets-petlistname">
                                                    {elem.nombre}
                                                </div>
                                                <div className="row">
                                                    {elem.Municipio && "En " + elem.Municipio.nombre + ", " + elem.Municipio.Provincia.Autonomia.nombre}
                                                </div>
                                            </Row>
                                        </div>
                                    </div>
                                </li>
                            )
                        }
                    })
                    }
                </ListGroup>
            </Card>
            <MessageModal show={show} onHide={() => { setShow(false); setMessages([]); setInput("") }} input={input} setInput={setInput} messages={messages} handleMessages={(msgs) => { setMessages(msgs) }} friendSelected={friendSelected} friendSelected={friendSelected} friendPhoto={friendPhoto}/>
            <Card className={"pb-searchcard" + context.class}>
                <input type="text" placeholder={"Busca un amigo para conversar"} style={{ margin: "10px" }} value={search} onChange={(e) => { setSearch(e.target.value) }}></input>
            </Card>
        </>

    )
}

export default ChatCol;