import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import { useState, useEffect, useContext } from 'react';
import Modal from 'react-bootstrap/Modal';
import { useHistory } from 'react-router-dom';
import requestHelper from './RequestHelper';
import Context from '../util/AppContext';


function Delete(props) {

    //state variables for type of page
    const [page, setPage] = useState("mascotas");
    const [mascotas, setMascotas] = useState([]);
    const [amistades, setAmistades] = useState([]);
    const [publicaciones, setPublicaciones] = useState([]);
    const [item, setItem] = useState(null);

    /*
    const [sessionDeleted, setSessionDeleted] = useState(false);
    const [mascotaDeleted, setMascotaDeleted] = useState(false);
    const [amistadDeleted, setAmistadDeleted] = useState(false);
    const [PublicacionDeleted, setPublicacionDeleted] = useState(false);
    const [mensajeDeleted, setMensajeDeleted] = useState(false);
    */

    const history = useHistory();

    const [show, setShow] = useState(false);
    const handleClose = () => {
        setShow(false);
    }
    const [finallyDelete, setFinallyDelete] = useState(false);

    const context = useContext(Context);

    function getPetsPosts() {

        //getting pet posts
        requestHelper.post('/pets/getPetsPosts', {}, true).then((res) => {

            if (res.status == true) {

                setPublicaciones(res.data);

            }

        });
    }

    function getPets() {
        // getting pets
        requestHelper.post('/pets/getPets', {}, true).then((res) => {

            if (res.status == true) {

                setMascotas(res.data);
                context.setMascotas(res.data);

            }

        });
    }

    function getFriends() {

        // getting friends
        requestHelper.post('/friends/getFriends', {}, true).then((res) => {

            setAmistades(res);
            context.setAmigos(res);

        });


    }

    //mount components depending on type of page
    useEffect(() => {

        getPets();

        getFriends();

        getPetsPosts();

        return (setPage("cuenta"));

    }, [])

    function deletePost() {

        requestHelper.post('/pets/deletePost', { id_post: item.id_post }, true).then((res) => {

            if (res.status == true) {

                getPetsPosts();

            }

        });

        setItem(null);

    }

    function deletePet() {

        requestHelper.post('/pets/deletePet', { id_mascota: item.id_mascota }, true).then((res) => {

            if (res.status == true) {

                getPets();
                getPetsPosts();

            }

        });

        setItem(null);

    }

    function deleteMessages() {

        requestHelper.post('/friends/deleteMessages', { id_usuario: item.id_usuario }, true).then((res) => {

            //modal, mensajes eliminados con éxito.

        });

        setItem(null);

    }

    function deleteFriend() {

        requestHelper.post('/friends/deleteFriend', { id_usuario: item.id_usuario }, true).then((res) => {

            if (res.status == true) {

                getFriends();

            }

        });

        setItem(null);

    }

    function deleteSession() {

        requestHelper.post('/session/deleteSession', {}, true).then((res) => {

            if (res.status == true) {

                //delete cookies and redirect outside petsbook
                document.cookie = "authorization=";
                window.location.href = "/";

            }

        });

        setItem(null);

    }

    function deleteItem() {
        switch (page) {
            case "cuenta":
                deleteSession()
                    ; break;
            case "mascotas":
                deletePet();
                ; break;
            case "amistades":
                deleteFriend();
                ; break;
            case "publicaciones":
                deletePost();
                ; break;
            case "mensajes":
                deleteMessages();
                ; break;
        }
    }

    return (
        <>
            <Row style={{width: "100%"}}>
                <Col sm={4} style={{ paddingLeft: "30px", paddingRight: "30px" }}>
                    <ListGroup className="pb-session-modifymenu">
                        <ListGroup.Item style={page == "cuenta" ? { background: "gray" } : { background: "white" }} onClick={() => { setPage("cuenta") }}>
                            Cuenta
                        </ListGroup.Item >
                        <ListGroup.Item style={page == "mascotas" ? { background: "gray" } : { background: "white" }} onClick={() => { setPage("mascotas") }}>
                            Mascotas
                        </ListGroup.Item>
                        <ListGroup.Item style={page == "amistades" ? { background: "gray" } : { background: "white" }} onClick={() => { setPage("amistades") }}>
                            Amistades
                        </ListGroup.Item>
                        <ListGroup.Item style={page == "publicaciones" ? { background: "gray" } : { background: "white" }} onClick={() => { setPage("publicaciones") }}>
                            Publicaciones
                        </ListGroup.Item>
                        <ListGroup.Item style={page == "mensajes" ? { background: "gray" } : { background: "white" }} onClick={() => { setPage("mensajes") }}>
                            Mensajes
                        </ListGroup.Item>
                    </ListGroup>
                </Col>
                <Col sm={8} style={{ paddingLeft: "30px", paddingRight: "30px" }}>
                    <div style={{ paddingTop: "50px" }}></div>
                    {page == "cuenta" &&
                        <>
                            <div className="pb-session-titularcont row"><p className="pb-session-titular">¿Seguro que deseas eliminar toda la cuenta?</p>
                                <p>Al hacerlo eliminarás todos los datos relacionados contigo (Perfil, mascotas, amistades, mensajes...) excepto los comentarios.</p></div>
                            <Button variant="danger" onClick={() => { setShow(true); }}>Eliminar cuenta</Button>
                        </>
                    }
                    <ul className="list-group list-group-flush">
                        {page == "mascotas" &&

                            mascotas.map((elem, idx) => {
                                return (
                                    <li className="list-group-item" key={idx}>
                                        <div className="row pb-pets-petlistcont"
                                            onClick={(e) => { setShow(true); setItem(elem) }}>
                                            <div className="col-2 pb-pets-petlistimgcol">
                                                <img className="pb-pets-petlistimg" src={elem.Foto_perfil} alt="" >
                                                </img>
                                            </div>
                                            <div className="col-10 pb-pets-petlistcol">
                                                <Row style={{ display: "inline" }}>
                                                    <div className="row pb-pets-petlistname">
                                                        {elem.nombre}
                                                    </div>
                                                    <div className="row">
                                                        {elem.Raza.nombre}
                                                    </div>
                                                    <div className="row">
                                                        {elem.genero}
                                                    </div>
                                                </Row>
                                            </div>
                                            <div className="col pb-pets-petlistcol">
                                            </div>
                                        </div>
                                    </li>
                                )
                            })
                        }
                    </ul>
                    <ul className="list-group list-group-flush">
                        {(page == "amistades" || page == "mensajes") &&

                            amistades.map((elem, idx) => {
                                if (elem.estado == "Aceptada") {
                                    if (elem.receptor == props.id_usuario) {
                                        elem = elem.emisorTable
                                    } else {
                                        elem = elem.receptorTable
                                    }

                                    return (
                                        <li className="list-group-item" key={idx}>
                                            <div className="row pb-friends-friendlistcont container" /*style={ { display: elem.nombre.toLowerCase().indexOf(search.toLowerCase()) !== -1 ? "" : "none"} } */
                                                onClick={() => {
                                                    setShow(true);
                                                    if (page == "amistades") {
                                                        setItem(elem);
                                                    } else {
                                                        setItem(elem);
                                                    }
                                                }}>

                                                <div className="col pb-pets-petlistimgcol">
                                                    <img className="pb-pets-petlistimg" src={elem.Foto_perfil} alt="">
                                                    </img>
                                                </div>
                                                <div className="col-10 pb-pets-petlistcol">
                                                    <Row style={{ display: "inline" }}>
                                                        <div className="row pb-pets-petlistname">
                                                            {elem.nombre}
                                                        </div>
                                                        <div className="row">
                                                            {"En " + elem.Municipio.nombre + ", " + elem.Municipio.Provincia.Autonomia.nombre}
                                                        </div>
                                                    </Row>
                                                </div>
                                            </div>
                                        </li>
                                    )
                                }
                            })
                        }
                    </ul>
                    {page == "publicaciones" &&
                        publicaciones.map((post, idx) => {
                            return (
                                <Card className="pb-post-maincard" onClick={(e) => { setItem(post); setShow(true); }} key={idx}>
                                    <Card.Header >
                                        <div className="row">
                                            <div className="col-2">
                                                <img className="pb-post-profileimg" src={post.Mascota && post.Mascota.Foto_perfil} alt="" />
                                            </div>
                                            <div className="col-6 pb-post-profilename">
                                                {post.Mascota && post.Mascota.nombre}
                                            </div>
                                            <div className="col-4 pb-post-datecol">
                                                {new Date(post.createdAt).toLocaleDateString()}
                                            </div>
                                        </div>
                                    </Card.Header>
                                    <Card.Body>
                                        <Card.Title>{post.titulo}</Card.Title>
                                        <Card.Text>
                                            {post.Descripcion}
                                        </Card.Text>
                                        <img className="pb-post-image" src={post.imagen} alt="" />
                                    </Card.Body>
                                </Card>
                            )
                        })
                    }
                    <Modal
                        show={show}
                        onHide={handleClose}
                        keyboard={false}
                    >
                        <Modal.Header closeButton>
                            <Modal.Title>
                                Confirmacion de eliminacion
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            ¿Está seguro que desea eliminar estos datos? Recuerde que una vez realizada la operación no hay vuelta atrás.
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>
                                Cancelar
                            </Button>
                            <button id="actionButton" type="submit" className=" btn btn-danger" onClick={() => {
                                deleteItem();
                                setShow(false);
                            }}>
                                Aceptar
                            </button>
                        </Modal.Footer>
                    </Modal>
                </Col>
            </Row>
        </>
    )

}

export default Delete;