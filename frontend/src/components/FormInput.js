
function FormInput(props) {

    return (

        <div className="form-group ">
            <label className="input-label">{props.label}</label>
            <input
                value={props.value}
                onChange={props.onChange}
                type={props.type}
                name={props.type}
                className={props.valid ? "form-control" : "form-control is-invalid"}
                id={props.id} placeholder={"Introduzca " + props.name}
                maxLength={props.maxlength}
            />
            <div className="invalid-feedback">
                {props.msg}
            </div>
        </div>
        
    )
}

export default FormInput;