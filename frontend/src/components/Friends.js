import { Search, PlusSquare } from 'react-bootstrap-icons';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import FormInput from './FormInput';
import ListGroup from 'react-bootstrap/ListGroup';
import { useState, useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import requestHelper from './RequestHelper';
import Context from '../util/AppContext';

import '../styles/pets.scss';
import BreedInput from "./BreedInput";


function Friends(props) {


    const [search, setSearch] = useState("");
    const history = useHistory();
    const [amigos, setAmigos] = useState([]);

    var noRecibidas = true;
    var noEnviadas = true;

    var context = useContext(Context);

    useEffect(() => {

        getFriends();

    }, [])

    function cancel(e) {


        requestHelper.post('/friends/cancel', { "receptor": e.target.id }, true).then((res) => {

            //re-render
            getFriends();

        });

    }

    function accept(e) {


        requestHelper.post('/friends/accept', { "id": e.target.id }, true).then((res) => {

            //re-render
            getFriends();

        });

    }

    function remove(e) {


        requestHelper.post('/friends/remove', { "id": e.target.id }, true).then((res) => {

            //re-render
            getFriends()

        });

    }

    function getFriends() {

        requestHelper.post('/friends/getFriends', {}, true).then((res) => {

            setAmigos(res);
            context.setAmigos(res);

        });

    }

    return (
            <Row style={{width: "100%"}}>
                <Col>
                </Col>
                <Col xs={12} md={8} lg={6} style={{ paddingLeft: "30px", paddingRight: "30px" }}>

                    <Card className="pb-friends-upperCard" >

                        <Button className="pb-friends-upperbutton" variant="info" onClick={() => { history.push("/userList") }}>
                            <div className="pb-iconinline-container" style={{ display: "inline-flex" }} >
                                <Search height="0.9em" />
                            </div> Busca nuevos amigos
                        </Button>

                    </Card>

                    <Card className="pb-friends-resultcontainer">

                        <div className="pb-friends-searchcontainer">
                            <FormInput
                                value={search}
                                onChange={(e) => setSearch(e.target.value)}
                                valid={true}
                                msg={""}
                                type="text"
                                name="el nombre del amigo"
                                id="searchInput"
                                label="Nombre del amigo"
                            />
                        </div>

                        <div className="pb-friends-ownpetscontainer">

                            <ul className="list-group list-group-flush">
                                {amigos.length > 0 && amigos.map((elem, idx) => {
                                    if (elem.estado == "Enviada" && elem.emisor == props.id_usuario) {

                                        if (noEnviadas) {
                                            noEnviadas = false;
                                            return (
                                                <>
                                                    <div className="input-label" style={{ fontWeight: "bold" }}>Solicitudes enviadas</div>
                                                    <li className="list-group-item">
                                                        <div className="row pb-friends-friendlistcont container" style={{ display: elem.receptorTable.nombre.toLowerCase().indexOf(search.toLowerCase()) !== -1 ? "" : "none" }}
                                                            onClick={(e) => {
                                                                // if the target of the event isnt a child button.
                                                                if (e.target.type != "button") {
                                                                    history.push("user?id=" + elem.receptorTable.id_usuario)
                                                                }
                                                            }}>

                                                            <div className="col pb-pets-petlistimgcol">
                                                                <img src={elem.receptorTable.Foto_perfil} className="pb-pets-petlistimg" alt="">
                                                                </img>
                                                            </div>
                                                            <div className="col-6 pb-pets-petlistcol">
                                                                <Row style={{ display: "inline" }}>
                                                                    <div className="row pb-pets-petlistname">
                                                                        {elem.receptorTable.nombre}
                                                                    </div>
                                                                    <div className="row">
                                                                        {"En " + elem.receptorTable.Municipio.nombre + ", " + elem.receptorTable.Municipio.Provincia.Autonomia.nombre}
                                                                    </div>
                                                                </Row>
                                                            </div>
                                                            <div className="col pb-friends-friendlistcol">

                                                                <div className="col" style={{ display: "flex", alignItems: "center" }}>

                                                                    <Button variant="outline-dark" onClick={(elem) => cancel(elem)} id={elem.receptor} className="pb-friends-friendlistbutton">
                                                                        Cancelar solicitud
                                                                    </Button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </li>
                                                </>
                                            )
                                        } else {

                                            return (
                                                <li className="list-group-item">
                                                    <div className="row pb-friends-friendlistcont container" style={{ display: elem.receptorTable.nombre.toLowerCase().indexOf(search.toLowerCase()) !== -1 ? "" : "none" }}
                                                        onClick={(e) => {
                                                            // if the target of the event isnt a child button.
                                                            if (e.target.type != "button") {
                                                                history.push("user?id=" + elem.receptorTable.id_usuario)
                                                            }
                                                        }}>

                                                        <div className="col pb-pets-petlistimgcol">
                                                            <img src={elem.receptorTable.Foto_perfil} className="pb-pets-petlistimg" alt="" onError="../img/user_profile.png">
                                                            </img>
                                                        </div>
                                                        <div className="col-6 pb-pets-petlistcol">
                                                            <Row style={{ display: "inline" }}>
                                                                <div className="row pb-pets-petlistname">
                                                                    {elem.receptorTable.nombre}
                                                                </div>
                                                                <div className="row">
                                                                    {"En " + elem.receptorTable.Municipio.nombre + ", " + elem.receptorTable.Municipio.Provincia.Autonomia.nombre}
                                                                </div>
                                                            </Row>
                                                        </div>
                                                        <div className="col pb-friends-friendlistcol">

                                                            <div className="col" style={{ display: "flex", alignItems: "center" }}>

                                                                <Button variant="outline-dark" onClick={(elem) => cancel(elem)} id={elem.receptor} className="pb-friends-friendlistbutton">
                                                                    Cancelar solicitud
                                                                </Button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </li>
                                            )

                                        }

                                    }
                                })}
                            </ul>

                            <ul className="list-group list-group-flush">
                                {amigos.length > 0 && amigos.map((elem, idx) => {
                                    if (elem.estado == "Enviada" && elem.receptor == props.id_usuario) {

                                        if (noRecibidas) {
                                            noRecibidas = false;
                                            return (
                                                <>
                                                    <div className="input-label" style={{ fontWeight: "bold" }}>Solicitudes recibidas</div>
                                                    <li className="list-group-item" key={idx}>
                                                        <div className="row pb-friends-friendlistcont container" style={{ display: elem.emisorTable.nombre.toLowerCase().indexOf(search.toLowerCase()) !== -1 ? "" : "none" }}
                                                            onClick={(e) => {
                                                                if (e.target.type != "button") {
                                                                    history.push("user?id=" + elem.emisorTable.id_usuario)
                                                                }
                                                            }}>

                                                            <div className="col pb-pets-petlistimgcol">
                                                                <img src={elem.emisorTable.Foto_perfil} className="pb-pets-petlistimg" alt="" >
                                                                </img>
                                                            </div>
                                                            <div className="col-6 pb-pets-petlistcol align-items-center">
                                                                <Row style={{ display: "inline" }}>
                                                                    <div className="row pb-pets-petlistname">
                                                                        {elem.emisorTable.nombre}
                                                                    </div>
                                                                    <div className="row">
                                                                        {"En " + elem.emisorTable.Municipio.nombre + ", " + elem.emisorTable.Municipio.Provincia.Autonomia.nombre}
                                                                    </div>
                                                                </Row>

                                                            </div>
                                                            <div className="col pb-friends-friendlistcol" style={{ display: "block" }}>

                                                                <div className="row" style={{ display: "flex", alignItems: "center" }}>

                                                                    <Button variant="success" onClick={(elem) => accept(elem)} id={elem.id} className="pb-friends-friendlistbutton">
                                                                        Aceptar solicitud
                                                                    </Button>
                                                                </div>

                                                                <div className="row" style={{ display: "flex", alignItems: "center" }}>

                                                                    <Button variant="danger" onClick={(elem) => remove(elem)} id={elem.id} className="pb-friends-friendlistbutton">
                                                                        Rechazar solicitud
                                                                    </Button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </li>
                                                </>
                                            )

                                        } else {
                                            return (
                                                <li className="list-group-item" key={idx}>
                                                    <div className="row pb-friends-friendlistcont container" style={{ display: elem.emisorTable.nombre.toLowerCase().indexOf(search.toLowerCase()) !== -1 ? "" : "none" }}
                                                        onClick={(e) => {
                                                            if (e.target.type != "button") {
                                                                history.push("user?id=" + elem.emisorTable.id_usuario)
                                                            }
                                                        }}>

                                                        <div className="col pb-pets-petlistimgcol">
                                                            <img src={elem.emisorTable.Foto_perfil} className="pb-pets-petlistimg" alt="">
                                                            </img>
                                                        </div>
                                                        <div className="col-6 pb-pets-petlistcol align-items-center">
                                                            <Row style={{ display: "inline" }}>
                                                                <div className="row pb-pets-petlistname">
                                                                    {elem.emisorTable.nombre}
                                                                </div>
                                                                <div className="row">
                                                                    {"En " + elem.emisorTable.Municipio.nombre + ", " + elem.emisorTable.Municipio.Provincia.Autonomia.nombre}
                                                                </div>
                                                            </Row>

                                                        </div>
                                                        <div className="col pb-friends-friendlistcol" style={{ display: "block" }}>

                                                            <div className="row" style={{ display: "flex", alignItems: "center" }}>

                                                                <Button variant="success" onClick={(elem) => accept(elem)} id={elem.id} className="pb-friends-friendlistbutton">
                                                                    Aceptar solicitud
                                                                </Button>
                                                            </div>

                                                            <div className="row" style={{ display: "flex", alignItems: "center" }}>

                                                                <Button variant="danger" onClick={(elem) => remove(elem)} id={elem.id} className="pb-friends-friendlistbutton">
                                                                    Rechazar solicitud
                                                                </Button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </li>
                                            )
                                        }
                                    }
                                })}
                            </ul>
                            <div className="input-label" style={{ fontWeight: "bold" }}>Amigos</div>
                            <ul className="list-group list-group-flush">
                                {amigos.length > 0 && amigos.map((elem, idx) => {
                                    if (elem.estado == "Aceptada") {
                                        if (elem.receptor == props.id_usuario) {
                                            elem = elem.emisorTable
                                        } else {
                                            elem = elem.receptorTable
                                        }


                                        return (
                                            <li className="list-group-item" style={{ display: elem.nombre.toLowerCase().indexOf(search.toLowerCase()) !== -1 ? "" : "none" }} key={idx}>
                                                <div className="row pb-friends-friendlistcont container"
                                                    onClick={(e) => {
                                                        if (e.target.type != "button") {
                                                            history.push("user?id=" + elem.id_usuario)
                                                        }
                                                    }}>

                                                    <div className="col pb-pets-petlistimgcol">
                                                        <img src={elem.Foto_perfil} className="pb-pets-petlistimg" alt="" >
                                                        </img>
                                                    </div>
                                                    <div className="col-6 pb-pets-petlistcol">
                                                        <Row style={{ display: "inline" }}>
                                                            <div className="row pb-pets-petlistname">
                                                                {elem.nombre}
                                                            </div>
                                                            <div className="row">
                                                                {"En " + elem.Municipio.nombre + ", " + elem.Municipio.Provincia.Autonomia.nombre}
                                                            </div>
                                                        </Row>
                                                    </div>
                                                </div>
                                            </li>
                                        )
                                    }
                                })
                                }
                            </ul>
                        </div>

                    </Card>
                </Col>
                <Col>
                </Col>
            </Row>
    )

}

export default Friends;