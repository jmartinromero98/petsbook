import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';

function generateWall() {

    var wallContent = [];

    for (var i = 0; i < 100; i++) {

        wallContent[i] =
            <>
                <ListGroup.Item>Cras justo odio</ListGroup.Item>
                <ListGroup.Item>Dapibus ac facilisis in</ListGroup.Item>
                <ListGroup.Item>Vestibulum at eros</ListGroup.Item>
            </>;

    }

    return (wallContent.map((elem) => elem));
}

function List() {
    return (

        <Card style={{ marginTop: `30px` }} className="mainContent">
            <ListGroup variant="flush">
                {generateWall()}
            </ListGroup>
        </Card>
        
    )
}

export default List;