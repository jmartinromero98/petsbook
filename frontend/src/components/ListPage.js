import List from './List.js';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import SearchCard from './SearchCard';


function ListPage(props) {

    return (

        <div className="container-fluid">
            <Row>
                <Col>
                </Col>
                <Col xs={6}> 
                <SearchCard placeholder={props.searchPlaceholder} type={props.type} add={true}/>
                <List/>
                </Col>
                <Col>
                </Col>
            </Row>
        </div>

    )
    
}

export default ListPage;