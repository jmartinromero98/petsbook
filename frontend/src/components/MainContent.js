import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import ProfileCol from './ProfileCol';
import ChatCol from './ChatCol';
import { useEffect, useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import Modal from 'react-bootstrap/Modal';
import ListGroup from 'react-bootstrap/ListGroup';
import Form from 'react-bootstrap/Form';

import Context from '../util/AppContext';

import requestHelper from './RequestHelper';
import { PlusSquareDotted } from 'react-bootstrap-icons';

function MainContent(props) {

    const history = useHistory();
    const [posts, setPosts] = useState([]);
    const [pets, setPets] = useState([]);
    const [usuario, setUsuario] = useState();
    const [amigos, setAmigos] = useState([]);
    const [page, setPage] = useState(1);

    const [postSelected, setPostSelected] = useState(-1);
    const [comments, setComments] = useState([]);
    const [showCom, setShowCom] = useState(false);
    const [input, setInput] = useState("");

    const context = useContext(Context);

    function showPostCom(id) {

        setPostSelected(id)
        setShowCom(true);

        requestHelper.post('/pets/getComments', { "id_post": id }, true).then((res) => {
    
            // show comments in comment modal
            setComments(res.data);
    
        })
    
    
    }

    function handleCloseCom() {

        setShowCom(false);

    }

    function sendMessage() {

        requestHelper.post('/pets/sendComment', {

            "id_post": postSelected,
            "comentario": input

        }, true).then((res) => {

            // show comments in comment modal
            setInput("");
            
            requestHelper.post('/pets/getComments', { "id_post": postSelected }, true).then((res) => {

                // show comments in comment modal
                setComments(res.data);
    
            })

        });

    }

    function getPrevious() {

        if (page > 0 || posts.length >= 5) {

            setPage(page - 1);

        }

    }

    function getNext() {

        if (posts.length >= 5) {

            setPage(page + 1);

        }

    }

    useEffect(() => {


        requestHelper.post('/session/getPosts', {

            "page": page,

        }, true).then((res) => {

            if ((res.data.length > 0 && !sameResponse(res.data)) || page == 1) {

                setPosts(res.data);

            } else {

                setPage(page - 1);

            }

        });



    }, [page]);

    function sameResponse(res) {

        if (res.length != posts.length) {
            return false;
        }

        for (var i = 0; i < posts.length; i++) {

            if (posts[i].id_post != posts[i].id_post) {

                return false;

            }

        }

        return true;

    }

    useEffect(() => {

        if (!context.connected) {
            history.push("/");
        }

        requestHelper.post('/session/getPosts', {

            "page": page,

        }, true).then((res) => {

            if (res.status == true) {

                setPosts(res.data);

            }

        })

        requestHelper.post('/pets/getPets', {}, true).then((res) => {

            if (res.status == true) {

                setPets(res.data);

            }

        })

        requestHelper.post('/session/data', {}, true).then((res) => {

            if (res.status == true) {

                setUsuario(res.usuario);

            }

        })

        requestHelper.post('/friends/getFriends', {}, true).then((res) => {

            if (res.status == true) {

                setAmigos(res);

            }

        })

    }, []);

    return (

        <div className="container-fluid">
            <Row>
                <Col id="pb-profilecol-maincol">
                    <div className="sticky-top pb-profilecol-container pb-maincontent-col" style={{ zIndex: "0" }}>
                        {usuario != undefined && pets != undefined &&
                            <ProfileCol usuario={usuario} mascotas={pets} />
                        }
                    </div>
                </Col>
                <Col id="pb-centralcol-maincol" xs={6}>
                    {posts.length == 0 &&
                        <>
                            <Card className="pb-post-maincard">
                                <Card.Header>
                                    <div className="row">
                                        <div className="col-2">
                                            <img className="pb-post-profileimg" src="img/logo.svg" alt="" />
                                        </div>
                                        <div className="col-6 pb-post-profilename">
                                            PetsBook
                                        </div>
                                        <div className="col-4 pb-post-datecol">
                                            {new Date().toLocaleDateString()}
                                        </div>
                                    </div>
                                </Card.Header>
                                <Card.Body>
                                    <Card.Title>¡Esto es una publicación!</Card.Title>
                                    <Card.Text>
                                        ¡Y de esta manera lucirán las publicaciones de las mascotas de tus amigos! ¡Date prisa y haz amigos, ¿a qué estás esperando?!
                                    </Card.Text>
                                    <img className="pb-post-image" src="img/logo.svg" />
                                </Card.Body>
                            </Card>
                        </>

                    }
                    {posts.length > 0 && posts.map((post, idx) => {
                        return (
                            <Card className="pb-post-maincard" key={idx}>
                                <Card.Header onClick={(e) => { history.push("pet?id=" + post.id_mascota) }}>
                                    <div className="row">
                                        <div className="col-2">
                                            <img className="pb-post-profileimg" src={post && post.Foto_perfil} alt="" />
                                        </div>
                                        <div className="col-6 pb-post-profilename">
                                            {post && post.nombre}
                                        </div>
                                        <div className="col-4 pb-post-datecol">
                                            {new Date(post.createdAt).toLocaleDateString()}
                                        </div>
                                    </div>
                                </Card.Header>
                                <Card.Body>
                                    <Card.Title>{post.titulo}</Card.Title>
                                    <Card.Text>
                                        {post.Descripcion}
                                    </Card.Text>
                                    <img className="pb-post-image" src={post.imagen} alt="" />
                                </Card.Body>
                                <Card.Footer>
                                    <Button className="pb-post-commentbtn" variant="light" data-npost={post.id_post} onClick={() => {showPostCom(post.id_post)}}>Comentarios</Button>
                                </Card.Footer>
                            </Card>
                        )
                    })

                    }
                    <Row className="d-flex justify-content-center" style={{ margin: "20px" }}>
                        <Col style={{ maxWidth: "450px" }}>
                            <Row className="d-flex justify-content-between" >
                                <button type="button" className="btn btn-outline-secondary" onClick={getPrevious} disabled={page == 1}> &lt; Anterior</button>
                                <button type="button" className="btn btn-outline-secondary" disabled>{page}</button>
                                <button type="button" className="btn btn-outline-secondary" onClick={getNext} disabled={posts.length < 5}> Siguiente &gt; </button>
                            </Row>
                        </Col>
                    </Row>
                </Col>
                <Col id="pb-chatcol-maincol">
                    <div className="sticky-top pb-chatcol-container" style={{ zIndex: "0" }}>
                        <ChatCol />
                    </div>
                </Col>
                <Modal show={showCom} onHide={handleCloseCom}>
                        <Modal.Header closeButton>
                            <Modal.Title>Comentarios</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <ListGroup variant="flush">
                                {comments && comments.length > 0 && comments.map((elem) => {
                                    return (
                                        <>
                                            <ListGroup.Item className="pb-comment-row" onClick={(e) => { history.push("user?id=" + elem.usuario) }}>
                                                <Row>
                                                    <Col xs={2}>
                                                        <img className="pb-pets-petlistimg" style={{ width: "100%" }} id="s1" srcset={elem.Usuario.Foto_perfil} type="image/png" />
                                                    </Col>
                                                    <Col>
                                                        <Row>
                                                            <Col>
                                                                {elem.Usuario.nombre}
                                                            </Col>
                                                            <Col className="pb-post-datecol">
                                                                {new Date(elem.createdAt).toLocaleDateString()}
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            {elem.comentario}
                                                        </Row>
                                                    </Col>
                                                </Row>
                                            </ListGroup.Item>
                                        </>
                                    )
                                })
                                }
                            </ListGroup>
                        </Modal.Body>
                        <Modal.Footer>
                            <Row className="pb-comment-inputrow">
                                <Col xs={8}>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Control type="text" value={input} placeholder="Escriba un comentario" onChange={(e) => { setInput(e.target.value) }} />
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Button className="pb-comment-sendbutton" variant="primary" onClick={sendMessage}>
                                        Enviar
                                    </Button>
                                </Col>
                            </Row>
                        </Modal.Footer>
                    </Modal>
            </Row>
        </div>

    )

}

export default MainContent;