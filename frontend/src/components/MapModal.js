import Modal from 'react-bootstrap/Modal';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import { useState, createRef } from 'react';
import Button from 'react-bootstrap/Button';
import SitesInput from './SitesInput';

import { MapContainer, TileLayer, Marker, Popup, useMapEvents, useMap } from 'react-leaflet';

import requestHelper from './RequestHelper';


function MapModal(props) {

    //map's hoooks
    const [mapState, setMapState] = useState({ lat: 40.463667, lng: -3.74922, zoom: 5 });
    const [position, setPosition] = useState({ lat: 0, lng: 0 });
    const [isPositionValid, setPositionValid] = useState({ valid: true, msg: "" })


    // form hooks
    const [titulo, setTitulo] = useState("");
    const [isTituloValid, setTituloValid] = useState({ valid: true, msg: "" });
    const [descripcion, setDescripcion] = useState("");
    const [isDescripcionValid, setDescripcionValid] = useState({ valid: true, msg: "" });
    const [tipo, setTipo] = useState("0");
    const [isTipoValid, setTipoValid] = useState({ valid: true, msg: "" });
    const [isMunicipioValid, setIsMunicipioValid] = useState({ valid: true, msg: "" });

    // form sites refs
    var auto = createRef();
    var prov = createRef();
    var muni = createRef();

    function onSubmit(e) {
        e.preventDefault();

        requestHelper.post('/ubis/registerUbi', {

            "lat": position.lat,
            "lng": position.lng,
            "titulo": titulo,
            "descripcion": descripcion,
            "tipo": tipo,
            "municipio": muni.current.value

        }, true).then((res) => {

            // manage error responses
            if (res.status == "badTitle") {

                setTituloValid({ valid: false, msg: "El formato del título es incorrecto." });

            }

            if (res.status == "badDescription") {

                setDescripcionValid({ valid: false, msg: "El formato de la descripción es incorrecto." });

            }

            if (res.status == "badType") {

                setTipoValid({ valid: false, msg: "El tipo de lugar es incorrecto." });

            }

            if (res.status == "badLocation") {

                setPositionValid({ valid: false, msg: "La ubicación seleccionada es incorrecta" });


            }

            if (res.status == "badMuni") {

                setIsMunicipioValid({ valid: false, msg: "La región seleccionada es incorrecta" });

            }



            if (res.status == true) {

                resetValues();

                

            }

        });

    }

    function resetValues() {

        // reset to default values
        setMapState({ lat: 40.463667, lng: -3.74922, zoom: 5 });
        setPosition({ lat: 0, lng: 0 });
        setPositionValid({ valid: true, msg: "" });
        setTitulo("");
        setTituloValid({ valid: true, msg: "" });
        setDescripcion("");
        setDescripcionValid({ valid: true, msg: "" });
        setTipo("0");
        setTipoValid({ valid: true, msg: "" });
        setIsMunicipioValid({ valid: true, msg: "" });

        props.handleClose();

    }


    function CreateLocation() {

        const map = useMapEvents({
            click(e) {
                setPosition({ lat: e.latlng.lat, lng: e.latlng.lng });
                setPositionValid({valid: true, msg: ""});
            },
        })

        return position === null ? null : (
            <Marker position={[position.lat, position.lng]} ref={(ref) => { }}>
                <Popup >You are here</Popup>
            </Marker>
        )
    }

    return (
        <>
            <Modal show={props.show} onHide={resetValues}>
                <Modal.Header closeButton={resetValues}>
                    <Modal.Title>Registrar un lugar</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                        <div className="input-label">Selecciona la ubicación</div>
                        <span>Haz click sobre el mapa, concretamente donde esté localizado el lugar a registrar.</span>
                        <MapContainer center={[mapState.lat, mapState.lng]} zoom={mapState.zoom} scrollWheelZoom={true}>
                            <TileLayer
                                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                            />
                            <CreateLocation />
                        </MapContainer>
                        <span className="pb-fakeinvalid">
                                    {isPositionValid.msg}
                                </span>
                        <br />
                        <Form.Group as={Row} controlId="formPlaintextEmail">
                            <Form.Label column sm="2">
                                Latitud
                        </Form.Label>
                            <Col sm="10">
                                <Form.Control type="text" placeholder={position.lat} readOnly />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="formPlaintextEmail">
                            <Form.Label column sm="2">
                                Longitud
                        </Form.Label>
                            <Col sm="10">
                                <Form.Control type="text" placeholder={position.lng} readOnly />
                            </Col>
                        </Form.Group>

                    <div className="form-group ">
                        <label className="input-label">Nombre del lugar</label>
                        <input
                            value={titulo}
                            onChange={(e) => setTitulo(e.target.value)}
                            type={"text"}
                            className={isTituloValid.valid ? "form-control" : "form-control is-invalid"}
                            id="titleInput"
                            placeholder={"Introduzca el nombre del lugar"}
                        />
                        <div className="invalid-feedback">
                            {isTituloValid.msg}
                        </div>
                    </div>
                    <div className="form-group ">
                        <label className="input-label">Descripcion del lugar</label>
                        <input
                            value={descripcion}
                            onChange={(e) => setDescripcion(e.target.value)}
                            type={"text"}
                            className={isDescripcionValid.valid ? "form-control" : "form-control is-invalid"}
                            id="titleInput"
                            placeholder={"Introduzca una descripción del lugar"}
                        />
                        <div className="invalid-feedback">
                            {isDescripcionValid.msg}
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="input-label">Tipo de lugar</label>
                        <select id="selectMuni" className="custom-select" value={tipo} onChange={(e) => {setTipo(e.target.value)}}>
                        <option value="0"> Elija el tipo del lugar...</option>
                                <option value="1"> Parque </option>
                                <option value="2"> Tienda </option>
                                <option value="3"> Veterinario </option>
                            </select>
                        <div className="invalid-feedback">
                            {isTipoValid.msg}
                        </div>
                    </div>

                    <SitesInput isMunicipioValid={isMunicipioValid} auto={auto} prov={prov} muni={muni} setMunicipioValid={setIsMunicipioValid}/>
                    <div className="form-group ">
                        <div className="invalid-feedback">
                            {isMunicipioValid.msg}
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                            <Button variant="secondary" onClick={resetValues}>
                                Cancelar
                                </Button>
                            <Button className="pb-comment-sendbutton" variant="btn btn-success" onClick={onSubmit}>
                                Enviar
                            </Button>
                </Modal.Footer>
            </Modal>
        </>
    )

}

export default MapModal;