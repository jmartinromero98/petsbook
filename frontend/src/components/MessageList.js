import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import FormInput from './FormInput';
import ListGroup from 'react-bootstrap/ListGroup';
import MessageModal from './MessageModal';

import { useState, useEffect, useContext } from 'react';

import { useHistory } from 'react-router-dom';

import requestHelper from './RequestHelper';
import Context from '../util/AppContext';

function MessageList(props) {

    const [messages, setMessages] = useState([]);
    const [amigos, setAmigos] = useState([]);
    const [search, setSearch] = useState("");
    const [show, setShow] = useState(false);
    const [friendSelected, setFriendSelected] = useState();
    const [input, setInput] = useState("");
    const [friendPhoto, setFriendPhoto] = useState();
    const history = useHistory();

    const context = useContext(Context);

    useEffect(() => {

        requestHelper.post('/friends/getFriends', {}, true).then((res) => {

            setAmigos(res);
            context.setAmigos(res);

        });

    }, []);

    function handleMessages(e) {

        requestHelper.post('/friends/getMessages', {

            id: e,

        }, true).then((res) => {

            if (res.status == true) {

                setMessages(res.data.msg);
                setFriendPhoto(res.data.foto);

            }

        });

    };

    return (
        <>
            <Row style={{width: "100%"}}>
                <Col></Col>
                <Col xs={12} md={8} lg={6} style={{ paddingLeft: "30px", paddingRight: "30px" }}>
                    <Card className="pb-friends-resultcontainer">

                        <div className="pb-pets-searchcontainer">
                            <FormInput
                                value={search}
                                onChange={(e) => setSearch(e.target.value)}
                                valid={true}
                                msg={""}
                                type="text"
                                name="el nombre del amigo"
                                id="searchInput"
                                label="Nombre del amigo"
                            />
                        </div>
                        <div className="pb-pets-friendspetscontainer">
                            <div className="input-label" style={{ fontWeight: "bold" }}>
                                Iniciar o continuar una conversación
                            </div>
                            <ul className="list-group list-group-flush">
                                {amigos.length > 0 && props.usuario && amigos.map((elem, idx) => {
                                    if (elem.estado == "Aceptada") {
                                        if (elem.receptor == props.usuario.id_usuario) {
                                            elem = elem.emisorTable
                                        } else {
                                            elem = elem.receptorTable
                                        }

                                        return (
                                            <li className="list-group-item" style={{ display: elem.nombre.toLowerCase().indexOf(search.toLowerCase()) !== -1 ? "" : "none" }} key={idx}>
                                            <div className="row pb-friends-friendlistcont container" 
                                                onClick={() => {
                                                    setShow(true);
                                                    setFriendSelected(elem.id_usuario);
                                                    handleMessages(elem.id_usuario);
                                                }}>

                                                <div className="col-4 pb-pets-petlistimgcol">
                                                    <img className="pb-pets-petlistimg" src={elem && elem.Foto_perfil} alt="" >
                                                    </img>
                                                </div>
                                                <div className="col-8 pb-pets-petlistcol">
                                                    <div className="row" style={{ display: "inline" }}>
                                                        <div className="row pb-pets-petlistname">
                                                            {elem.nombre}
                                                        </div>
                                                        <div className="row">
                                                            {"En " + elem.Municipio.nombre + ", " + elem.Municipio.Provincia.Autonomia.nombre}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </li>
                                            )
                                    }
                                })
                                }
                            </ul>
                        </div>
                        <MessageModal show={show} onHide={() => { setShow(false); setMessages([]); setInput("") }} input={input} setInput={setInput} messages={messages} handleMessages={(msgs) => { setMessages(msgs) }} friendSelected={friendSelected} friendPhoto={friendPhoto}/>
                    </Card>
                </Col>
                <Col></Col>
            </Row>
        </>
    )

}

export default MessageList;