import Modal from 'react-bootstrap/Modal';
import ListGroup from 'react-bootstrap/ListGroup';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import { useHistory } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import Context from '../util/AppContext';
import { useContext} from 'react';

import requestHelper from './RequestHelper';


function MessageModal(props) {

    const history = useHistory();

    const context = useContext(Context);

    function sendMessage() {

        requestHelper.post('/friends/sendMessage', {

            receptor: props.friendSelected,
            msg: props.input,

        }, true).then((res) => {

            if (res.status == true) {

                props.handleMessages(res.data);

            }

        });

    }

    return(
        <>
        <Modal show={props.show} onHide={props.onHide}>
                        <Modal.Header closeButton>
                            <Modal.Title>Mensajes</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                        <ListGroup variant="flush">
                            {props.messages && props.messages.length > 0 && props.messages.map((elem) => {
                                                                
                                if (elem.receptor == props.friendSelected) {

                                    //el emisor es el propio usuario: Diseño contrario
                                    elem.id_usuario = elem.emisor;
                                    return (
                                        <>
                                        <ListGroup.Item className="pb-comment-row" onClick={(e) => {history.push("user?id="+elem.emisor)}}>
                                            <Row>
                                                <Col>
                                                    <Row> 
                                                        <Col className="pb-post-datecol2">
                                                        {new Date(elem.createdAt).toLocaleDateString()}
                                                        </Col>
                                                        <Col style={{textAlign: "right"}}>
                                                        {elem.emTable.nombre}
                                                        </Col>
                                                    </Row>
                                                    <Row >
                                                        <Col style={{textAlign: "right"}}>
                                                        {elem.Texto}
                                                        </Col>
                                                    </Row>
                                                </Col>
                                                <Col xs={2}>
                                                    <img className="pb-pets-petlistimg" src={context.usuario && context.usuario.Foto_perfil} alt="" />
                                                </Col>
                                            </Row>
                                        </ListGroup.Item>
                                        </>
                                    )

                                } else {

                                    //el emisor es el amigo, diseño normal.
                                    elem.id_usuario = elem.receptor;
                                    return (
                                        <>
                                        <ListGroup.Item className="pb-comment-row" onClick={(e) => {history.push("user?id="+elem.emisor)}}>
                                            <Row>
                                                <Col xs={2}>
                                                <img className="pb-pets-petlistimg" src={props.friendPhoto} alt="" />
                                                </Col>
                                                <Col>
                                                    <Row> 
                                                        <Col style={{paddingLeft: "0px"}}>
                                                        {elem.emTable.nombre}
                                                        </Col>
                                                        <Col className="pb-post-datecol">
                                                        {new Date(elem.createdAt).toLocaleDateString()}
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        {elem.Texto}
                                                    </Row>
                                                </Col>
                                            </Row>
                                        </ListGroup.Item>
                                        </>
                                    )
                                }
                                
                            })
                            }
                        </ListGroup>
                        </Modal.Body>
                        <Modal.Footer>
                            <Row  className="pb-comment-inputrow">
                                <Col xs={8}>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Control type="text" value={props.input} placeholder="Escriba un comentario" onChange={(e) => {props.setInput(e.target.value)}}/>
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Button className="pb-comment-sendbutton" variant="primary" onClick={sendMessage}>
                                        Enviar
                                    </Button>
                                </Col>
                            </Row>
                        </Modal.Footer>
                    </Modal>
        </>
    )

}

export default MessageModal;