import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import FormInput from './FormInput';
import Cropper from './Cropper';
import Form from 'react-bootstrap/Form';
import BreedInput from './BreedInput';
import React, { useState } from 'react';
import { Search, PlusSquare } from 'react-bootstrap-icons';

function ForumModal(props) {

    // hooks for modal
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    
    const [isLoading, setLoading] = useState(true);

    return (
        <>
            <Button onClick={handleShow} className="pb-pets-upperbutton" data-toggle="modal" variant="success">{props.name}</Button>

            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >

                <Modal.Header closeButton>
                    <Modal.Title>
                        
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Cropper />
                    <FormInput
                        valid={true}
                        label="Nombre de la mascota"
                        type="text"
                        id="#forumTitle"
                        name="el nombre de la mascota"
                        maxlength="100" />

                    <FormInput
                        valid={true}
                        label="Año de nacimiento"
                        type="number"
                        id="#forumTitle"
                        name="el año de nacimiento de la mascota"
                        maxlength="100" />

                    <BreedInput />

                    <Form.Label style={{ fontWeight: "bold" }}>Género</Form.Label>

                    <Form.Control className="my-1 mr-sm-2" id="selectGenre" as="select" custom>
                        <option value="0"> Elija un género del animal</option>
                        <option value="0"> Macho </option>
                        <option value="0"> Hembra </option>
                        <option value="0"> Otro </option>
                    </Form.Control>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cancelar
                    </Button>
                    <Button variant="success" id="actionButton" type="submit" className=" btn btn-block mybtn btn-primary tx-tfm" disabled={isLoading} >
                           {isLoading && <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>}
                           {isLoading ? "Cargando..." : "Registrar"}</Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}

export default ForumModal;