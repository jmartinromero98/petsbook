import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import ListGroup from 'react-bootstrap/ListGroup';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Cropper from './Cropper';
import Form from 'react-bootstrap/Form';
import BreedInput from './BreedInput';

import FormInput from './FormInput';
import SitesInput from './SitesInput';

import { useState, useEffect, createRef } from 'react';

import { useHistory } from 'react-router-dom';

function ModifyPet() {

    //hooks of fields
    const [name, setName] = useState("");
    const [year, setYear] = useState("");
    const [genre, setGenre] = useState(0);
    const [breed, setBreed] = useState(0);
    const [cropData, setCropData] = useState("#");


    const [isImageValid, setImageValid] = useState({ valid: true, msg: "" });
    const [isNameValid, setNameValid] = useState({ valid: true, msg: "" });
    const [isGenreValid, setGenreValid] = useState({ valid: true, msg: "" });
    const [isBreedValid, setBreedValid] = useState({ valid: true, msg: "" });
    const [isYearValid, setYearValid] = useState({ valid: true, msg: "" });
    const [isCropValid, setCropValid] = useState({ valid: true, msg: "" });

    // hooks for modal
    const [show, setShow] = useState(false);
    const handleClose = () => {

        setShow(false);
        setNameValid({ valid: true, msg: "" });
        setGenreValid({ valid: true, msg: "" });
        setBreedValid({ valid: true, msg: "" });
        setYearValid({ valid: true, msg: "" });
        setCropValid({ valid: true, msg: "" });
        setImageValid({ valid: true, msg: "" });

    };
    const handleShow = () => setShow(true);
    const handleCrop = (i) => setCropData(i);
    const handleCropValid = (i) => setCropValid(i);

    const [isLoading, setLoading] = useState();

    useEffect(() => {

        var url_string = window.location.href;
        var url = new URL(url_string);
        var id = url.searchParams.get("id");


    }, []);

    return (
        <>
            <Row>
                <Col sm={4}>
                    <ListGroup className="pb-session-modifymenu">
                        <ListGroup.Item onClick={() => { setPage("cuenta") }}>
                            Cuenta
                            </ListGroup.Item >
                        <ListGroup.Item style={{ background: "gray" }}>
                            Mascotas
                            </ListGroup.Item>
                        <ListGroup.Item  onClick={() => { setPage("contraseña") }}>
                            Contraseña
                            </ListGroup.Item>
                    </ListGroup>
                </Col>
                <Col sm={8}>
                    <Cropper
                        titulo={"Foto de perfil"}
                        cropData={cropData}
                        setImage={handleCrop}
                        valid={isCropValid}
                        setValid={handleCropValid}

                    />
                    {isImageValid.msg != "" && <span className="pb-fakeinvalid">Debes elegir una imagen en un formato válido</span>}
                    <FormInput
                        valid={isNameValid.valid}
                        onChange={(e) => { setName(e.target.value); setNameValid({ valid: true, msg: "" }) }}
                        label="Nombre de la mascota"
                        type="text"
                        msg={isNameValid.msg}
                        id="#forumTitle"
                        name="el nombre de la mascota"
                        maxlength="100" />

                    <FormInput
                        valid={isYearValid.valid}
                        msg={isYearValid.msg}
                        onChange={(e) => { setYear(e.target.value); setYearValid({ valid: true, msg: "" }) }}
                        label="Año de nacimiento"
                        type="number"
                        id="#forumTitle"
                        name="el año de nacimiento de la mascota"
                        maxlength="100" />

                    <BreedInput isBreedValid={isBreedValid} setBreed={setBreed}
                    />

                    <Form.Label style={{ fontWeight: "bold" }}>Género</Form.Label>

                    <Form.Control onChange={e => {setGenre(e.target.value);}} className={isGenreValid.valid ? "my-1 mr-sm-2" : "my-1 mr-sm-2 is-invalid"} id="selectGenre" as="select" custom>
                        <option value="0"> Elija un género del animal</option>
                        <option value="1"> Macho </option>
                        <option value="2"> Hembra </option>
                        <option value="3"> Otro </option>
                    </Form.Control>
                    <div className="invalid-feedback">
                        {isGenreValid.msg}
                    </div>
                </Col>
            </Row>
        </>
    )
}

export default ModifyPet;