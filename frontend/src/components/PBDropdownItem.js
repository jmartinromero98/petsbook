import Dropdown from 'react-bootstrap/Dropdown';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import { useHistory } from 'react-router-dom';

function PBDropdownItem(props, key) {

    const history = useHistory();

    return (
        <>
            <Dropdown.Item className="pb-dropdown-item" style={ {display: "flex"}} onClick={() => {history.push(props.link)}} key={key}>
                <div className="pb-dropdowitem-itemcontainer col">
                    <div className="column pb-dropdown-itemcolumn pb-dropdown-item-photocolumn">
                        <img className="pb-dropdownitem-photo" src={props.imgPath} />
                    </div>
                    <div className="column pb-dropdown-itemcolumn pb-dropdownitem-namecolumn">
                        <span className="pb-petname">{props.title}</span>
                        <span className="pb-dropdownitem-span">{props.subtitle}</span>
                    </div>
                </div>
        </Dropdown.Item> 
        </>
    )
}

export default PBDropdownItem;