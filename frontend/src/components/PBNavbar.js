import Navbar from 'react-bootstrap/Navbar';
import Row from 'react-bootstrap/Row';
import Dropdown from 'react-bootstrap/Dropdown';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import PBDDI from './PBDropdownItem';
import { Link, useHistory } from 'react-router-dom';
import { useContext } from 'react';
import Context from '../util/AppContext';

// main sections for each user.
var sections = [
    { "name": "Mascotas", "href": "/pets", "dropdown": "true" },
    { "name": "Amigos", "href": "/friends", "dropdown": "true" },
    { "name": "Lugares", "href": "/sites", "dropdown": "false" },
    { "name": "Mensajes", "href": "/messages", "dropdown": "false" }
];

function PBNavbar(props) {

    const history = useHistory();

    // generate notification span inside dropdown navbar.

    const context = useContext(Context);

    function generateDropdownItem(sectionName, idx) {

        if (sectionName.name == "Mascotas") {
            return (
                <li className="nav-item" key={idx}>
                    <Dropdown as={ButtonGroup}>
                        <Button variant="secondary" className="pb-dropdown-leftbutton" data-href={sectionName.href} onClick={(e) => { history.push(e.target.dataset.href) }}>{sectionName.name}
                        </Button>

                        {context.mascotas.length > 0 && <Dropdown.Toggle split variant="secondary" id="dropdown-split-basic" className="pb-dropdown-rightbutton" />}

                        {context.mascotas.length > 0 && <Dropdown.Menu className="pb-dropdown-menu">
                            {context.mascotas.length > 0 && context.mascotas.map((mascota, idx) => {
                                return (
                                    <PBDDI title={mascota.nombre} link={"/pet?id=" + mascota.id_mascota} subtitle={mascota.Raza.nombre} imgPath={mascota.Foto_perfil} key={idx} />
                                )
                            })}
                        </Dropdown.Menu>
                        }
                    </Dropdown>
                </li >
            )
        }

        if (sectionName.name == "Amigos") {


            var tengoAmigos = false;
            var counter = 0;
            while (counter < context.amigos.length && !tengoAmigos) {

                if (context.amigos[counter].estado == "Aceptada") {

                    tengoAmigos = true;

                }

                counter++;
            }

            return (
                <li className="nav-item" key={idx}>
                    <Dropdown as={ButtonGroup}>
                        <Button variant="secondary" className="pb-dropdown-leftbutton" data-href={sectionName.href} onClick={(e) => { history.push(e.target.dataset.href) }}>{sectionName.name}
                        </Button>

                        {tengoAmigos && <Dropdown.Toggle split variant="secondary" id="dropdown-split-basic" className="pb-dropdown-rightbutton" />}

                        {context.amigos.length > 0 && <Dropdown.Menu className="pb-dropdown-menu">
                            {context.amigos != undefined && context.amigos != null && context.amigos.length > 0 && context.amigos.map((amigo, idx) => {

                                if (amigo.estado == "Aceptada") {

                                    if (Object.entries(amigo.emisorTable).length === 0) {

                                        return (

                                            <PBDDI imgPath={amigo.receptorTable.Foto_perfil} title={amigo.receptorTable.nombre} link={"user?id=" + amigo.receptorTable.id_usuario} subtitle={amigo.receptorTable.Municipio.nombre} key={idx} />
                                        )

                                    } else {

                                        return (
                                            <PBDDI imgPath={amigo.emisorTable.Foto_perfil} title={amigo.emisorTable.nombre} link={"user?id=" + amigo.emisorTable.id_usuario} subtitle={amigo.emisorTable.Municipio.nombre} key={idx} />
                                        )

                                    }

                                }


                            })}

                        </Dropdown.Menu>
                        }
                    </Dropdown>
                </li >
            )
        }

        // default case
        return (
            <li className="nav-item" key={idx}>
                <Dropdown as={ButtonGroup} >
                    <Button variant="secondary" className="pb-dropdown-leftbutton" data-href={sectionName.href} onClick={(e) => { history.push(e.target.dataset.href) }}>
                        {sectionName.name}
                    </Button>

                </Dropdown>
            </li >
        )

    }

    function logOut() {

        document.cookie = "authorization=";
        window.location.href = "/";

    }

    return (
        <Navbar className="navbar-dark bg-dark shadow-lg sticky-top" expand="lg" bg="dark" id="pb-navbar" style={{ zIndex: "20" }}>
            <Row className="pb-navbar-row">
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <a className="navbar-brand" data-link="/main" onClick={(e) => { history.push(e.currentTarget.dataset.link) }}>
                    <div className="p-1 pb-leftnavbar-container align-items-center" id="pb-logo-2">
                        <img id="pb-navbar-logo" src="img/logo.svg" />

                        <span className="Camila">PetsBook</span>
                    </div>
                </a>
                <Navbar.Collapse id="basic-navbar-nav">
                    <div className="d-flex justify-content-between" id="pb-navbar-flex">
                        <a className="navbar-brand" data-link="/main" onClick={(e) => { history.push(e.currentTarget.dataset.link) }}>
                            <div className="p-1 pb-leftnavbar-container align-items-center" id="pb-logo-1">
                                <img id="pb-navbar-logo" src="img/logo.svg"></img>

                                <span className="Camila">PetsBook</span>

                            </div>
                        </a>
                        <div className="p-1 align-self-center" id={!context.connected ? "navbarNavSupCont" : "false"}>
                            <ul className="navbar-nav" id="navbarNavSup">
                                {context.connected && sections.map((section, idx) => generateDropdownItem(section, idx))}
                            </ul>
                        </div>
                        <div className="p-1 align-items-center" id="pb-navbar-sessioncontainer">
                            {context.connected && context.usuario ?
                                <>
                                    <Dropdown as={ButtonGroup}>
                                        <Button variant="secondary" className="pb-dropdown-leftbutton" data-href={"/user?id=" + context.usuario.id_usuario} onClick={(e) => { history.push(e.target.dataset.href) }}>
                                            Sesión
                                        </Button>

                                        <Dropdown.Toggle split align="right" variant="secondary" id="dropdown-split-basic" className="pb-dropdown-rightbutton" />

                                        <Dropdown.Menu align="right" style={{ zIndex: "999", top: "50px" }}>
                                            <Dropdown.Item className="pb-dropdown-item" style={{ display: "flex" }} onClick={() => { history.push("/modify") }} key="modify">
                                                <div className="pb-dropdowitem-itemcontainer col">
                                                    <div className="column pb-dropdown-itemcolumn pb-dropdownitem-namecolumn">
                                                        <span className="pb-petname">Modificar datos</span>
                                                    </div>
                                                </div>
                                            </Dropdown.Item>
                                            <Dropdown.Item className="pb-dropdown-item" style={{ display: "flex" }} onClick={() => { history.push("/delete") }} key="delete">
                                                <div className="pb-dropdowitem-itemcontainer col">
                                                    <div className="column pb-dropdown-itemcolumn pb-dropdownitem-namecolumn">
                                                        <span className="pb-petname">Eliminar datos</span>
                                                    </div>
                                                </div>
                                            </Dropdown.Item>
                                            <Dropdown.Item className="pb-dropdown-item-logout" style={{ display: "flex" }} onClick={() => { logOut() }} key="logout">
                                                <div className="pb-dropdowitem-itemcontainer col">
                                                    <div className="column pb-dropdown-itemcolumn pb-dropdownitem-namecolumn">
                                                        <span className="pb-logout">Salir</span>
                                                    </div>
                                                </div>
                                            </Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>

                                    <ul className="navbar-nav" id="navbarSession">
                                        <li className="nav-item" key="acceder">
                                            <Link className="nav-link" to="/modify">Modificacion<span className="sr-only">(current)</span></Link>
                                        </li>
                                        <li className="nav-item" key="registrarse">
                                            <Link className="nav-link" to="/delete" key={"registrarse"}>Eliminación</Link>
                                        </li>
                                        <li className="nav-item" key="registrarse">
                                            <Link className="nav-link" onClick={logOut} key={"registrarse"}>Salir</Link>
                                        </li>
                                    </ul>
                                </>
                                :
                                <ul className="navbar-nav" id="navbarUserUl">

                                    <li className="nav-item active" key="acceder">
                                        <Link className="nav-link" to="/login">Acceder<span className="sr-only">(current)</span></Link>
                                    </li>
                                    <li className="nav-item" key="registrarse">
                                        <Link className="nav-link" to="/register" key={"registrarse"}>Registrarse</Link>
                                    </li>
                                </ul>
                            }
                        </div>
                    </div>
                </Navbar.Collapse>
            </Row>
        </Navbar>
    );
}

export default PBNavbar;