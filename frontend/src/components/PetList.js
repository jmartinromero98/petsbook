import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
import '../styles/pets.scss';
import { useState, createRef, useEffect } from 'react';
import SitesInput from './SitesInput';
import Button from 'react-bootstrap/Button';
import FormInput from './FormInput';
import BreedInput from './BreedInput';
import { Form } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

import requestHelper from './RequestHelper';
import UserList from './UserList';

function PetList() {

    const [breed, setBreed] = useState(null);

    const [isMunicipioValid, setMunicipioValid] = useState({ valid: true, msg: "" });
    const [isBreedValid, setBreedValid] = useState({ valid: true, msg: "" });
    const [genre, setGenre] = useState("0");

    const [pets, setPets] = useState([]);
    const [name, setName] = useState();

    const [page, setPage] = useState(1);

    const history = useHistory();

    // form sites refs
    var auto = createRef();
    var prov = createRef();
    var muni = createRef();

    function onSubmit(e) {

        e.preventDefault();

        if (page == 1) {

            getPets();

        } else {

            setPage(1);

        }

    }

    useEffect(() => {
        

        requestHelper.post('/pets/searchPets', {

            "nombre": document.getElementById("PetName").value,
            "animal": document.getElementById("selectTipo").value,
            "raza": document.getElementById("selectBreed").value,
            "autonomia": document.getElementById("selectCA").value,
            "provincia": document.getElementById("selectProv").value,
            "municipio": document.getElementById("selectMuni").value,
            "genero": genre,
            "page": page


        }, true).then((res) => {


            if ((res.length > 0 && !sameResponse(res)) || page == 1 ) {

                setPets(res);

            } else {

                setPage(page - 1);

            }

        });

        

    }, [page]);

    function sameResponse(res) {

        if (res.length != pets.length) {
            return false;
        }

        for (var i = 0; i < pets.length; i++) {


            if (res[i].id_mascota != pets[i].id_mascota) {

                return false;

            }

        }

        return true;

    }

    function getPets() {

        requestHelper.post('/pets/searchPets', {

            "nombre": document.getElementById("PetName").value,
            "animal": document.getElementById("selectTipo").value,
            "raza": document.getElementById("selectBreed").value,
            "autonomia": document.getElementById("selectCA").value,
            "provincia": document.getElementById("selectProv").value,
            "municipio": document.getElementById("selectMuni").value,
            "genero": genre,
            "page": page


        }, true).then((res) => {

            setPets(res);

        });

    }

    function getPrevious() {
        
        if (page > 1 || pets.length >= 5) {

            setPage(page-1);

        }

    }

    function getNext() {

        if (pets.length >= 5) {

            setPage(page+1);

        }

    }

    return (
        <>
            <Row style={{width: "100%"}}>
                <Col sm={4} style={{ paddingLeft: "30px", paddingRight: "30px" }}>
                    <Form onSubmit={onSubmit} name="searchPets" noValidate="noValidate">
                        <Card className="pb-petlist-searchcard" >
                            <FormInput
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                                valid={true}
                                msg={true}
                                type="text"
                                name="el nombre de la mascota"
                                id="PetName"
                                label="Nombre" />

                            <BreedInput isBreedValid={isBreedValid} setBreed={setBreed} setBreedValid={setBreedValid} />
                            <SitesInput auto={auto} prov={prov} muni={muni} isMunicipioValid={isMunicipioValid} setMunicipioValid={setMunicipioValid} />
                            <Form.Label style={{ fontWeight: "bold" }}>Género</Form.Label>
                            <Form.Control onChange={e => setGenre(e.target.value)} className="my-1 mr-sm-2" id="selectGenre" as="select" custom>
                                <option value="0"> Cualquiera</option>
                                <option value="1"> Macho </option>
                                <option value="2"> Hembra </option>
                                <option value="3"> Otro </option>
                            </Form.Control>
                        </Card>

                        <Card>
                            <Button variant="success" type="submit">
                                Buscar una nueva mascota
                            </Button>
                        </Card>
                    </Form>
                </Col>
                <Col sm={8} style={{ paddingLeft: "30px", paddingRight: "30px" }}>
                    <div className="listContainer">
                        {pets.length == 0 && <h1 className="pb-userlist-notfound">Realiza una nueva búsqueda</h1>}
                        <ul className="list-group list-group-flush">
                            {pets.length > 0 && pets.map((elem, idx) => {
                                return (
                                    <>
                                        <li className="list-group-item" key={idx}>
                                            <div className="row pb-pets-petlistcont" onClick={(e) => { history.push("pet?id=" + elem.id_mascota) }}>
                                                <div className="col-2 pb-pets-petlistimgcol">
                                                    <img className="pb-pets-petlistimg" src={elem.Foto_perfil} alt="" >
                                                    </img>
                                                </div>
                                                <div className="col-10 pb-pets-petlistcol align-self-center">
                                                    <div className="row pb-pets-petlistname">
                                                        {elem.nombre}
                                                    </div>
                                                    <div className="row">
                                                        {elem.Raza.nombre}
                                                    </div>
                                                    <div className="row">
                                                        {elem.genero}
                                                    </div>
                                                </div>
                                                <div className="col pb-pets-petlistcol">

                                                </div>
                                            </div>
                                        </li>
                                        {idx == pets.length - 1 &&
                                            <Row className="d-flex justify-content-center" style={{ margin: "20px" }}>
                                                <Col style={{ maxWidth: "450px" }}>
                                                    <Row className="d-flex justify-content-between" >
                                                        <button type="button" className="btn btn-outline-secondary" onClick={getPrevious} disabled={page == 1}> &lt; Anterior</button>
                                                        <button type="button" className="btn btn-outline-secondary" disabled>{page}</button>
                                                        <button type="button" className="btn btn-outline-secondary" onClick={getNext} disabled={pets.length < 5}> Siguiente &gt; </button>
                                                    </Row>
                                                </Col>
                                            </Row>

                                        }
                                    </>
                                )
                            })
                            }
                        </ul>
                    </div>
                </Col>
            </Row>
        </>
    )

}

export default PetList;