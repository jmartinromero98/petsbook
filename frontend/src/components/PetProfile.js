import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
import { useState, useEffect } from 'react';
import { PlusSquare } from 'react-bootstrap-icons';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Cropper from './Cropper';
import FormInput from './FormInput';
import Form from 'react-bootstrap/Form';
import ListGroup from 'react-bootstrap/ListGroup';
import { useHistory } from 'react-router-dom';
import requestHelper from './RequestHelper';

import '../styles/profile.scss';

function PetProfile(props) {

    const history = useHistory();

    // global info hooks
    const [profile, setProfile] = useState(undefined);
    const [posts, setPosts] = useState([]);


    // hooks for new post modal
    const [show, setShow] = useState(false);
    const handleClose = () => {

        setShow(false);
        setCropData("#");
        setName("");

    };
    const handleShow = () => setShow(true);
    const handleCrop = (i) => setCropData(i);
    const [cropData, setCropData] = useState("#");
    const [isCropValid, setCropValid] = useState({ valid: true, msg: "" });
    const handleCropValid = (i) => setCropValid(i);

    // hooks for new post info
    const [name, setName] = useState("");
    const [isNameValid, setNameValid] = useState({ valid: true, msg: "" });
    const [isLoading, setLoading] = useState();
    const [desc, setDesc] = useState("");
    const [isDescValid, setDescValid] = useState({ valid: true, msg: "" });
    var url_string = window.location.href
    var url = new URL(url_string);
    var id = url.searchParams.get("id")

    // hooks for comment modal
    const [postSelected, setPostSelected] = useState();
    const [comments, setComments] = useState([]);

    var getPosts = (id) => {

        requestHelper.post('/pets/getPosts', {

            "id_mascota": id,
            "page": 1

        }, true).then((res) => {

            setPosts(res.data);

        });

    };

    var getComments = (e) => {

        requestHelper.post('/pets/getComments', { "id_post": e }, true).then((res) => {

            // show comments in comment modal
            setComments(res.data);

        })
    };

    const handlePostSelected = (e) => {

        setShowCom(true);
        setPostSelected(e.target.dataset.npost);

        getComments(e.target.dataset.npost);


    }
    const [input, setInput] = useState("");
    const [showCom, setShowCom] = useState(false);
    const handleCloseCom = () => {

        setShowCom(false);

    };

    const sendMessage = () => {

        requestHelper.post('/pets/sendComment', {

            "id_post": postSelected,
            "comentario": input

        }, true).then((res) => {

            // show comments in comment modal
            setInput("");
            getComments(postSelected);

        });

    }

    useEffect(() => {

        var url_string = window.location.href
        var url = new URL(url_string);
        var id = url.searchParams.get("id")

        requestHelper.post('/pets/getProfile', {

            "id_mascota": id,

        }, true).then((res) => {

            setProfile(res[0]);


        });

        requestHelper.post('/pets/getPosts', {

            "id_mascota": id,
            "page": 1

        }, true).then((res) => {

            if (res.status == true) {
                setPosts(res.data);
            }


        });

    }, [id]);

    function onSubmit(e) {
        e.preventDefault();

        requestHelper.post('/pets/uploadPost', {

            "title": name,
            "desc": desc,
            "photo": cropData,
            "id": id,

        }, true).then((res) => {


            // fetch last posts (page = 1)
            handleClose();
            getPosts(profile.id_mascota);

        });
    }

    return (
        <>
            <Row style={{width: "100%"}}>
                <Col>
                </Col>
                <Col xs={12} md={8} lg={6}>
                    <Card className="pb-petprofile-maincard">
                        <picture  >
                            <img className="pb-profilecol-img" src={profile && profile.Foto_perfil} alt="" />
                            <source id="s1" srcset={profile && profile.Foto_perfil} type="image/png" />
                        </picture>

                        <p align="center" className="pb-profilecol-name">{profile && profile.nombre}</p>

                        <div className="pb-petprofile-p">{profile && profile.Raza.nombre}, {profile && profile.Raza.Animale.nombre}</div>
                        <div className="pb-petprofile-p">Género: {profile && profile.genero}</div>
                        <div className="pb-petprofile-p">Nacimiento: {profile && profile.edad}</div>

                        {profile && profile.Usuario.id_usuario != props.usuario.id_usuario &&
                            <>
                                <p className="pb-pets-ownername">Dueño/a:</p>
                                <Card className="pb-pets-ownercard" onClick={(e) => { history.push("user?id=" + profile.Usuario.id_usuario) }}>
                                    <div className="row pb-friends-friendlistcont container" /*style={ { display: elem.nombre.toLowerCase().indexOf(search.toLowerCase()) !== -1 ? "" : "none"} } */>

                                        <div className="col pb-pets-petlistimgcol">
                                            <img className="pb-pets-petlistimg" src={profile && profile.Usuario.Foto_perfil} alt="" >
                                            </img>
                                        </div>
                                        <div className="col-6 pb-pets-petlistcol">
                                            <div className="row pb-pets-petlistname">
                                                {profile && profile.Usuario.nombre}
                                            </div>
                                            <div className="row">
                                                {profile && "En " + profile.Usuario.Municipio.nombre + ", " + profile.Usuario.Municipio.Provincia.Autonomia.nombre}
                                            </div>

                                        </div>
                                    </div>
                                </Card>
                            </>
                        }
                    </Card>
                    {profile && profile.Usuario.id_usuario == props.usuario.id_usuario &&
                        <>
                            <Card className="pb-pets-addPostCard" >

                                <Button className="pb-friends-upperbutton" variant="success" onClick={handleShow}>
                                    <div className="pb-iconinline-container" style={{ display: "inline-flex" }} >
                                        <PlusSquare height="0.9em" />
                                    </div> Añade un nuevo post
                                </Button>

                            </Card>

                            <Modal
                                show={show}
                                onHide={handleClose}
                                backdrop="static"
                                keyboard={false}
                            >
                                <form onSubmit={onSubmit} name="petRegister" noValidate="noValidate">
                                    <Modal.Header closeButton>
                                        <Modal.Title>
                                            Subir un post
                                </Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                        <FormInput
                                            valid={isNameValid.valid}
                                            onChange={(e) => { setName(e.target.value); setNameValid({ valid: true, msg: "" }) }}
                                            label="Título del post"
                                            type="text"
                                            msg={isNameValid.msg}
                                            id="#forumTitle"
                                            name="un título para el post"
                                            maxlength="100" />
                                        <FormInput
                                            valid={isDescValid.valid}
                                            onChange={(e) => { setDesc(e.target.value); setDescValid({ valid: true, msg: "" }) }}
                                            label="Descripción del post"
                                            type="text"
                                            msg={isDescValid.msg}
                                            id="#forumTitle"
                                            name="una descripción para el post"
                                            maxlength="100" />
                                        <Cropper
                                            titulo={"Imagen del post"}
                                            cropData={cropData}
                                            setImage={handleCrop}
                                            valid={isCropValid}
                                            setValid={handleCropValid}
                                            image={cropData}
                                        />
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button variant="secondary" onClick={handleClose}>
                                            Cancelar
                                </Button>
                                        <button id="actionButton" type="submit" className=" btn btn-success" disabled={isLoading} >
                                            {isLoading && <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>}
                                            {isLoading ? "Cargando..." : "Subir"}
                                        </button>
                                    </Modal.Footer>
                                </form>
                            </Modal>
                        </>
                    }
                    {posts.length > 0 && posts.map((post, idx) => {
                        return (
                            <>
                                <Card className="pb-post-maincard">
                                    <Card.Header onClick={(e) => { history.push("pet?id=" + post.id_mascota) }}>
                                        <div className="row">
                                            <div className="col-2">
                                                <img className="pb-post-profileimg" src={profile && profile.Foto_perfil} alt="" />
                                            </div>
                                            <div className="col-6 pb-post-profilename">
                                                {profile && profile.nombre}
                                            </div>
                                            <div className="col-4 pb-post-datecol">
                                                {new Date(post.createdAt).toLocaleDateString()}
                                            </div>
                                        </div>
                                    </Card.Header>
                                    <Card.Body>
                                        <Card.Title>{post.titulo}</Card.Title>
                                        <Card.Text>
                                            {post.Descripcion}
                                        </Card.Text>
                                        <img className="pb-post-image" src={post.imagen} alt="" />
                                    </Card.Body>
                                    <Card.Footer>
                                        <Button className="pb-post-commentbtn" variant="light" data-npost={post.id_post} onClick={handlePostSelected}>Comentarios</Button>
                                    </Card.Footer>
                                </Card>
                            </>
                        )
                    })

                    }

                    <Modal show={showCom} onHide={handleCloseCom}>
                        <Modal.Header closeButton>
                            <Modal.Title>Comentarios</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <ListGroup variant="flush">
                                {comments && comments.length > 0 && comments.map((elem) => {
                                    return (
                                        <>
                                            <ListGroup.Item className="pb-comment-row" onClick={(e) => { history.push("user?id=" + elem.usuario) }}>
                                                <Row>
                                                    <Col xs={2}>
                                                        <img className="pb-pets-petlistimg" style={{ width: "100%" }} id="s1" srcset={elem.Usuario.Foto_perfil} type="image/png" />
                                                    </Col>
                                                    <Col>
                                                        <Row>
                                                            <Col>
                                                                {elem.Usuario.nombre}
                                                            </Col>
                                                            <Col className="pb-post-datecol">
                                                                {new Date(elem.createdAt).toLocaleDateString()}
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            {elem.comentario}
                                                        </Row>
                                                    </Col>
                                                </Row>
                                            </ListGroup.Item>
                                        </>
                                    )
                                })
                                }
                            </ListGroup>
                        </Modal.Body>
                        <Modal.Footer>
                            <Row className="pb-comment-inputrow">
                                <Col xs={8}>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Control type="text" value={input} placeholder="Escriba un comentario" onChange={(e) => { setInput(e.target.value) }} />
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Button className="pb-comment-sendbutton" variant="primary" onClick={sendMessage}>
                                        Enviar
                                    </Button>
                                </Col>
                            </Row>
                        </Modal.Footer>
                    </Modal>
                </Col>
                <Col>
                </Col>
            </Row>
        </>
    )

}

export default PetProfile;