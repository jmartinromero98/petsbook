import { Search, PlusSquare } from 'react-bootstrap-icons';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import FormInput from './FormInput';
import ListGroup from 'react-bootstrap/ListGroup';
import { useState, useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import Modal from 'react-bootstrap/Modal';
import Cropper from './Cropper';

import '../styles/friends.scss';
import BreedInput from "./BreedInput";

import requestHelper from './RequestHelper';
import Context from '../util/AppContext';


function Pets(props) {

    //hooks of fields
    const [name, setName] = useState("");
    const [year, setYear] = useState("");
    const [genre, setGenre] = useState(0);
    const [breed, setBreed] = useState(0);
    const [cropData, setCropData] = useState("#");
    const [pets, setPets] = useState([]);
    const [petFriends, setPetFriends] = useState([]);

    const [isImageValid, setImageValid] = useState({ valid: true, msg: "" });
    const [isNameValid, setNameValid] = useState({ valid: true, msg: "" });
    const [isGenreValid, setGenreValid] = useState({ valid: true, msg: "" });
    const [isBreedValid, setBreedValid] = useState({ valid: true, msg: "" });
    const [isYearValid, setYearValid] = useState({ valid: true, msg: "" });
    const [isCropValid, setCropValid] = useState({ valid: true, msg: "" });

    // hooks for modal
    const [show, setShow] = useState(false);
    const handleClose = () => {

        setShow(false);
        setNameValid({ valid: true, msg: "" });
        setGenreValid({ valid: true, msg: "" });
        setBreedValid({ valid: true, msg: "" });
        setYearValid({ valid: true, msg: "" });
        setCropValid({ valid: true, msg: "" });
        setImageValid({ valid: true, msg: "" });

    };
    const handleShow = () => setShow(true);
    const handleCrop = (i) => setCropData(i);
    const handleCropValid = (i) => setCropValid(i);

    const [isLoading, setLoading] = useState();

    const [search, setSearch] = useState("");
    const history = useHistory();

    const context = useContext(Context);

    const getPetList = () => {

        requestHelper.post('/pets/getPetList', {}, true).then((res) => {

            if (res.status == true) {

                setPetFriends(res.data);

            }

        });

    }

    const getPets = () => {

        requestHelper.post('/pets/getPets', {}, true).then((res) => {

            if (res.status == true) {

                setPets(res.data);
                context.setMascotas(res.data);

            }

        });

    }

    useEffect(() => {

        getPetList();

        getPets();

    }, [])


    function onSubmit(e) {

        e.preventDefault();

        requestHelper.post('/pets/createPet', {

            "name": name,
            "year": year,
            "genre": genre,
            "breed": breed,
            "img": cropData

        }, true).then((res) => {

            // manage error responses
            if (res.status == "badName") {

                setNameValid({ valid: false, msg: "El formato del nombre es incorrecto." });

            }

            if (res.status == "badYear") {

                setYearValid({ valid: false, msg: "El año es incorrecto." });

            }

            if (res.status == "badGenre") {

                setGenreValid({ valid: false, msg: "La género es incorrecto." });

            }

            if (res.status == "badBreed") {

                setBreedValid({ valid: false, msg: "La raza o especie del animal es incorrecta" });

            }

            if (res.status == "badImage") {

                setImageValid({ valid: false, msg: "Debes seleccionar una imágen en un formato válido." });

            }

            setLoading(false);

            // if its ok then save token and redirect.
            if (res.status == true) {

                setShow(false);

                getPetList();

                getPets();

            }

        });
    }

    return (
        <Row style={{width: "100%"}}>
            <Col>
            </Col>
            <Col xs={12} md={8} lg={6} style={{ paddingLeft: "30px", paddingRight: "30px" }}>

                <Card className="pb-pets-upperCard" >

                    <Button className="pb-pets-upperbutton" variant="info" onClick={() => { history.push("/PetList") }}>
                        <div className="pb-iconinline-container" style={{ display: "inline-flex" }} >
                            <Search height="0.9em" />
                        </div> Busca nuevas mascotas
                    </Button>

                    <Button onClick={handleShow} className="pb-pets-upperbutton" data-toggle="modal" variant="success">
                        <div className="pb-iconinline-container" style={{ display: "inline-table" }} >
                            <PlusSquare height="0.9em" />
                        </div> Registra una mascota
                    </Button>

                    <Modal
                        show={show}
                        onHide={handleClose}
                        backdrop="static"
                        keyboard={false}
                    >
                        <form onSubmit={onSubmit} name="petRegister" noValidate="noValidate">
                            <Modal.Header closeButton>
                                <Modal.Title>
                                    Registrar mascota
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <Cropper
                                    titulo={"Foto de perfil"}
                                    cropData={cropData}
                                    setImage={handleCrop}
                                    valid={isCropValid}
                                    setValid={handleCropValid}

                                />
                                {isImageValid.msg != "" && <span className="pb-fakeinvalid">Debes elegir una imagen en un formato válido</span>}
                                <FormInput
                                    valid={isNameValid.valid}
                                    onChange={(e) => { setName(e.target.value); setNameValid({ valid: true, msg: "" }) }}
                                    label="Nombre de la mascota"
                                    type="text"
                                    msg={isNameValid.msg}
                                    id="#forumTitle"
                                    name="el nombre de la mascota"
                                    maxlength="100" />

                                <FormInput
                                    valid={isYearValid.valid}
                                    msg={isYearValid.msg}
                                    onChange={(e) => { setYear(e.target.value); setYearValid({ valid: true, msg: "" }) }}
                                    label="Año de nacimiento"
                                    type="number"
                                    id="#forumTitle"
                                    name="el año de nacimiento de la mascota"
                                    maxlength="100" />

                                <BreedInput isBreedValid={isBreedValid} setBreed={setBreed} setBreedValid={setBreedValid}
                                />

                                <Form.Label style={{ fontWeight: "bold" }}>Género</Form.Label>

                                <Form.Control onChange={e => setGenre(e.target.value)} className={isGenreValid.valid ? "my-1 mr-sm-2" : "my-1 mr-sm-2 is-invalid"} id="selectGenre" as="select" custom>
                                    <option value="0"> Elija un género del animal</option>
                                    <option value="1"> Macho </option>
                                    <option value="2"> Hembra </option>
                                    <option value="3"> Otro </option>
                                </Form.Control>
                                <div className="invalid-feedback">
                                    {isGenreValid.msg}
                                </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={handleClose}>
                                    Cancelar
                                </Button>
                                <button id="actionButton" type="submit" className=" btn btn-success" disabled={isLoading} >
                                    {isLoading && <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>}
                                    {isLoading ? "Cargando..." : "Registrar"}
                                </button>
                            </Modal.Footer>
                        </form>
                    </Modal>

                </Card>

                <Card className="pb-friends-resultcontainer">

                    <div className="pb-pets-searchcontainer">
                        <FormInput
                            value={search}
                            onChange={(e) => setSearch(e.target.value)}
                            valid={true}
                            msg={""}
                            type="text"
                            name="el nombre de la mascota"
                            id="searchInput"
                            label="Nombre de la mascota"
                        />
                    </div>

                    <div className="pb-pets-ownpetscontainer">
                        <ul className="list-group list-group-flush">
                            {pets.length > 0 && <div className="input-label" style={{ fontWeight: "bold" }}>
                                Mis mascotas

                            </div>
                            }
                            {pets && pets.map((elem, idx) => {
                                return (
                                    <li className="list-group-item" style={{ display: elem.nombre.toLowerCase().indexOf(search.toLowerCase()) !== -1 ? "" : "none" }} key="idx">
                                        <div className="row pb-pets-petlistcont"
                                            onClick={(e) => { history.push("pet?id=" + elem.id_mascota) }}>
                                            <div className="col-2 pb-pets-petlistimgcol">
                                                <img className="pb-pets-petlistimg" src={elem.Foto_perfil} alt="" >
                                                </img>
                                            </div>
                                            <div className="col-10 pb-pets-petlistcol align-self-center">
                                                <div className="row pb-pets-petlistname">
                                                    {elem.nombre}
                                                </div>
                                                <div className="row">
                                                    {elem.Raza.nombre}
                                                </div>
                                                <div className="row">
                                                    {elem.genero}
                                                </div>
                                            </div>
                                            <div className="col pb-pets-petlistcol">
                                            </div>
                                        </div>
                                    </li>
                                )
                            })
                            }
                        </ul>
                    </div>

                    <div className="pb-pets-friendspetscontainer">
                        {petFriends.length > 0 &&
                            <div className="input-label" style={{ fontWeight: "bold" }}>
                                Mascotas de mis amigos
                            </div>
                        }
                        <ul className="list-group list-group-flush">
                            {petFriends && petFriends.map((solicitud) => {
                                return (
                                    solicitud.amigo.mascotas.map((elem, idx) => {
                                        return (
                                            <li className="list-group-item" style={{ display: elem.nombre.toLowerCase().indexOf(search.toLowerCase()) !== -1 ? "" : "none" }} key={idx}>
                                                <div className="row pb-pets-petlistcont" 
                                                    onClick={(e) => { history.push("pet?id=" + elem.id_mascota) }}>
                                                    <div className="col-2 pb-pets-petlistimgcol">
                                                        <img className="pb-pets-petlistimg" src={elem.Foto_perfil} alt="" >
                                                        </img>
                                                    </div>
                                                    <div className="col-10 pb-pets-petlistcol align-self-center">
                                                        <div className="row pb-pets-petlistname">
                                                            {elem.nombre}
                                                        </div>
                                                        <div className="row">
                                                            {elem.Raza.nombre + ", " + elem.genero}
                                                        </div>
                                                        <div className="row">
                                                            {"De "}<span style={{ fontWeight: "bold", whiteSpace: "pre" }}> {solicitud.amigo.nombre}</span>
                                                        </div>
                                                    </div>
                                                    <div className="col pb-pets-petlistcol">
                                                    </div>
                                                </div>
                                            </li>
                                        )
                                    })
                                )
                            })}
                        </ul>
                    </div>

                </Card>
            </Col>
            <Col>
            </Col>
        </Row>
    )

}

export default Pets;