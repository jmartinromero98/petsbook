import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

import requestHelper from './RequestHelper';

import '../styles/profile.scss';

function Profile() {

    const [profile, setProfile] = useState();
    const history = useHistory();

    useEffect(() => {

        var url_string = window.location.href
        var url = new URL(url_string);
        var id = url.searchParams.get("id")

        requestHelper.post('/friends/getProfile', {

            "id_profile": id,

        }, true).then((res) => {

            if (res.status == true) {

                setProfile(res.data[0]);

            }

        });


    }, []);

    return (
        <>
            <Row style={{width: "100%"}}>
                <Col>
                </Col>
                <Col xs={12} md={8} lg={6}>
                    <Card className="pb-profilecol-user" style={{marginTop: "40px"}}>
                        <picture  >
                            <img className="pb-pets-petlistimg" src={profile && profile.Foto_perfil} alt="" >
                            </img>
                            <source id="s1" srcSet="../img/user_profile.png" type="image/png" />
                        </picture>

                        <p align="center" className="pb-profilecol-name">{profile != undefined && profile.nombre}</p>
                        <p align="center" className="pb-profilecol-p">{profile != undefined && profile.email != undefined && profile.email}</p>

                    </Card>
                    {profile != undefined && profile.mascotas != undefined && profile.mascotas.map((elem, idx) => {
                        return (
                            <Card className="pb-profilecol-pet" onClick={(e) => { history.push("pet?id=" + elem.id_mascota) }} key={idx}>
                                <div className="d-flex">
                                    <div className="col-4 pb-pets-petlistimgcol" style={{ padding: "5px" }}>
                                        <img className="pb-pets-petlistimg" src={elem.Foto_perfil} alt="" >
                                        </img>
                                    </div>
                                    <div className="col-8">
                                        <Row style={{ display: "inline" }}>
                                            <Row style={{ fontWeight: "bold" }}>
                                                {elem.nombre}
                                            </Row>
                                            <Row>
                                                {elem.Raza.nombre}
                                            </Row>
                                            <Row>
                                                {elem.genero}
                                            </Row>
                                        </Row>
                                    </div>
                                </div>
                            </Card>
                        )
                    })}
                </Col>
                <Col>
                </Col>
            </Row>
        </>
    )

}

export default Profile;