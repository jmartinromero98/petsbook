import { useHistory } from 'react-router-dom';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import '../styles/ProfileCol.scss';
import { useState } from 'react';


function ProfileCol(props) {

    const history = useHistory();

    var handleProfile = () => {

        history.push("/user?id=" + props.usuario.id_usuario);

    }

    var handlePetProfile = (id) => {


        //history.push("/pet?id="+id);
    }

    return (
        <>

            <Card className="pb-profilecol" onClick={handleProfile}>
                <img className="pb-pets-petlistimg" src={props.usuario && props.usuario.Foto_perfil} alt="" >
                </img>
                <p align="center" className="pb-profilecol-name">{props.usuario.nombre}</p>
                <p align="center" className="pb-profilecol-p">{props.usuario.email}</p>
                <p align="center" className="pb-profilecol-p">De {props.usuario.Municipio.nombre}, {props.usuario.Municipio.Provincia.Autonomia.nombre}</p>
            </Card>
            {true && props.mascotas.map((elem, idx) => {

                return (
                        <Card className="pb-profilecol-pet" onClick={(e) => { history.push("pet?id=" + elem.id_mascota) }} key={idx}>
                            <div className="d-flex">
                                <div className="col-4 pb-pets-petlistimgcol" style={{ padding: "5px" }}>
                                    <img className="pb-pets-petlistimg" src={elem.Foto_perfil} alt="" >
                                    </img>
                                </div>
                                <div className="col-8 align-self-center">
                                        <Row style={{ fontWeight: "bold" }}>
                                            {elem.nombre}
                                        </Row>
                                        <Row>
                                            {elem.Raza.nombre}
                                        </Row>
                                        <Row>
                                            {elem.genero}
                                        </Row>
                                </div>
                            </div>
                        </Card>
                )
            })
            }
        </>
    )
}

export default ProfileCol;