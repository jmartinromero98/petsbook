import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom';

import Context from '../util/AppContext';

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

function Public(props) {

    const history = useHistory();
    var nvideo = getRandomInt(1,7);

    const context = useContext(Context);

    useEffect(() => {
        
        if (context.usuario) {


            history.push("/main");

        }

    }, [context.usuario]);

    return (
        <Container>
            <video playsInline autoPlay muted loop id="bgvid">
                <source src={"video/bgvideo_"+nvideo.toString()+".mp4"} type="video/mp4" />
            </video>
            <Row className="row justify-content-around">
                <Col >
                    <div className="pb-publiccard">
                        <h1 className="public-title">PetsBook.</h1>
                        <h3>Conecta con el mundo animal.</h3>
                        <br></br>
                        <br></br>
                        <h5>Comparte los momentos más especiales de tus mascotas y descubre un mundo totalmente nuevo con tan solo registrarte.</h5>
                    </div>
                </Col>
            </Row>
        </Container>
    )

}

export default Public