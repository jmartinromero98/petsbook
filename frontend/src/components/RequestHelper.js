const API_URL = "http://localhost:8000"

const requestHelper = {

    get: (url, options) => {
        let headers = {};

        url = API_URL + url;

        // the endpoint might require a different method
        let method = "GET";
        if ("method" in options) {
            method = options.method;
        }

        return fetch(
            url,
            {
                method,
                headers: headers
            }
        ).then(response => response.json());
    },

    post: (url, body, auth) => {
        let headers = {
            "Content-Type": "application/json",
            "Accept": "application/json"
        };

        if (auth) {
            // we must add the authentication token in the headers
            headers["authorization"] = document.cookie.split('=')[1];

        }

        // the endpoint might require a different method
        let method = "POST";

        url = API_URL + url;

        let bodyParams = JSON.stringify(body);

        return fetch(
            url,
            {
                method: method,
                body: bodyParams,
                headers: headers
            }
        ).then(res => res.json()).then((res) => {

            if (res.status == "badToken" || res.status == "badVerify") {


                document.cookie = "authorization=";
                window.location = "/login";

                return Promise.reject();

            } else {

                return res;

            }

        })
    }
};

export default requestHelper;