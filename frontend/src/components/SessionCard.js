import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import FormInput from './FormInput';
import { useState, useEffect, createRef } from 'react';
import '../styles/session.scss';
import SitesInput from './SitesInput2';
import Cropper from './Cropper';
import { useHistory } from 'react-router-dom';
import requestHelper from './RequestHelper';

function SessionCard(props) {

   // form states to get input values
   const [name, setName] = useState("");
   const [email, setEmail] = useState("");
   const [password, setPassword] = useState("");
   const [repeated, setRepeated] = useState("");
   const [hash, setHash] = useState("");
   const [cropData, setCropData] = useState("#");
   const [muni, setMuni] = useState(0);

   // card states used by inputs to display themselves
   const [isEmailValid, setEmailValid] = useState({ valid: true, msg: "" });
   const [isPasswordValid, setPasswordValid] = useState({ valid: true, msg: "" });
   const [isRepeatedValid, setRepeatedValid] = useState({ valid: true, msg: "" });
   const [isNameValid, setNameValid] = useState({ valid: true, msg: "" });
   const [isHashValid, setHashValid] = useState({ valid: true, msg: "" });
   const [isMunicipioValid, setMunicipioValid] = useState({ valid: true, msg: "" });
   const [isCropValid, setCropValid] = useState({ valid: true, msg: "" });

   const handleCrop = (i) => setCropData(i);
   const handleCropValid = (i) => setCropValid(i);


   // hook for form state
   const [isLoading, setLoading] = useState(false);

   // for redirects
   const history = useHistory();

   // useEffect to load info for register
   useEffect(() => {

      setName("");
      setEmail("");
      setPassword("");
      setRepeated("");
      setHash("");
      setEmailValid({ valid: true, msg: "" });
      setPasswordValid({ valid: true, msg: "" });
      setRepeatedValid({ valid: true, msg: "" });
      setNameValid({ valid: true, msg: "" });
      setHashValid({ valid: true, msg: "" });

   }, [props.type]);

   //submit function
   function onSubmit(e) {

      e.preventDefault();

      setLoading(true);

      // register validation and request
      if (props.type == "Register") {

         // it must to be checked in server side too
         if (password != repeated) {

            setPasswordValid({ valid: false, msg: "" });
            setRepeatedValid({ valid: false, msg: "Las contraseñas no coinciden" });

            setLoading(false);

         } else {

            requestHelper.post('/session/register', {

               "Foto_perfil": cropData,
               "name": name,
               "email": email,
               "municipio": muni,
               "password": password,
               "repeated": repeated

            }, false).then((res) => {

               if (res.status == "rpassword") {

                  setPasswordValid({ valid: false, msg: "" });
                  setRepeatedValid({ valid: false, msg: "Las contraseñas no coinciden." });
               }

               if (res.status == "password") {

                  setPasswordValid({ valid: false, msg: "La constraseña debe tener al menos 8 caracteres." });
                  setRepeatedValid({ valid: false, msg: "" });
               }

               if (res.status == "name") {

                  setNameValid({ status: false, msg: "El formato del nombre es incorrecto." })

               }

               if (res.status == "badEmail") {

                  setEmailValid({ status: false, msg: "El formato del email es incorrecto." });

               }

               if (res.status == "emailFound") {
                  setEmailValid({ status: false, msg: "El email ya está registrado." });
               }

               if (res.status == "badMuni") {
                  setMunicipioValid({ status: false, msg: "El municipio seleccionado no es correcto." });
               }

               setLoading(false);

               // if register succesfull, save jwt in cookie and redirect 
               if (res.status == true) {
                  document.cookie = "authorization=Bearer " + res.token;
                  window.location.href = "/main";
               }

            }).catch((err) => {

               setLoading(false);
               
            })
         }
      }

      if (props.type == "Login") {

         requestHelper.post('/session/login', {

            "email": email,
            "password": password,

         }, false).then((res) => {

            // manage error responses
            if (res.status == "badEmail") {

               setEmailValid({ status: false, msg: "El formato del email es incorrecto." });

            }

            if (res.status == "emailNotFound") {

               setEmailValid({ status: false, msg: "El email no está registrado." });

            }

            if (res.status == "badPassword") {

               setPasswordValid({ status: false, msg: "La constraseña es incorrecta." });

            }

            setLoading(false);

            // if its ok then save token and redirect.
            if (res.status == true) {

               document.cookie = "authorization=Bearer " + res.token;
               window.location.href = "/main";

            }

         });
      }

      if (props.type == "Forgot") {

         requestHelper.post('/session/forgot', {

            email: email

         }, false).then((res) => {

            if (res.status == "badEmail") {

               setEmailValid({ status: false, msg: "El formato del email es incorrecto." });

            }

            // check for emailNotFound
            if (res.status == "emailNotFound") {

               setEmailValid({ status: false, msg: "El email introducido no está registrado." });

            }

            if (res.status == true) {

               history.push("/restore");

            }

            setLoading(false);

         });

      }

      if (props.type == "Restore") {

         requestHelper.post('/session/restore', {

            email: email,
            recovery: hash,
            password: password,
            repeated: repeated,

         }, false).then((res) => {


            if (res.status == "badHash") {

               setLoading(false);
               setHashValid({ status: false, msg: "Código erróneo." });

            }

            if (res.status == "expiredHash") {

               setLoading(false);
               setHashValid({ status: false, msg: "Código expirado." });

            }

            if (res.status == "badEmail") {

               setLoading(false);
               setEmailValid({ status: false, msg: "El formato es incorrecto." });

            }

            if (res.status == "emailNotFound") {

               setLoading(false);
               setEmailValid({ status: false, msg: "El correo no está registrado." });

            }

            if (res.status == "rpassword") {

               setLoading(false);
               setPasswordValid({ status: false, msg: "" });
               setRepeatedValid({ status: false, msg: "Las contraseñas no coinciden." });

            }

            if (res.status == "password") {

               setLoading(false);
               setPasswordValid({ status: false, msg: "La contraseña debe ser de al menos 8 caracteres." });

            }

            if (res.status == true) {

               document.cookie = "authorization=Bearer " + res.token;
               window.location.href = "/main";

            }

         });

      }
   }

   return (
      <Container>
         <Row>
            <Col className="col-md-10 mx-auto">
               <div className="myform form sessionCard">
                  <div className="logo mb-3">
                     <div className="col-md-12 text-center">
                        <h1 className="pb-sessioncard-title">
                           {props.type == "Login" && "Inicio de sesión"
                              || props.type == "Register" && "Registro"
                              || props.type == "Restore" && "Restablecer contraseña"
                              || props.type == "Forgot" && "Restablecer contraseña"
                           }
                        </h1>
                     </div>
                  </div>
                  <form onSubmit={onSubmit} name={props.type} noValidate="noValidate">
                     {props.type == "Login" &&
                        <>

                           <FormInput
                              value={email}
                              onChange={(e) => { setEmail(e.target.value); setEmailValid({ valid: true, msg: "" }) }}
                              valid={isEmailValid.valid}
                              msg={isEmailValid.msg}
                              type="email"
                              name="su email"
                              id="emailInput"
                              label="Email" />

                           <FormInput
                              value={password}
                              onChange={(e) => { setPassword(e.target.value); setPasswordValid({ valid: true, msg: "" }) }}
                              valid={isPasswordValid.valid}
                              msg={isPasswordValid.msg}
                              type="password"
                              name="su constraseña"
                              id="password"
                              label="Contraseña" />

                           <div className="form-group">
                              <p className="text-center"><a className="session-a" data-href="/forgot" onClick={(e) => { history.push(e.target.dataset.href) }}>¿Olvidó su contraseña?</a></p>
                           </div>

                        </>

                     }

                     {props.type == "Register" &&
                        <>
                           <Cropper
                              titulo={"Foto de perfil"}
                              cropData={cropData}
                              setImage={handleCrop}
                              valid={isCropValid}
                              setValid={handleCropValid}
                           >

                           </Cropper>
                           <FormInput
                              value={name}
                              onChange={(e) => { setName(e.target.value); setNameValid({ valid: true, msg: "" }) }}
                              valid={isNameValid.valid}
                              msg={isNameValid.msg}
                              type="text"
                              name="su nombre"
                              id="name"
                              label="Nombre" />

                           <FormInput
                              value={email}
                              onChange={(e) => { setEmail(e.target.value); setEmailValid({ valid: true, msg: "" }) }}
                              valid={isEmailValid.valid}
                              msg={isEmailValid.msg}
                              type="email"
                              name="su email"
                              id="emailInput"
                              label="Email"
                           />

                           <SitesInput onChange={(muniSel) => setMuni(muniSel)} isMunicipioValid={isMunicipioValid} setMunicipioValid={setMunicipioValid} />

                           <FormInput
                              value={password}
                              onChange={(e) => { setPassword(e.target.value); setPasswordValid({ valid: true, msg: "" }) }}
                              valid={isPasswordValid.valid}
                              msg={isPasswordValid.msg}
                              type="password"
                              name="su constraseña"
                              id="password"
                              label="Contraseña" />

                           <FormInput
                              value={repeated}
                              onChange={(e) => { setRepeated(e.target.value); setRepeatedValid({ valid: true, msg: "" }) }}
                              valid={isRepeatedValid.valid}
                              msg={isRepeatedValid.msg}
                              type="password"
                              name="su constraseña"
                              id="repeated"
                              label="Repita la contraseña" />


                        </>
                     }

                     {props.type == "Forgot" &&
                        <>
                           <FormInput
                              value={email}
                              onChange={(e) => { setEmail(e.target.value); setEmailValid({ valid: true, msg: "" }) }}
                              valid={isEmailValid.valid}
                              msg={isEmailValid.msg}
                              type="email"
                              name="su email"
                              id="emailInput"
                              label="Email" />
                           <div className="form-group">
                              <p className="text-center">Le enviaremos la nueva contraseña a su correo.</p>
                           </div>
                        </>
                     }

                     {props.type == "Restore" &&
                        <>

                           <FormInput
                              value={email}
                              onChange={(e) => { setEmail(e.target.value); setEmailValid({ valid: true, msg: "" }) }}
                              valid={isEmailValid.valid}
                              msg={isEmailValid.msg}
                              type="email"
                              name="su email"
                              id="emailInput"
                              label="Email" />

                           <FormInput
                              value={hash}
                              onChange={(e) => { setHash(e.target.value); setHashValid({ valid: true, msg: "" }) }}
                              valid={isHashValid.valid}
                              msg={isHashValid.msg}
                              type="text"
                              name="el código de recuperación"
                              id="hashInput"
                              label="Código" />

                           <FormInput
                              value={password}
                              onChange={(e) => setPassword(e.target.value)}
                              valid={isPasswordValid.valid}
                              msg={isPasswordValid.msg}
                              type="password"
                              name="una nueva constraseña"
                              id="password"
                              label="Nueva contraseña" />

                           <FormInput
                              value={repeated}
                              onChange={(e) => setRepeated(e.target.value)}
                              valid={isRepeatedValid.valid}
                              msg={isRepeatedValid.msg}
                              type="password"
                              name="una nueva constraseña"
                              id="repeated"
                              label="Repita la nueva contraseña" />

                           <div className="form-group">
                              <p className="text-center"> Su nueva contraseña será establecida. </p>
                           </div>

                        </>

                     }
                     <div className="col-md-12 text-center ">
                        <button id="actionButton" type="submit" className=" btn btn-block mybtn btn-primary tx-tfm" disabled={isLoading} >
                           {isLoading && <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>}
                           {isLoading ? "Cargando..." : props.buttonName}</button>
                     </div>
                  </form>
               </div>
            </Col>
         </Row>
      </Container>
   )
}

export default SessionCard;