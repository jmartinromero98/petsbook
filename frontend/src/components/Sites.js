import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import ListGroup from 'react-bootstrap/ListGroup';
import FormInput from './FormInput';

import SitesInput from './SitesInput';
import { useState, createRef, useRef, useEffect } from 'react';

import { MapContainer, TileLayer, Marker, Popup, useMapEvents, useMap } from 'react-leaflet';
import L from 'leaflet';

import MapModal from './MapModal';

import { useHistory } from 'react-router-dom';


import requestHelper from './RequestHelper';
//const ENDPOINT = "http://127.0.0.1:8000";
const ENDPOINT = "ws://localhost:8000/";

function Sites(props) {

    //hooks for filter values
    const auto = createRef();
    const prov = createRef();
    const muni = createRef();
    const [isMunicipioValid, setMunicipioValid] = useState({ valid: true, msg: "" });
    const [type, setType] = useState("");

    const [isLoading, setLoading] = useState(false);

    //map position clicked
    const [sites, setSites] = useState(undefined);

    const [mapState, setMapState] = useState({ lat: 40.463667, lng: -3.74922, zoom: 6 });

    //for register modal
    const [show, setShow] = useState(false);


    //positions refs
    var markerRefs = [];
    var id = 0;

    //hooks for socket.io
    const [amigos, setAmigos] = useState([]);
    const [isSharing, setIsSharing] = useState(false);
    const [userPosition, setUserPosition] = useState({ lat: 0, lng: 0 })
    var interval = useRef(-1);
    const socket = useRef(null);
    const map = useRef(null);

    //hooks for modal of comments
    const [showCom, setShowCom] = useState(false);
    const [siteSelected, setSiteSelected] = useState(-1);
    const [input, setInput] = useState("");
    const [comments, setComments] = useState([]);
    const history = useHistory();

    const [name, setName] = useState("");

    const handleCloseCom = () => {
        setShowCom(false);
        setComments([]);
    };

    const handleShowCom = () => {
        setShowCom(true);

        requestHelper.post('/ubis/getComments', { ubicacion: siteSelected }, true).then((res) => {

            if (res.status == true) {

                setComments(res.data);

            }

        });

    }

    var hospIcon = L.icon({
        iconUrl: "img/hospital-location.png",
        iconSize: [32, 32],
        iconAnchor: [16, 32],
        shadowUrl: null,
        shadowSize: null,
        shadowAnchor: null
    });

    var shopIcon = L.icon({
        iconUrl: "img/shop-location.png",
        iconSize: [48, 48],
        iconAnchor: [24, 48],
        shadowUrl: null,
        shadowSize: null,
        shadowAnchor: null
    });

    var parkIcon = L.icon({
        iconUrl: "img/park-location.png",
        iconSize: [32, 32],
        iconAnchor: [16, 32],
        shadowUrl: null,
        shadowSize: null,
        shadowAnchor: null
    });


    useEffect(() => {

        requestHelper.post('/friends/getFriends', {}, true).then((res) => {

            //modifying response to make structure
            var response = res;
            var result = {};
            response.map((elem, idx) => {
                if (Object.keys(elem.receptorTable).length === 0 &&
                    elem.receptorTable.constructor === Object) {

                    result[elem.emisorTable.id_usuario] = elem.emisorTable;
                    result[elem.emisorTable.id_usuario].show = false;

                } else {

                    result[elem.receptorTable.id_usuario] = elem.receptorTable;
                    result[elem.receptorTable.id_usuario].show = false;

                }
            })
            setAmigos(result);

        });

        return function cleanup() {
            closeSharing();
        }

    }, []);

    const popupClick = (e) => {
    }

    function onSubmit(e) {

        e.preventDefault();

        requestHelper.post('/ubis/getUbis', {

            "name": name,
            "autonomia": document.getElementById("selectCA").value,
            "provincia": document.getElementById("selectProv").value,
            "municipio": document.getElementById("selectMuni").value,
            "tipo": document.getElementById("selectSite").value

        }, true).then((res) => {

            if (res.status == true) {

                setSites(res.data);

            }

        });

    }

    function openPopUp(marker, id) {
        if (marker && marker.leafletElement) {
            marker.leafletElement.openPopup(id);
        }
    }

    function clickAction(id, lat, lng) {

        map.current.setView([lat, lng], 16)

    }

    function FocusInMarker() {
        const map = useMap();
        map.setView([mapState.lat, mapState.lng], mapState.zoom);

        return null;
    }

    function handleClose() {

        setShow(false);

    }

    function showPosition(position) {

        setUserPosition({ lat: position.latitude, lng: position.longitude });

    }

    function closeSharing() {

        clearInterval(interval.current);


        if (socket.current) {
            socket.current.close();
        }


        var nextAmigos = { ...amigos }
        Object.keys(nextAmigos).map((key) => {
            nextAmigos[key].show = false;
        })
        setAmigos(nextAmigos);

        setIsSharing(false);

        socket.current = null;

    }

    function sharePosition() {

        if (socket.current == null) {
            socket.current = new WebSocket(ENDPOINT);
        }

        if (isSharing) {

            closeSharing();

        } else {

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            }

            socket.current.onopen = () => {

                interval.current = setInterval(() => {
                    socket.current.send(JSON.stringify({ token: document.cookie.split("=")[1], lat: Math.random() * 10, lng: Math.random() * 10 }));
                }, 1000)
            }

            socket.current.onmessage = (msg) => {


                // modifying amigos
                var nextAmigos = { ...amigos };
                msg = JSON.parse(msg.data);

                if (msg.type == "location") {

                    nextAmigos[msg.id].lat = msg.lat;
                    nextAmigos[msg.id].lng = msg.lng;
                    nextAmigos[msg.id].show = true;

                } else if (msg.type == "ending") {

                    nextAmigos[msg.id].show = false;

                }

                setAmigos(nextAmigos);

            }

            setIsSharing(true);
            //socket.send({lat: 2, long: 1});

        }


    }

    function sendCom() {


        requestHelper.post('/ubis/sendComment', {

            ubicacion: siteSelected,
            comentario: input,

        }, true).then((res) => {

            if (res.status == true) {

                setComments(res.data);
                setInput("");

            }

        });

    }

    return (
            <Row style={{width: "100%"}}>
                <Col md={4} style={{ paddingLeft: "30px", paddingRight: "30px" }}>
                    <Form onSubmit={onSubmit} name="searchFriends" id="sitesForm" noValidate="noValidate">
                        <Card className="pb-petlist-searchcard" >
                        <FormInput
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                                valid={true}
                                msg={true}
                                type="text"
                                name="el nombre del usuario a buscar"
                                id="PetName"
                                label="Nombre" />
                            <SitesInput isMunicipioValid={isMunicipioValid} auto={auto} prov={prov} muni={muni} setMunicipioValid={setMunicipioValid} />
                            <Form.Group id="exampleForm.SelectCustomSizeSm">
                                <div className="input-label">Tipo de lugar</div>
                                <Form.Control className="my-1 mr-sm-2" id="selectSite" as="select" custom>
                                    <option value="0"> Elija un tipo de lugar...</option>
                                    <option value="1"> Parque </option>
                                    <option value="2"> Tienda </option>
                                    <option value="3"> Veterinario </option>
                                </Form.Control>
                            </Form.Group>
                        </Card>

                        <Card>
                            <button id="actionButton" type="submit" className=" btn btn-block btn-success tx-tfm" disabled={isLoading} >
                                {isLoading && <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>}
                                {isLoading ? "Cargando..." : "Muestra lugares"}
                            </button>
                        </Card>
                    </Form>

                    <button id="actionButton" className=" btn btn-block btn-success tx-tfm" style={{ marginBottom: "20px" }} disabled={isLoading} onClick={() => setShow(true)}>
                        Añade una nueva ubicación
                    </button>

                    <button id="actionButton" className=" btn btn-block btn-warning tx-tfm" disabled={isLoading} onClick={() => sharePosition()}>
                        {isSharing ? "Deja de compartir la ubicacion" : "Comparte la ubicacion"}
                    </button>

                </Col>
                <Col md={8} style={{ paddingLeft: "35px" }}>

                    <MapContainer className="leafletMapa" center={[mapState.lat, mapState.lng]} zoom={mapState.zoom} scrollWheelZoom={true} whenCreated={(mapa) => { map.current = mapa }} style={{ zIndex: "0" }}>
                        <TileLayer
                            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                        />
                        {amigos && Object.keys(amigos).map((key, idx) => {

                            if (amigos[key].show) {

                                var imgIcon = L.icon({
                                    className: "icon-rounded",
                                    iconUrl: amigos[key].Foto_perfil,
                                    iconSize: [18, 18],
                                    iconAnchor: [9, 18],
                                    shadowUrl: null,
                                    shadowSize: null,
                                    shadowAnchor: null
                                })

                                return (
                                    <>
                                        <Marker icon={imgIcon} position={[amigos[key].lat, amigos[key].lng]} key={idx}>
                                            <Popup>
                                                <span style={{ fontWeight: "bold" }}>{amigos[key].nombre}</span>
                                                <br></br>
                                                <img className="pb-pets-petlistimg" src={amigos[key].Foto_perfil} alt="" >
                                                </img>
                                                <Button onClick={() => history.push("/user?id=" + amigos[key].id_usuario)}>
                                                    Ver perfil
                                                </Button>
                                            </Popup>
                                        </Marker>
                                    </>
                                )

                            }
                        })

                        }

                        {sites && sites.length > 0 && sites.map((elem, idx) => {

                            var img;

                            switch (elem.tipo) {

                                case "Parque": img = parkIcon; break;
                                case "Veterinario": img = hospIcon; break;
                                case "Tienda": img = shopIcon; break;

                            }

                            return (
                                <>
                                    <Marker icon={img} position={[elem.lat, elem.long]} key={idx}>
                                        <Popup onOpen={(e) => { setSiteSelected(elem.id_ubi) }}>
                                            <span style={{ fontWeight: "bold" }}>{elem.titulo}</span>
                                            <br></br>
                                            <p>{elem.descripcion}</p>
                                            <Button onClick={(e) => { handleShowCom() }}>
                                                Comentarios
                                            </Button>
                                        </Popup>
                                    </Marker>
                                </>
                            )

                        })}

                    </MapContainer>
                    <div className="siteListContainer">
                        {isSharing && <p style={{ fontWeight: "bold" }}>Amigos</p>}
                        <ul className="list-group list-group-flush">
                        {amigos && Object.keys(amigos).map((elem, idx) => {

                            if (amigos[elem].show) {

                                return (
                                    <li className="list-group-item" key={idx}>
                                        <div className="row pb-friends-friendlistcont container" /*style={ { display: elem.nombre.toLowerCase().indexOf(search.toLowerCase()) !== -1 ? "" : "none"} } */
                                            onClick={(e) => {
                                                // if the target of the event isnt a child button.
                                                if (e.target.type == "button") {

                                                } else {
                                                    clickAction(0, amigos[elem].lat, amigos[elem].lng);
                                                }
                                            }}
                                        >

                                            <div className="col pb-pets-petlistimgcol">
                                                <img className="pb-pets-petlistimg" src={elem && amigos[elem].Foto_perfil} alt="" >
                                                </img>
                                            </div>
                                            <div className="col-6 pb-pets-petlistcol">
                                                <div className="row pb-pets-petlistname">
                                                    {amigos[elem].nombre}
                                                </div>
                                                <div className="row">
                                                    {"En " + amigos[elem].Municipio.nombre + ", " + amigos[elem].Municipio.Provincia.Autonomia.nombre}
                                                </div>

                                            </div>
                                        </div>
                                    </li>
                                )

                            }
                        })}
                        </ul>

                        {sites && sites.length > 0 && <p style={{ fontWeight: "bold" }}>Lugares</p>}
                        <ul className="list-group list-group-flush">
                            {sites && sites.length > 0 && sites.map((elem, idx) => {

                                var img;

                                switch (elem.tipo) {

                                    case "Parque": img = "../img/arbol.png"; break;
                                    case "Tienda": img = "../img/cesta3.png"; break;
                                    case "Veterinario": img = "../img/hospital.png"; break;

                                }

                                return (
                                    <li className="list-group-item" key={idx}>
                                        <div className="row pb-pets-petlistcont" onClick={(e) => { clickAction(elem.id_ubi, elem.lat, elem.long) }}>
                                            <div className="col-2 pb-pets-petlistimgcol">
                                                <img className="pb-pets-petlistimg" src={img} alt="" >
                                                </img>
                                            </div>
                                            <div className="col-10 pb-pets-petlistcol">
                                                <div className="row pb-pets-petlistname">
                                                    {elem.titulo}
                                                </div>
                                                <div className="row">
                                                    {elem.Municipio.nombre}
                                                </div>
                                                <div className="row">
                                                    {elem.Municipio.Provincia.Autonomia.nombre}
                                                </div>
                                            </div>
                                            <div className="col pb-pets-petlistcol">
                                            </div>
                                        </div>
                                    </li>
                                )
                            })
                            }
                        </ul>
                        <Modal show={showCom} handleClose={handleCloseCom} onHide={handleCloseCom}>
                            <Modal.Header closeButton>
                                <Modal.Title>Comentarios</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <ListGroup variant="flush">
                                    {comments.length > 0 && comments.map((elem, idx) => {
                                        return (
                                            <>
                                                <ListGroup.Item className="pb-comment-row" onClick={(e) => { history.push("user?id=" + elem.emisor) }} key={idx}>
                                                    <Row>
                                                        <Col xs={2}>
                                                            <img className="pb-pets-petlistimg" style={{ width: "100%", margin: "0px" }} id="s1" srcset={elem.escritor.Foto_perfil} type="image/png" />
                                                        </Col>
                                                        <Col>
                                                            <Row>
                                                                <Col style={{ paddingLeft: "0px" }}>
                                                                    {elem.escritor.nombre}
                                                                </Col>
                                                                <Col className="pb-post-datecol">
                                                                    {new Date(elem.createdAt).toLocaleDateString()}
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                {elem.comentario}
                                                            </Row>
                                                        </Col>
                                                    </Row>
                                                </ListGroup.Item>
                                            </>
                                        )
                                    })
                                    }
                                </ListGroup>
                            </Modal.Body>
                            <Modal.Footer>
                                <Row className="pb-comment-inputrow">
                                    <Col xs={8}>
                                        <Form.Group controlId="exampleForm.ControlTextarea1">
                                            <Form.Control type="text" value={input} placeholder="Escriba un comentario" onChange={(e) => { setInput(e.target.value) }} />
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                        <Button className="pb-comment-sendbutton" variant="primary" onClick={sendCom}>
                                            Enviar
                                        </Button>
                                    </Col>
                                </Row>
                            </Modal.Footer>
                        </Modal>

                        <MapModal show={show} handleClose={handleClose} />
                    </div>
                </Col>
            </Row>
    )

}



export default Sites;