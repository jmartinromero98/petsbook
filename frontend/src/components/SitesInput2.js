import Form from 'react-bootstrap/Form';
import { useState, useEffect } from 'react';
import requestHelper from './RequestHelper';
import { getAllByPlaceholderText, getDefaultNormalizer } from '@testing-library/react';

function SitesInput(props) {

    const [auto, setAuto] = useState([]);
    const [muni, setMuni] = useState([]);
    const [prov, setProv] = useState([]);

    const [autoSel, setAutoSel] = useState(0);
    const [provSel, setProvSel] = useState(0);
    const [muniSel, setMuniSel] = useState(0);

    const [autoSetted, setAutoSetted] = useState(false);
    const [provSetted, setProvSetted] = useState(false);
    const [muniSetted, setMuniSetted] = useState(false);

    useEffect(() => {

        console.log(props.defAuto);

        if (!autoSetted && props.defAuto) {

            setAutoSetted(true);

            getCA();

            setAutoSel(props.defAuto);

        } else {

            getCA();

        }

    }, [])


    function getCA() {

        requestHelper.post('/session/sites/ca', {}, false).then((res) => {

            setAuto(res.data);

        });

    }

    useEffect(() => {

        if (autoSel != 0) {

            if (!provSetted && props.defProv) {

                setProvSetted(true);

                getProv();

                setProvSel(props.defProv);


            } else {

                getProv();

                setMuni([]);
                setProvSel(0);
                setMuniSel(0);
            }

        } else {

            setProv([]);
            setProvSel(0);
            setMuniSel(0);

        }

    }, [autoSel]);

    function getProv() {

        requestHelper.post('/session/sites/provincias', {

            id_autonomia: autoSel

        }, false).then((res) => {

            setProv(res.data);

        });

    }

    useEffect(() => {

        if (provSel != 0) {

            if (!muniSetted && props.defMuni) {

                setMuniSetted(true);

                getMuni();

                setMuniSel(props.defMuni);

            } else {

                getMuni();
                setMuniSel(0);

            }

        } else {

            setMuni([]);
            setMuniSel(0);

        }

    }, [provSel]);

    useEffect(() => {

        props.onChange(muniSel);
    
    }, [muniSel]) 

    function getMuni() {

        requestHelper.post('/session/sites/municipios', {

            id_provincia: provSel

        }, false).then((res) => {

            setMuni(res.data);

        });

    }

    function getResultado() {

        console.log(autoSel + " " + provSel + " " + muniSel);

    }

    return (

        <>
            <Form.Group id="exampleForm.SelectCustomSizeSm">
                <div className="input-label">Comunidad autonoma</div>
                <Form.Control className="my-1 mr-sm-2" id="selectCA" as="select" onChange={(e) => { setAutoSel(e.target.value) }} custom>
                    <option value="0"> Elija una comunidad autonoma...</option>
                    {auto.map((elem) => {

                        return (
                            <option value={elem.id_autonomia} selected={autoSel == elem.id_autonomia}>{elem.nombre}</option>
                        )

                    })}
                </Form.Control>
            </Form.Group>

            <Form.Group id="exampleForm.SelectCustomSizeSm">
                <div className="input-label">Provincia</div>
                <Form.Control as="select" id="selectProv" onChange={(e) => { setProvSel(e.target.value) }} custom>
                    <option value="0"> Elija una provincia...</option>
                    {prov.map((elem) => {

                        return (
                            <option value={elem.id_provincia} selected={provSel == elem.id_provincia}>{elem.nombre}</option>
                        )

                    })}
                </Form.Control>
            </Form.Group>

            <Form.Group id="exampleForm.SelectCustomSizeSm">
                <div className="input-label">Municipio</div>
                <Form.Control as="select" id="selectMuni" onChange={(e) => { setMuniSel(e.target.value) }} className={props.isMunicipioValid.valid ? "" : "is-invalid"} custom>
                    <option value="0">Elija un municipio...</option>
                    {muni.map((elem) => {

                        return (
                            <option value={elem.codine} selected={muniSel == elem.codine}>{elem.nombre}</option>
                        )

                    })}
                </Form.Control>
                <div className="invalid-feedback">
                    {props.isMunicipioValid.msg}
                </div>
            </Form.Group>
        </>

    )

}

export default SitesInput;