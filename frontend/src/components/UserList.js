import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import '../styles/pets.scss';
import { useState, createRef, useEffect } from 'react';
import SitesInput from './SitesInput';
import { Form } from 'react-bootstrap';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import { useHistory } from 'react-router-dom';
import requestHelper from './RequestHelper';
import FormInput from './FormInput';

function UserList() {

    const [isMunicipioValid, setMunicipioValid] = useState({ valid: true, msg: "" });
    const [isLoading, setLoading] = useState(false);
    const [users, setUsers] = useState([]);
    const [name, setName] = useState("");
    const [page, setPage] = useState(1);

    var history = useHistory();

    // form sites refs
    var auto = createRef();
    var prov = createRef();
    var muni = createRef();

    function petition(e) {


        /*
        var i = 0;
        var found = false;
        while(i < users.length && !found) {

            if (users[i].id_usuario == parseInt(e.target.id)) {
                users[i].SolAsReceptor = [{estado: "Enviada"}];
                setUsers([...users]);
                found = true;
            }  

            i++;

        }
        */
        requestHelper.post('/friends/petition', {

            "receptor": e.target.id

        }, true).then((res) => {

            // re-render
            getUsers();

        });


    }

    function getPrevious() {

        if (page > 0 || users.length >= 5) {

            setPage(page-1);

        }

    }

    function getNext() {

        if (users.length >= 5) {

            setPage(page+1);

        }

    }

    function cancel(e) {


        requestHelper.post('/friends/cancel', {

            "receptor": e.target.id

        }, true).then((res) => {

            //re-render
            getUsers();

        });

    }

    function accept(e) {


        requestHelper.post('/friends/accept', {

            "id": e.target.id

        }, true).then((res) => {

            //re-render
            getUsers();

        });

    }

    function remove(e) {


        requestHelper.post('/friends/remove', {

            "id": e.target.id

        }, true).then((res) => {

            //re-render
            getUsers()

        });

    }


    function onSubmit(e) {

        e.preventDefault();

        if (page == 1) {

            getUsers();

        } else {

            setPage(1);

        }

    }

    function getUsers() {

        requestHelper.post('/friends/getUsers', {

            "nombre": name,
            "Autonomia": document.getElementById("selectCA").value,
            "Provincia": document.getElementById("selectProv").value,
            "Municipio": document.getElementById("selectMuni").value,
            "page" :page

        }, true).then((res) => {

            setUsers(res.data);

        });

    }

    useEffect(() => {
        

        requestHelper.post('/friends/getUsers', {

            "nombre": name,
            "Autonomia": document.getElementById("selectCA").value,
            "Provincia": document.getElementById("selectProv").value,
            "Municipio": document.getElementById("selectMuni").value,
            "page" :page

        }, true).then((res) => {

            if ((res.data.length > 0 && !sameResponse(res.data)) || page == 1 ) {

                setUsers(res.data);

            } else {

                setPage(page - 1);

            }

        });

        

    }, [page]);

    function sameResponse(res) {

        if (res.length != users.length) {
            return false;
        }

        for (var i = 0; i < users.length; i++) {


            if (res[i].id_usuario != users[i].id_usuario) {

                return false;

            }

        }

        return true;

    }

    return (

        <>
            <Row style={{width: "100%"}}>
                <Col sm={4} style={{ paddingLeft: "30px", paddingRight: "30px" }}>
                    <Form onSubmit={onSubmit} name="searchFriends" noValidate="noValidate">
                        <Card className="pb-petlist-searchcard" >
                            <FormInput
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                                valid={true}
                                msg={true}
                                type="text"
                                name="el nombre del usuario a buscar"
                                id="PetName"
                                label="Nombre" />
                            <SitesInput auto={auto} prov={prov} muni={muni} isMunicipioValid={isMunicipioValid} setMunicipioValid={setMunicipioValid} />
                        </Card>

                        <Card>
                            <button id="actionButton" type="submit" className=" btn btn-block btn-success tx-tfm" disabled={isLoading} >
                                {isLoading && <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>}
                                {isLoading ? "Cargando..." : "Busca un nuevo amigo"}
                            </button>
                        </Card>
                    </Form>
                </Col>
                <Col sm={8} style={{ paddingLeft: "30px", paddingRight: "30px" }}>
                    <div className="listContainer">
                        <ul className="list-group list-group-flush">
                            {users.length == 0 && <h1 className="pb-userlist-notfound">Realiza una nueva búsqueda</h1>}
                            {users.length > 0 && users.map((elem, idx) => {

                                    return (
                                        <>
                                            <li className="list-group-item" key={idx}>
                                                <div className="row pb-friends-friendlistcont container" /*style={ { display: elem.nombre.toLowerCase().indexOf(search.toLowerCase()) !== -1 ? "" : "none"} } */
                                                    onClick={(e) => {
                                                        // if the target of the event isnt a child button.
                                                        if (e.target.type != "button") {
                                                            history.push("user?id=" + elem.id_usuario)
                                                        }
                                                    }}
                                                >

                                                    <div className="col pb-pets-petlistimgcol">
                                                        <img className="pb-pets-petlistimg" src={elem && elem.Foto_perfil} alt="" >
                                                        </img>
                                                    </div>
                                                    <div className="col-6 pb-pets-petlistcol align-self-center">
                                                        <div className="row pb-pets-petlistname">
                                                            {elem.nombre}
                                                        </div>
                                                        <div className="row">
                                                            {"En " + elem.Municipio.nombre + ", " + elem.Municipio.Provincia.Autonomia.nombre}
                                                        </div>

                                                    </div>
                                                    <div className="col pb-friends-friendlistcol" style={{ display: "block", alignSelf: "center" }}>
                                                        {(elem.SolAsEmisor.length == 0 && elem.SolAsReceptor.length == 0) &&

                                                            <div className="row" style={{ display: "flex", alignItems: "center" }}>

                                                                <Button variant="primary" onClick={(elem) => petition(elem)} id={elem.id_usuario} className="pb-friends-friendlistbutton">
                                                                    Enviar solicitud
                                                                </Button>
                                                            </div>

                                                        }
                                                        {elem.SolAsReceptor.length != 0 && elem.SolAsReceptor[0].estado == "Enviada" &&

                                                            <div className="row" style={{ display: "flex", alignItems: "center" }}>

                                                                <Button variant="outline-dark" onClick={(elem) => cancel(elem)} id={elem.id_usuario} className="pb-friends-friendlistbutton">
                                                                    Cancelar solicitud
                                                                </Button>
                                                            </div>
                                                        }
                                                        {elem.SolAsEmisor.length != 0 && elem.SolAsEmisor[0].estado == "Enviada" &&
                                                            <>
                                                                <div className="row" style={{ display: "flex", alignItems: "center" }}>

                                                                    <Button variant="success" onClick={(elem) => accept(elem)} id={elem.id} className="pb-friends-friendlistbutton">
                                                                        Aceptar solicitud
                                                                    </Button>
                                                                </div>

                                                                <div className="row" style={{ display: "flex", alignItems: "center" }}>

                                                                    <Button variant="danger" onClick={(elem) => remove(elem)} id={elem.id} className="pb-friends-friendlistbutton">
                                                                        Rechazar solicitud
                                                                    </Button>
                                                                </div>
                                                            </>

                                                        }

                                                    </div>
                                                </div>
                                            </li>
                                            {idx == users.length -1 && <Row className="d-flex justify-content-center" style={{ margin: "20px" }}>
                                                <Col style={{ maxWidth: "450px" }}>
                                                    <Row className="d-flex justify-content-between" >
                                                        <button type="button" className="btn btn-outline-secondary" onClick={getPrevious} disabled={page == 1}> &lt; Anterior</button>
                                                        <button type="button" className="btn btn-outline-secondary" disabled>{page}</button>
                                                        <button type="button" className="btn btn-outline-secondary" onClick={getNext} disabled={users.length < 5}> Siguiente &gt; </button>
                                                    </Row>
                                                </Col>
                                            </Row>
                                            }

                                        </>
                                    )

                            })}

                        </ul>
                    </div>
                </Col>
            </Row>
        </>

    )

}

export default UserList;