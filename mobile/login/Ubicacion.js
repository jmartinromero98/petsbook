import React, { useState, useEffect, useRef } from "react";

import styles from "./style";

import { Keyboard, Text, View, TextInput, TouchableWithoutFeedback, Alert, KeyboardAvoidingView, TouchableOpacity } from 'react-native';

import Geolocation from '@react-native-community/geolocation';

import AsyncStorage from '@react-native-async-storage/async-storage';

const ENDPOINT = "http://192.168.0.159:8000";

function Ubicacion() {

    const [isSharing, setIsSharing] = useState(false);
    const [position, setPosition] = useState({ lat: null, lng: null });

    var interval = useRef(-1);

    var socket = useRef(null);

    let token;

    useEffect(() => {

        AsyncStorage.getItem('authorization').then((tk) => {
            token = tk;
        });


        return function cleanup() {

            console.log("Cerrando comunicación por salida");
            closeSharing();

        }

    }, [])

    function closeSharing() {

        clearInterval(interval.current);

        if (socket.current) {

            socket.current.close();

        }

        setIsSharing(false);

        socket.current = null;

    }

    let geoOptions = {

        enableHighAccuracy: true,
        timeout: 10000,
        maximumAge: 100,

    }

    function handleShare() {

        if (socket.current == null) {

            socket.current = new WebSocket(ENDPOINT);

        }

        if (isSharing) {

            console.log("Cerrando comunicación");

            closeSharing();

        } else {

            console.log("Abriendo comunicación");

            socket.current.onopen = () => {

                interval.current = setInterval(() => {

                    console.log("ejecutando intervalo");

                    Geolocation.getCurrentPosition(pos => {

                        //console.log(pos);
                        console.log("GET LOCATION");

                        setPosition({ lat: pos.coords.latitude, lng: pos.coords.longitude });

                        var msg = JSON.stringify({ token: "Bearer " + token, lat: pos.coords.latitude, lng: pos.coords.longitude });

                        socket.current.send(msg);
                        console.log(msg);

                    }, err => console.log(err), geoOptions);
                    //socket.send(JSON.stringify({ token: "Bearer " + AsyncStorage.getItem('authorization'), lat: position.lat, lng: position.lng }));


                }, 1000);

            }

            console.log("CONECTED");
            setIsSharing(true);
            //socket.send({lat: 2, long: 1});

        }

        clearInterval(interval);

    }



    return (
        <KeyboardAvoidingView style={styles.containerView} behavior="padding">

            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={styles.loginScreenContainer}>
                <View style={styles.loginFormView}>
                    <Text style={styles.logoText}>PetsBook</Text>
                    <Text>{isSharing ? "Lat: " + position.lat : "No estás compartiendo tu ubicación."}</Text>
                    <Text>{isSharing ? "Long: " + position.lng : "Activa la opción pulsando el botón."}</Text>

                    <TouchableOpacity onPress={handleShare} style={styles.btn}>
                        <Text style={styles.btnText}>
                            {isSharing ? "Dejar de compartir ubicación" : "Compartir ubicación"}
                        </Text>
                    </TouchableOpacity>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
    );

}

export default Ubicacion;