import React, { useState } from "react";

import styles from "./style";
import requestHelper from './RequestHelper';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { Keyboard, Text, View, TextInput, TouchableWithoutFeedback, Alert, KeyboardAvoidingView, TouchableOpacity } from 'react-native';
import style from "./style";


const appId = "1047121222092614"

function LoginScreen({ navigation }) {

  const [email, setEmail] = useState("jmartinromero98@gmail.com");
  const [isEmailValid, setEmailValid] = useState({valid: true, msg: ""});

  const [contra, setContra] = useState("12121212");
  const [isPasswordValid, setPasswordValid] = useState({valid: true, msg: ""});

  const [loading, setLoading] = useState(false);

  function onSubmit(e) {

   setLoading(true);

   console.log("login: "+email+", "+contra);
    

      requestHelper.post('/session/login',{

         "email": email,
         "password": contra,

      }, false ).then((res) => {

         console.log(res);

            // manage error responses
            if (res.status == "badEmail") {

               setEmailValid({ status: false, msg: "El formato del email es incorrecto." });

            }

            if (res.status == "emailNotFound") {

               setEmailValid({ status: false, msg: "El email no está registrado." });

            }
            
            if (res.status == "badPassword") {

               setPasswordValid({ status: false, msg: "La constraseña es incorrecta." });

            }

            // if its ok then save token and redirect.
            if (res.status == true) {
               try {
                  AsyncStorage.setItem('authorization', res.token);

               } catch (error) {
                  console.log(error);
               }
               navigation.navigate('Ubicacion');

            }

            setLoading(false);

         }).catch((err) => {

            console.log(err);

         });
         
  }

  return (
    <KeyboardAvoidingView style={styles.containerView} behavior="padding">

      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.loginScreenContainer}>
          <View style={styles.loginFormView}>
            <Text style={styles.logoText}>PetsBook</Text>
            <TextInput value={email} onChangeText={(email) => { setEmailValid({valid: true, msg: ""}); setEmail(email)}} placeholder="Username" placeholderColor="#c4c3cb" style={styles.loginFormTextInput} />
            {!isEmailValid.valid && 
               <Text style={styles.badInput}>Email no encontrado</Text>
            }
            <TextInput value={contra} onChangeText={(contra) => { setPasswordValid({valid: true, msg: ""}); setContra(contra)}} placeholder="Password" placeholderColor="#c4c3cb" style={styles.loginFormTextInput} secureTextEntry={true} />
            {!isPasswordValid.valid && 
               <Text style={styles.badInput}>Contraseña no correcta</Text>
            }
            <TouchableOpacity onPress={(e) => {onSubmit(e)}} disabled={loading} style={loading ? styles.btnDisabled : styles.btn}>
               <Text style={styles.btnText}>
               {loading ? "Cargando" : "Acceder"}
               </Text>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );


}

export default LoginScreen;
